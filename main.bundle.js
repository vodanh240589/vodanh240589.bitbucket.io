webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.css":
/***/ (function(module, exports) {

module.exports = ".app-container {\n  width: 100%;\n  position: relative;\n  top: 50px;\n  min-height: calc(100vh - 10vh);\n  /* overflow-x: hidden; */\n}\n\n.blue {\n  color: #2EC4B6;\n}\n\nfooter {\n  width: 100%;\n  height: 50px;\n  color: #5603AD;\n  border-top: 1px solid #5603AD;\n  margin-bottom: 0;\n  padding-bottom: 0;\n}\n\nfooter div {\n  text-align: center;\n}\n\nfooter div h2 {\n  font-weight: normal;\n  -webkit-transform: translateY(110%);\n  transform: translateY(110%);\n}\n\nmedia only screen and (max-width: 1345px) {\n  #container {\n    width: auto;\n    margin : 0 3%;\n  }\n}"

/***/ }),

/***/ "./src/app/app.html":
/***/ (function(module, exports) {

module.exports = "<app-nav></app-nav>\n<div class=\"app-container\">\n  <router-outlet></router-outlet>\n  <!-- <app-loading *ngIf=\"loading\"></app-loading> -->\n</div>\n<app-footer style=\"position: relative; top: 50px;\"></app-footer>"

/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__agm_core__ = __webpack_require__("./node_modules/@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular_bootstrap_md__ = __webpack_require__("./node_modules/angular-bootstrap-md/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2__ = __webpack_require__("./node_modules/angularfire2/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_angularfire2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angularfire2_database__ = __webpack_require__("./node_modules/angularfire2/database/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app__ = __webpack_require__("./src/app/app.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__nav_nav__ = __webpack_require__("./src/app/nav/nav.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__footer_footer__ = __webpack_require__("./src/app/footer/footer.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__loading_loading__ = __webpack_require__("./src/app/loading/loading.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__public_home_home__ = __webpack_require__("./src/app/public/home/home.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__public_login_login__ = __webpack_require__("./src/app/public/login/login.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__public_business_business__ = __webpack_require__("./src/app/public/business/business.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__public_registration_registration__ = __webpack_require__("./src/app/public/registration/registration.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__public_password_reset_password_reset__ = __webpack_require__("./src/app/public/password_reset/password_reset.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__public_verification_verification__ = __webpack_require__("./src/app/public/verification/verification.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__public_discover_discover__ = __webpack_require__("./src/app/public/discover/discover.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__public_discover_trending_trending__ = __webpack_require__("./src/app/public/discover/trending/trending.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__public_discover_trending_card_item__ = __webpack_require__("./src/app/public/discover/trending/card/item.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__public_discover_trending_map_map__ = __webpack_require__("./src/app/public/discover/trending/map/map.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__public_discover_special_special__ = __webpack_require__("./src/app/public/discover/special/special.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__public_discover_special_card_item__ = __webpack_require__("./src/app/public/discover/special/card/item.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__public_discover_special_detail_special_item__ = __webpack_require__("./src/app/public/discover/special/detail-special/item.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__public_experience_experience__ = __webpack_require__("./src/app/public/experience/experience.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__public_experience_category_category__ = __webpack_require__("./src/app/public/experience/category/category.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__public_experience_filter_filter__ = __webpack_require__("./src/app/public/experience/filter/filter.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__public_experience_list_feed_item__ = __webpack_require__("./src/app/public/experience/list-feed/item.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__public_experience_detail_feed_item__ = __webpack_require__("./src/app/public/experience/detail-feed/item.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__public_experience_detail_feed_create_share_feed_index__ = __webpack_require__("./src/app/public/experience/detail-feed/create-share-feed/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__public_experience_detail_feed_create_review_index__ = __webpack_require__("./src/app/public/experience/detail-feed/create-review/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__public_experience_list_feed_xpcard_item__ = __webpack_require__("./src/app/public/experience/list-feed/xpcard/item.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__public_cinema_item__ = __webpack_require__("./src/app/public/cinema/item.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__public_cinema_cinemacard_item__ = __webpack_require__("./src/app/public/cinema/cinemacard/item.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__public_cinema_cinema_showtime_item__ = __webpack_require__("./src/app/public/cinema/cinema-showtime/item.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__private_infos_reset_password_verify_reset_password_verify__ = __webpack_require__("./src/app/private/infos/reset-password-verify/reset-password-verify.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__private_infos_reset_password_success_reset_password_success__ = __webpack_require__("./src/app/private/infos/reset-password-success/reset-password-success.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__private_infos_username_username__ = __webpack_require__("./src/app/private/infos/username/username.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__private_infos_birthday_birthday__ = __webpack_require__("./src/app/private/infos/birthday/birthday.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__private_infos_avatar_avatar__ = __webpack_require__("./src/app/private/infos/avatar/avatar.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__private_infos_contact_contact__ = __webpack_require__("./src/app/private/infos/contact/contact.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__private_connect_connect__ = __webpack_require__("./src/app/private/connect/connect.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__private_connect_message_message__ = __webpack_require__("./src/app/private/connect/message/message.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__private_connect_conversation_conversation__ = __webpack_require__("./src/app/private/connect/conversation/conversation.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__private_connect_typing_message_typing__ = __webpack_require__("./src/app/private/connect/typing-message/typing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__private_upload_upload__ = __webpack_require__("./src/app/private/upload/upload.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__private_upload_create_upload_create__ = __webpack_require__("./src/app/private/upload/create-upload/create.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__private_upload_my_uploads_uploads__ = __webpack_require__("./src/app/private/upload/my-uploads/uploads.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__private_upload_latest_uploads_latest__ = __webpack_require__("./src/app/private/upload/latest-uploads/latest.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__private_profile_profile__ = __webpack_require__("./src/app/private/profile/profile.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__private_profile_info_my_profile__ = __webpack_require__("./src/app/private/profile/info/my-profile.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__private_profile_setting_setting__ = __webpack_require__("./src/app/private/profile/setting/setting.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__private_profile_preference_pref__ = __webpack_require__("./src/app/private/profile/preference/pref.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__private_profile_invitation_invitation__ = __webpack_require__("./src/app/private/profile/invitation/invitation.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__private_profile_calendar_calendar__ = __webpack_require__("./src/app/private/profile/calendar/calendar.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__private_profile_save_item_save_item__ = __webpack_require__("./src/app/private/profile/save-item/save-item.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__private_profile_timeline_timeline__ = __webpack_require__("./src/app/private/profile/timeline/timeline.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__private_payment_payment__ = __webpack_require__("./src/app/private/payment/payment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__store_app__ = __webpack_require__("./src/app/store/app.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__services_ui__ = __webpack_require__("./src/app/services/ui.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__utils_util__ = __webpack_require__("./src/app/utils/util.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__public_discover_trending_detail_feed_item__ = __webpack_require__("./src/app/public/discover/trending/detail-feed/item.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










//General components




//Public components






//Discover Page







//Experience Page








//Cinema Page



//Info Components






//Connect Page




//Upload page




//Profile Page








//Payment Page

//Store data


//Services




var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_6_angular_bootstrap_md__["a" /* MDBBootstrapModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_61__ngrx_store__["b" /* StoreModule */].forRoot(__WEBPACK_IMPORTED_MODULE_62__store_app__["a" /* default */]),
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["d" /* RouterModule */].forRoot([
                    {
                        path: '',
                        redirectTo: '/cinema',
                        pathMatch: 'full'
                    },
                    {
                        path: 'home',
                        component: __WEBPACK_IMPORTED_MODULE_14__public_home_home__["a" /* HomePage */]
                    },
                    {
                        path: 'discover',
                        component: __WEBPACK_IMPORTED_MODULE_20__public_discover_discover__["a" /* DiscoverPage */]
                    },
                    {
                        path: 'trending/feed/:id',
                        component: __WEBPACK_IMPORTED_MODULE_66__public_discover_trending_detail_feed_item__["a" /* DetailTrendingPage */]
                    },
                    {
                        path: 'special/feed/:id',
                        component: __WEBPACK_IMPORTED_MODULE_26__public_discover_special_detail_special_item__["a" /* DetailSpecialPage */]
                    },
                    {
                        path: 'cinema',
                        component: __WEBPACK_IMPORTED_MODULE_35__public_cinema_item__["a" /* CinemaPage */]
                    },
                    {
                        path: 'cinema/:id/showtimes',
                        component: __WEBPACK_IMPORTED_MODULE_37__public_cinema_cinema_showtime_item__["a" /* CinemaShowTimePage */]
                    },
                    {
                        path: 'experience',
                        component: __WEBPACK_IMPORTED_MODULE_27__public_experience_experience__["a" /* XPPage */]
                    },
                    {
                        path: 'experience/category/:id',
                        component: __WEBPACK_IMPORTED_MODULE_30__public_experience_list_feed_item__["a" /* ListXPPage */]
                    },
                    {
                        path: 'experience/feed/:id',
                        component: __WEBPACK_IMPORTED_MODULE_31__public_experience_detail_feed_item__["a" /* DetailXPPage */]
                    },
                    {
                        path: 'connect',
                        component: __WEBPACK_IMPORTED_MODULE_44__private_connect_connect__["a" /* ConnectPage */]
                    },
                    {
                        path: 'upload',
                        component: __WEBPACK_IMPORTED_MODULE_48__private_upload_upload__["a" /* UploadPage */]
                    },
                    {
                        path: 'profile',
                        component: __WEBPACK_IMPORTED_MODULE_52__private_profile_profile__["a" /* ProfilePage */]
                    },
                    {
                        path: 'password_reset',
                        component: __WEBPACK_IMPORTED_MODULE_18__public_password_reset_password_reset__["a" /* ResetPasswordPage */]
                    },
                    {
                        path: 'registration',
                        component: __WEBPACK_IMPORTED_MODULE_17__public_registration_registration__["a" /* RegistrationPage */]
                    },
                    {
                        path: 'verification',
                        component: __WEBPACK_IMPORTED_MODULE_19__public_verification_verification__["a" /* VerificationPage */]
                    },
                    {
                        path: 'login',
                        component: __WEBPACK_IMPORTED_MODULE_15__public_login_login__["a" /* LoginPage */],
                    },
                    {
                        path: 'payment',
                        component: __WEBPACK_IMPORTED_MODULE_60__private_payment_payment__["a" /* PaymentPage */]
                    },
                    {
                        path: 'business',
                        component: __WEBPACK_IMPORTED_MODULE_16__public_business_business__["a" /* BusinessPage */]
                    }
                ]),
                __WEBPACK_IMPORTED_MODULE_5__agm_core__["a" /* AgmCoreModule */].forRoot({
                    apiKey: 'AIzaSyBgXAUJIbUHsu3wugHyC-Z5p070xjLn62E'
                }),
                __WEBPACK_IMPORTED_MODULE_7_angularfire2__["AngularFireModule"].initializeApp(__WEBPACK_IMPORTED_MODULE_9_environments_environment__["a" /* environment */].firebase),
                __WEBPACK_IMPORTED_MODULE_8_angularfire2_database__["AngularFireDatabaseModule"]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NO_ERRORS_SCHEMA */]],
            providers: [__WEBPACK_IMPORTED_MODULE_63__services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_64__services_ui__["a" /* UiService */], __WEBPACK_IMPORTED_MODULE_65__utils_util__["a" /* UtilService */]],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_10__app__["a" /* AppRoot */],
                __WEBPACK_IMPORTED_MODULE_11__nav_nav__["a" /* Nav */],
                __WEBPACK_IMPORTED_MODULE_12__footer_footer__["a" /* Footer */],
                __WEBPACK_IMPORTED_MODULE_13__loading_loading__["a" /* AppLoading */],
                __WEBPACK_IMPORTED_MODULE_14__public_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_15__public_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_16__public_business_business__["a" /* BusinessPage */],
                __WEBPACK_IMPORTED_MODULE_18__public_password_reset_password_reset__["a" /* ResetPasswordPage */],
                __WEBPACK_IMPORTED_MODULE_17__public_registration_registration__["a" /* RegistrationPage */],
                __WEBPACK_IMPORTED_MODULE_19__public_verification_verification__["a" /* VerificationPage */],
                __WEBPACK_IMPORTED_MODULE_38__private_infos_reset_password_verify_reset_password_verify__["a" /* ResetPasswordVerify */],
                __WEBPACK_IMPORTED_MODULE_39__private_infos_reset_password_success_reset_password_success__["a" /* ResetPasswordSuccess */],
                __WEBPACK_IMPORTED_MODULE_40__private_infos_username_username__["a" /* UsernameInfo */],
                __WEBPACK_IMPORTED_MODULE_41__private_infos_birthday_birthday__["a" /* BirthdayInfo */],
                __WEBPACK_IMPORTED_MODULE_42__private_infos_avatar_avatar__["a" /* AvatarInfo */],
                __WEBPACK_IMPORTED_MODULE_43__private_infos_contact_contact__["a" /* ContactInfo */],
                __WEBPACK_IMPORTED_MODULE_20__public_discover_discover__["a" /* DiscoverPage */],
                __WEBPACK_IMPORTED_MODULE_21__public_discover_trending_trending__["a" /* TrendingItem */],
                __WEBPACK_IMPORTED_MODULE_22__public_discover_trending_card_item__["a" /* TrendingCardItem */],
                __WEBPACK_IMPORTED_MODULE_23__public_discover_trending_map_map__["a" /* TrendingDetailMap */],
                __WEBPACK_IMPORTED_MODULE_66__public_discover_trending_detail_feed_item__["a" /* DetailTrendingPage */],
                __WEBPACK_IMPORTED_MODULE_24__public_discover_special_special__["a" /* SpecialItem */],
                __WEBPACK_IMPORTED_MODULE_25__public_discover_special_card_item__["a" /* SpecialCardItem */],
                __WEBPACK_IMPORTED_MODULE_26__public_discover_special_detail_special_item__["a" /* DetailSpecialPage */],
                __WEBPACK_IMPORTED_MODULE_27__public_experience_experience__["a" /* XPPage */],
                __WEBPACK_IMPORTED_MODULE_28__public_experience_category_category__["a" /* CategoryItem */],
                __WEBPACK_IMPORTED_MODULE_30__public_experience_list_feed_item__["a" /* ListXPPage */],
                __WEBPACK_IMPORTED_MODULE_31__public_experience_detail_feed_item__["a" /* DetailXPPage */],
                __WEBPACK_IMPORTED_MODULE_32__public_experience_detail_feed_create_share_feed_index__["a" /* NewShareFeed */],
                __WEBPACK_IMPORTED_MODULE_33__public_experience_detail_feed_create_review_index__["a" /* ShareFeedReview */],
                __WEBPACK_IMPORTED_MODULE_29__public_experience_filter_filter__["a" /* XpFilter */],
                __WEBPACK_IMPORTED_MODULE_34__public_experience_list_feed_xpcard_item__["a" /* XPCardItem */],
                __WEBPACK_IMPORTED_MODULE_35__public_cinema_item__["a" /* CinemaPage */],
                __WEBPACK_IMPORTED_MODULE_36__public_cinema_cinemacard_item__["a" /* CinemaCardItem */],
                __WEBPACK_IMPORTED_MODULE_37__public_cinema_cinema_showtime_item__["a" /* CinemaShowTimePage */],
                __WEBPACK_IMPORTED_MODULE_44__private_connect_connect__["a" /* ConnectPage */],
                __WEBPACK_IMPORTED_MODULE_48__private_upload_upload__["a" /* UploadPage */],
                __WEBPACK_IMPORTED_MODULE_49__private_upload_create_upload_create__["a" /* CreateNewUpload */],
                __WEBPACK_IMPORTED_MODULE_51__private_upload_latest_uploads_latest__["a" /* LatestUpload */],
                __WEBPACK_IMPORTED_MODULE_50__private_upload_my_uploads_uploads__["a" /* MyUpload */],
                __WEBPACK_IMPORTED_MODULE_45__private_connect_message_message__["a" /* Message */],
                __WEBPACK_IMPORTED_MODULE_46__private_connect_conversation_conversation__["a" /* ConversationCard */],
                __WEBPACK_IMPORTED_MODULE_47__private_connect_typing_message_typing__["a" /* Typing */],
                __WEBPACK_IMPORTED_MODULE_52__private_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_53__private_profile_info_my_profile__["a" /* MyProfileInfo */],
                __WEBPACK_IMPORTED_MODULE_54__private_profile_setting_setting__["a" /* AccountSettingInfo */],
                __WEBPACK_IMPORTED_MODULE_55__private_profile_preference_pref__["a" /* PreferenceSettingInfo */],
                __WEBPACK_IMPORTED_MODULE_56__private_profile_invitation_invitation__["a" /* InvitationSettingInfo */],
                __WEBPACK_IMPORTED_MODULE_57__private_profile_calendar_calendar__["a" /* CalendarInfo */],
                __WEBPACK_IMPORTED_MODULE_58__private_profile_save_item_save_item__["a" /* SavedItemInfo */],
                __WEBPACK_IMPORTED_MODULE_58__private_profile_save_item_save_item__["a" /* SavedItemInfo */],
                __WEBPACK_IMPORTED_MODULE_59__private_profile_timeline_timeline__["a" /* TimelineInfo */],
                __WEBPACK_IMPORTED_MODULE_60__private_payment_payment__["a" /* PaymentPage */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_10__app__["a" /* AppRoot */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoot; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppRoot = /** @class */ (function () {
    function AppRoot(store, apiService, router) {
        var _this = this;
        this.store = store;
        this.apiService = apiService;
        this.router = router;
        console.log('Root Page');
        store.select('loading').subscribe(function (l) {
            _this.loading = l;
        });
        store.select('user').subscribe(function (usr) {
            if (!_this.user && usr) {
                apiService.getUser();
                apiService.setLocation();
            }
            _this.user = usr;
        });
        if (localStorage.getItem('access_token')) {
            apiService.getUser();
        }
        else {
            apiService.getLocation();
        }
    }
    AppRoot.prototype.ngOnInit = function () {
    };
    AppRoot = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.html"),
            styles: [__webpack_require__("./src/app/app.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], AppRoot);
    return AppRoot;
}());



/***/ }),

/***/ "./src/app/footer/footer.css":
/***/ (function(module, exports) {

module.exports = ".app-footer {\n  width: 100%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 10px;\n}\n\n.app-footer > h2 {\n  margin: auto;\n  font-size: 16px;\n}"

/***/ }),

/***/ "./src/app/footer/footer.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-footer\">\n  <h2>Copyright 2018 by Spinshare</h2>\n</div>\n"

/***/ }),

/***/ "./src/app/footer/footer.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Footer; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var Footer = /** @class */ (function () {
    function Footer() {
    }
    Footer = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-footer',
            template: __webpack_require__("./src/app/footer/footer.html"),
            styles: [__webpack_require__("./src/app/footer/footer.css")]
        })
    ], Footer);
    return Footer;
}());



/***/ }),

/***/ "./src/app/loading/loading.css":
/***/ (function(module, exports) {

module.exports = ".loading {\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100vw;\n  height: 100vh;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.loading > i {\n  margin: auto;\n  font-size: 50px;\n  color: black;\n  -webkit-animation: spin 2s linear infinite; /* Safari */\n  animation: spin 2s linear infinite;\n}\n\n/* Safari */\n\n@-webkit-keyframes spin {\n0% { -webkit-transform: rotate(0deg); }\n100% { -webkit-transform: rotate(360deg); }\n}\n\n@keyframes spin {\n0% { -webkit-transform: rotate(0deg); transform: rotate(0deg); }\n100% { -webkit-transform: rotate(360deg); transform: rotate(360deg); }\n}"

/***/ }),

/***/ "./src/app/loading/loading.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"loading\">\n  <i class=\"fas fa-spinner\"></i>\n</div>"

/***/ }),

/***/ "./src/app/loading/loading.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppLoading; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppLoading = /** @class */ (function () {
    function AppLoading() {
    }
    AppLoading.prototype.ngOnInit = function () {
    };
    AppLoading = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-loading',
            template: __webpack_require__("./src/app/loading/loading.html"),
            styles: [__webpack_require__("./src/app/loading/loading.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AppLoading);
    return AppLoading;
}());



/***/ }),

/***/ "./src/app/nav/nav.css":
/***/ (function(module, exports) {

module.exports = ".app-nav {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  height: 50px;\n  background: #F0F8FF;\n  color: #000;\n  overflow-x: none;\n  position: fixed;\n  top: 0;\n  z-index: 999;\n}\n\n.app-nav>div {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin: auto;\n  width: 80vw;\n  min-width: 1200px;\n}\n\n.app-nav>div>.icon>img {\n  max-width: 100px;\n  margin-top: 13px;\n  margin-left: 5px;\n}\n\n.app-nav>div>div {\n  margin: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.app-nav>div>div>.route {\n  margin: auto 5px;\n}\n\n.app-nav>div>div>.route>a {\n  font-size: 20px;\n  line-height: normal;\n  text-align: center;\n  color: #5603AD;\n  padding: 5px 20px;\n}\n\n.app-nav>div>div>.route>a:hover {\n  color: #000;\n}\n\n.app-nav>div>div>.route>a.active {\n  color: #5603AD;\n  border-bottom: 1px solid #5603AD;\n  margin-bottom: 1px;\n}\n\n.app-nav>div>div>.username {\n  position: relative;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin: auto 5px;\n  cursor: pointer;\n  -webkit-user-select: none;\n     -moz-user-select: none;\n      -ms-user-select: none;\n          user-select: none;\n}\n\n.app-nav>div>div>.username>a {\n  font-size: 14px;\n  margin: auto;\n  padding-right: 5px;\n  color: #5603AD;\n}\n\n.app-nav>div>div>.username>img {\n  border: solid 1px rgb(161, 161, 161);\n  border-radius: 50%;\n  margin: auto;\n  width: 40px;\n  height: 40px;\n}\n\n.app-nav>div>div>.username>i {\n  margin: auto 10px;\n  font-size: 20px;\n}\n\n.app-nav>div>div>.username>div {\n  position: absolute;\n  min-width: 100px;\n  top: calc(5vh - 5px);\n  left: -20px;\n  border: solid 1px #c1c1c1;\n  background-color: white;\n  padding: 10px 25px 10px 25px;\n  -webkit-box-shadow: 0px 3px 5px #797979;\n          box-shadow: 0px 3px 5px #797979;\n}\n\n.app-nav>div>div>.username>div>i {\n  position: absolute;\n  top: -15px;\n  right: 5px;\n  font-size: 20px;\n  color: #848484;\n}\n\n.app-nav>div>div>.username>div>a {\n  padding: 10px 0px;\n}\n\n.app-nav>div>div>.username>div>a:hover {\n  color: #5603AD;\n}\n\n.app-nav>div>div>.username>div>a>i {\n  margin-right: 10px;\n  font-size: 20px;\n}"

/***/ }),

/***/ "./src/app/nav/nav.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-nav\">\n  <div>\n    <!-- <a class=\"icon\" routerLink=\"/home\">\n      <img src=\"../../assets/graphics/logotext.png\">\n    </a> -->\n    <div style=\"flex: 1;\">\n      <!-- <div class=\"route\" style=\"margin: auto 5px auto auto;\">\n        <a routerLink=\"/discover\" routerLinkActive=\"active\">Deal</a>\n      </div> -->\n      <!-- <div class=\"route\" style=\"margin: auto 5px auto auto;\">\n        <a routerLink=\"/experience\" routerLinkActive=\"active\">Discover</a>\n      </div> -->\n      <div class=\"route\" [ngStyle]=\"{'margin' : user ? 'auto 5px' : 'auto auto auto 5px'}\">\n        <a routerLink=\"/cinema\" routerLinkActive=\"active\">Cinemas in your area</a>\n      </div>\n      <!-- <div class=\"route\" *ngIf=\"!user\">\n        <a routerLink=\"/business\" routerLinkActive=\"active\">Business Registration</a>\n      </div> -->\n      <!-- <div class=\"route\" style=\"margin: auto 5px;\" (click)=\"gotoPrivateRoute('connect')\">\n        <a routerLinkActive=\"active\">Connect</a>\n      </div> -->\n      <!-- <div class=\"route\" style=\"margin: auto auto auto 5px;\" (click)=\"gotoPrivateRoute('upload')\">\n        <a routerLinkActive=\"active\">Upload</a>\n      </div> -->\n    </div>\n    <!-- <div>\n      <div class=\"route\" *ngIf=\"!user\">\n        <a routerLink=\"/login\" routerLinkActive=\"active\" style=\"display: flex;\">\n          <i class=\"fas fa-user-circle\" style=\"font-size: 1.6rem; margin: auto 5px;\"></i> Login\n        </a>\n      </div>\n      <div class=\"username\" (click)=\"toggleOption()\" *ngIf=\"user\">\n        <a>{{user?.username}}</a>\n        <img src=\"{{user?.profile_pic ? user.profile_pic : 'assets/graphics/user-avatar-placeholder.png'}}\" />\n        <i class=\"fas fa-caret-down\" [ngStyle]=\"{'color': showMore ? '#848484' : 'black'}\"></i>\n        <div *ngIf=\"showMore\">\n          <a routerLink=\"/profile\" routerLinkActive=\"active\" style=\"cursor: pointer;\">\n            <i class=\"fas fa-info-circle\"></i>Profile</a>\n          <hr>\n          <a (click)=\"logout()\" style=\"cursor: pointer;\">\n            <i class=\"fas fa-sign-out-alt\"></i>Logout</a>\n          <i class=\"fas fa-caret-up\"></i>\n        </div>\n      </div>\n    </div> -->\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/nav/nav.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Nav; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Nav = /** @class */ (function () {
    function Nav(router, apiService, uiService, store, _eref) {
        var _this = this;
        this.router = router;
        this.apiService = apiService;
        this.uiService = uiService;
        this.store = store;
        this._eref = _eref;
        this.showMore = false;
        store.select('user').subscribe(function (usr) {
            if (!_this.user && usr) {
                _this.user = usr;
                if (_this.route) {
                    _this.router.navigate(['/' + _this.route]);
                }
            }
        });
    }
    Nav.prototype.logout = function () {
        this.router.navigate(['/cinema']);
        this.user = null;
        this.uiService.logout();
    };
    Nav.prototype.toggleOption = function () {
        this.showMore = !this.showMore;
    };
    Nav.prototype.onScreenClick = function (event) {
        if (!this._eref.nativeElement.contains(event.target) && this.showMore) {
            this.showMore = false;
        }
    };
    Nav.prototype.ngOnInit = function () {
    };
    Nav.prototype.gotoPrivateRoute = function (route) {
        if (!this.user) {
            this.router.navigate(['/login']);
            this.route = route;
        }
        else {
            this.router.navigate(['/' + route]);
        }
    };
    Nav = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-nav',
            template: __webpack_require__("./src/app/nav/nav.html"),
            styles: [__webpack_require__("./src/app/nav/nav.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_1_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_2_app_services_ui__["a" /* UiService */], __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], Nav);
    return Nav;
}());



/***/ }),

/***/ "./src/app/private/connect/connect.css":
/***/ (function(module, exports) {

module.exports = ".connect {\n  position: relative;\n  min-width: 960px;\n  max-width: 1280px;\n  margin: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.connect > .list-conv {\n  width: 30%;\n  margin: 5px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  overflow-y: auto;\n  height: 90vh;\n}\n\n.connect > .content-conv {\n  width: 70%;\n  height: 100%;\n  margin: 5px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.content-conv > .messages {\n  -webkit-box-flex: 1;\n      -ms-flex-positive: 1;\n          flex-grow: 1;\n  background-color: white;\n  border: 1px solid rgba(0, 0, 0, 0.125);\n}\n\n.content-conv > .funcs {\n  height: 40px;\n  margin: 5px auto;\n  border: 1px solid rgba(0, 0, 0, 0.125);\n  width: 100%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  background-color: white;\n}\n\n.content-conv > .funcs > i {\n  font-size: 25px;\n  margin: auto 5px;\n}\n\n.messages {\n  width: 100%;\n  height: calc(100vh - 300px);\n  background-color: white;\n  margin: 0px auto;\n  padding: 10px;\n}\n\n.connect > .content-conv > .typing-box {\n  height: 100px;\n  background-color: white;\n  outline: none;\n  border: 1px solid rgba(0, 0, 0, 0.125);\n  padding: 5px 10px;\n}\n\n.group-info {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 0.75rem 1.25rem;\n}\n\n.group-info > img {\n  width: 50px;\n  height: 50px;\n  border-radius: 50%;\n  margin-right: 10px;\n}\n\n.group-info > span {\n  width: 60px;\n  height: 50px;\n  border-radius: 50%;\n  margin-right: 10px;\n  border: 1px solid #9c9c9c;\n}\n\n.group-info > label {\n  cursor: pointer;\n  font-size: 1.3rem;\n  position: absolute;\n  top: 43px;\n  left: 50px;\n}\n\n.group-info > #imageSlt {\n  opacity: 0;\n  position: absolute;\n  z-index: -1;\n}"

/***/ }),

/***/ "./src/app/private/connect/connect.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"connect\">\n  <ul class=\"list-group list-conv\">\n    <li style=\"height: 50px; text-align: center; padding: 10px; background-color: white; border: 1px solid rgba(0, 0, 0, 0.125); margin-bottom: 5px;\">\n      <span>Conversations</span>\n      <i class=\"fas fa-edit\" style=\"float: right; cursor: pointer;\" (click)=\"openFriendsModal()\"></i>\n    </li>\n    <li *ngFor=\"let conv of conversations\" class=\"list-group-item d-flex justify-content-between align-items-center\" style=\"cursor: pointer; padding: 5px 10px; margin-bottom: 5px;\"\n      [ngStyle]=\"conv.isSelected ? {'background-color': '#d9eeff'}:{}\">\n      <conversation [conversation]=\"conv\" [user]=\"user\" *ngIf=\"user && conv\" style=\"width: 100%;\"></conversation>\n      <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 20px;\" *ngIf=\"conv.isSelected\"></span>\n    </li>\n  </ul>\n  <div class=\"content-conv\">\n    <div style=\"height: 50px; text-align: center; padding: 10px; background-color: white; border: 1px solid rgba(0, 0, 0, 0.125); margin-bottom: 5px;\">Messages</div>\n    <div class=\"messages\">\n      <messages></messages>\n      <typing-message [conversation]=\"conversation\" [user]=\"user\" *ngIf=\"conversation && user\"></typing-message>\n    </div>\n    <div class=\"funcs\">\n      <i class=\"fa fa-paperclip\"></i>\n    </div>\n    <textarea style=\"height: 100px;\" class=\"typing-box\" placeholder=\"Type your message ...\" (keydown)=\"sendMessage($event)\" [(ngModel)]=\"inputMessage\"></textarea>\n  </div>\n</div>\n\n<div mdbModal #friendModal=\"mdb-modal\" class=\"modal fade\" tabindex=\"-1\" style=\"top: 15%;\" role=\"dialog\" aria-hidden=\"true\"\n  [config]=\"{backdrop: true, ignoreBackdropClick: false, show: false}\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title w-100\">Select your friend</h4>\n      </div>\n      <div class=\"modal-body\" style=\"max-height: 400px; overflow-y: auto;\">\n        <div class=\"group-info\" *ngIf=\"showGrpInfo\">\n          <img src=\"{{grpAvatarUrl}}\" *ngIf=\"grpAvatarUrl\" />\n          <span *ngIf=\"!grpAvatarUrl\"></span>\n          <input class=\"form-control\" [(ngModel)]=\"grpName\" style=\"padding: 0.5rem\" placeholder=\"Group name\" />\n          <label for=\"imageSlt\" class=\"fas fa-cloud-upload-alt\" [ngStyle]=\"{color: !grpAvatarUrl ? '#808080' : 'transparent'}\"></label>\n          <input id=\"imageSlt\" type=\"file\" accept=\".jpg,.png\" (change)=\"onGrpAvatarSlt($event)\"\n          />\n        </div>\n        <ul class=\"list-group\" *ngIf=\"user\">\n          <li class=\"list-group-item\" *ngFor=\"let friend of user.friends\" (click)=\"sltFriend(friend.id)\" style=\"display: flex;\">\n            <a>\n              <img style=\"width: 50px; height: 50px; border-radius: 50%;\" src=\"{{friend.picture}}\" *ngIf=\"friend.picture\" />\n              <img style=\"width: 50px; height: 50px; border-radius: 50%;\" src=\"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDUwIDUwIiBoZWlnaHQ9IjUwcHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1MCA1MCIgd2lkdGg9IjUwcHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxjaXJjbGUgY3g9IjI1IiBjeT0iMjUiIGZpbGw9Im5vbmUiIHI9IjI0IiBzdHJva2U9IiMwMDAwMDAiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBzdHJva2Utd2lkdGg9IjIiLz48cmVjdCBmaWxsPSJub25lIiBoZWlnaHQ9IjUwIiB3aWR0aD0iNTAiLz48cGF0aCBkPSJNMjkuOTMzLDM1LjUyOGMtMC4xNDYtMS42MTItMC4wOS0yLjczNy0wLjA5LTQuMjFjMC43My0wLjM4MywyLjAzOC0yLjgyNSwyLjI1OS00Ljg4OGMwLjU3NC0wLjA0NywxLjQ3OS0wLjYwNywxLjc0NC0yLjgxOCAgYzAuMTQzLTEuMTg3LTAuNDI1LTEuODU1LTAuNzcxLTIuMDY1YzAuOTM0LTIuODA5LDIuODc0LTExLjQ5OS0zLjU4OC0xMi4zOTdjLTAuNjY1LTEuMTY4LTIuMzY4LTEuNzU5LTQuNTgxLTEuNzU5ICBjLTguODU0LDAuMTYzLTkuOTIyLDYuNjg2LTcuOTgxLDE0LjE1NmMtMC4zNDUsMC4yMS0wLjkxMywwLjg3OC0wLjc3MSwyLjA2NWMwLjI2NiwyLjIxMSwxLjE3LDIuNzcxLDEuNzQ0LDIuODE4ICBjMC4yMiwyLjA2MiwxLjU4LDQuNTA1LDIuMzEyLDQuODg4YzAsMS40NzMsMC4wNTUsMi41OTgtMC4wOTEsNC4yMWMtMS4yNjEsMy4zOS03LjczNywzLjY1NS0xMS40NzMsNi45MjQgIGMzLjkwNiwzLjkzMywxMC4yMzYsNi43NDYsMTYuOTE2LDYuNzQ2czE0LjUzMi01LjI3NCwxNS44MzktNi43MTNDMzcuNjg4LDM5LjE4NiwzMS4xOTcsMzguOTMsMjkuOTMzLDM1LjUyOHoiLz48L3N2Zz4=\"\n                *ngIf=\"!friend.picture\" />\n              <span>{{friend.username}}</span>\n            </a>\n            <i *ngIf=\"friend.isSelected\" class=\"fas fa-check\" style=\"color: #2196F3; float: right; font-size: 20px; margin: auto; margin-right: 5px;\"></i>\n          </li>\n        </ul>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary waves-light\" aria-label=\"Close\" (click)=\"friendModal.hide()\" mdbRippleRadius>Close</button>\n        <button type=\"button\" class=\"btn btn-secondary waves-light\" aria-label=\"Close\" (click)=\"createConversation()\" mdbRippleRadius>Create</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/private/connect/connect.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConnectPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__("./node_modules/angularfire2/database/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular_bootstrap_md__ = __webpack_require__("./node_modules/angular-bootstrap-md/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_ui__ = __webpack_require__("./src/app/services/ui.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ConnectPage = /** @class */ (function () {
    function ConnectPage(apiService, uiService, store, router, firebase) {
        var _this = this;
        this.apiService = apiService;
        this.uiService = uiService;
        this.store = store;
        this.router = router;
        this.firebase = firebase;
        this.inputMessage = null;
        this.friends = [];
        this.grpAvatarUrl = null;
        this.grpAvatar = null;
        this.grpName = null;
        this.showGrpInfo = false;
        console.log('Connect');
        //Subcribe user
        store.select('user').subscribe(function (u) {
            if (!_this.user && u) {
                //Fetching all conversations
                _this.apiService.fetchConversation(1);
                //Fetching all friends
                _this.apiService.fetchFriends();
            }
            //Set user data
            _this.user = u;
        });
        //Subcribe conversations
        store.select('conversations').subscribe(function (convs) {
            _this.conversations = convs;
            _this.conversation = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](convs, function (item) {
                return item.isSelected;
            });
        });
    }
    ConnectPage.prototype.openFriendsModal = function () {
        this.friends = this.user.friends;
        __WEBPACK_IMPORTED_MODULE_0_underscore__["each"](this.friends, function (f) { return f.isSelected; });
        this.friendModal.show();
    };
    ConnectPage.prototype.sltFriend = function (id) {
        var friend = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](this.friends, function (f) { return f.id == id; });
        if (friend)
            friend.isSelected = !friend.isSelected;
        if (__WEBPACK_IMPORTED_MODULE_0_underscore__["filter"](this.friends, function (f) { return f.isSelected; }).length > 1) {
            this.showGrpInfo = true;
        }
        else {
            this.showGrpInfo = false;
        }
    };
    ConnectPage.prototype.createConversation = function () {
        var sltFriends = __WEBPACK_IMPORTED_MODULE_0_underscore__["filter"](this.friends, function (f) { return f.isSelected; });
        if (sltFriends.length == 1) {
            this.apiService.createPersonalConversation(this.user, sltFriends);
        }
        else {
            this.apiService.createGroupConversation(sltFriends, this.grpName, this.grpAvatar, this.user);
        }
        this.friendModal.hide();
    };
    ConnectPage.prototype.sendMessage = function (e) {
        var _this = this;
        if (e.keyCode == 13 && !e.shiftKey) {
            //Send to Firebase
            var message = {
                content: this.inputMessage,
                contentType: 'text',
                seen: false,
                seenby: (_a = {},
                    _a[this.user.id.toString()] = new Date().getTime(),
                    _a),
                sender: this.user.id,
                time: new Date().getTime()
            };
            if (!this.conversation.isGroup) {
                this.firebase.list('conversations/' + this.user.id + '/' + this.conversation.partnerID).push(message);
                this.firebase.list('conversations/' + this.conversation.partnerID + '/' + this.user.id).push(message);
                //For backend
                this.apiService.saveConversation({
                    conversationID: this.conversation.partnerID,
                    senderID: this.conversation.senderID,
                    msg: this.inputMessage,
                    contentType: 'text',
                    isGroup: false
                });
            }
            else {
                //TODO: Send message to group. Firebase structure: [memberId]/[groupId]/msg -> [userId]/[conversationId]/msg
                this.firebase.list('conversations/' + this.user.id + '/' + this.conversation.id).push(message);
                __WEBPACK_IMPORTED_MODULE_0_underscore__["each"](this.conversation.participants, function (participant) {
                    if (participant.id == _this.user.id)
                        return;
                    _this.firebase.list('conversations/' + participant.id + '/' + _this.conversation.id).push(message);
                });
                //For backend
                this.apiService.saveConversation({
                    conversationID: this.conversation.id,
                    senderID: this.conversation.senderID,
                    msg: this.inputMessage,
                    contentType: 'text',
                    isGroup: true
                });
            }
            //Reset input message box
            this.inputMessage = null;
            e.preventDefault();
        }
        var _a;
    };
    ConnectPage.prototype.onGrpAvatarSlt = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var src = e.target.result;
                _this.grpAvatarUrl = src;
            };
            reader.readAsDataURL(event.target.files[0]);
            this.grpAvatar = event.target.files[0];
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_11" /* ViewChild */])('friendModal'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_angular_bootstrap_md__["b" /* ModalDirective */])
    ], ConnectPage.prototype, "friendModal", void 0);
    ConnectPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/private/connect/connect.html"),
            styles: [__webpack_require__("./src/app/private/connect/connect.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_7__services_ui__["a" /* UiService */], __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["AngularFireDatabase"]])
    ], ConnectPage);
    return ConnectPage;
}());



/***/ }),

/***/ "./src/app/private/connect/conversation/conversation.css":
/***/ (function(module, exports) {

module.exports = ".conv-item {\n  width: 100%;\n}\n\n.conv-item > .avatar > img {\n  width: 60px;\n  height: 60px;\n  border-radius: 50%;\n}\n\n.conv-item > .content {\n  margin-top: 5px;\n}\n\n.conv-item > .content > h5 {\n  margin-bottom: 0px;\n  font-weight: 600;\n}\n\n.conv-item > .content > p {\n  margin-bottom: 0px;\n  max-width: 200px;\n  overflow: hidden;\n  white-space: nowrap;\n  text-overflow: ellipsis;\n  color: #696969;\n}"

/***/ }),

/***/ "./src/app/private/connect/conversation/conversation.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"media conv-item\" (click)=\"select()\">\n  <div class=\"avatar\">\n    <img class=\"d-flex mr-3\" src=\"{{conversation.partnerPicture}}\" alt=\"###\" *ngIf=\"conversation.partnerPicture\">\n    <img class=\"d-flex mr-3\" src=\"data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDUwIDUwIiBoZWlnaHQ9IjUwcHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1MCA1MCIgd2lkdGg9IjUwcHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxjaXJjbGUgY3g9IjI1IiBjeT0iMjUiIGZpbGw9Im5vbmUiIHI9IjI0IiBzdHJva2U9IiMwMDAwMDAiIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBzdHJva2Utd2lkdGg9IjIiLz48cmVjdCBmaWxsPSJub25lIiBoZWlnaHQ9IjUwIiB3aWR0aD0iNTAiLz48cGF0aCBkPSJNMjkuOTMzLDM1LjUyOGMtMC4xNDYtMS42MTItMC4wOS0yLjczNy0wLjA5LTQuMjFjMC43My0wLjM4MywyLjAzOC0yLjgyNSwyLjI1OS00Ljg4OGMwLjU3NC0wLjA0NywxLjQ3OS0wLjYwNywxLjc0NC0yLjgxOCAgYzAuMTQzLTEuMTg3LTAuNDI1LTEuODU1LTAuNzcxLTIuMDY1YzAuOTM0LTIuODA5LDIuODc0LTExLjQ5OS0zLjU4OC0xMi4zOTdjLTAuNjY1LTEuMTY4LTIuMzY4LTEuNzU5LTQuNTgxLTEuNzU5ICBjLTguODU0LDAuMTYzLTkuOTIyLDYuNjg2LTcuOTgxLDE0LjE1NmMtMC4zNDUsMC4yMS0wLjkxMywwLjg3OC0wLjc3MSwyLjA2NWMwLjI2NiwyLjIxMSwxLjE3LDIuNzcxLDEuNzQ0LDIuODE4ICBjMC4yMiwyLjA2MiwxLjU4LDQuNTA1LDIuMzEyLDQuODg4YzAsMS40NzMsMC4wNTUsMi41OTgtMC4wOTEsNC4yMWMtMS4yNjEsMy4zOS03LjczNywzLjY1NS0xMS40NzMsNi45MjQgIGMzLjkwNiwzLjkzMywxMC4yMzYsNi43NDYsMTYuOTE2LDYuNzQ2czE0LjUzMi01LjI3NCwxNS44MzktNi43MTNDMzcuNjg4LDM5LjE4NiwzMS4xOTcsMzguOTMsMjkuOTMzLDM1LjUyOHoiLz48L3N2Zz4=\" *ngIf=\"!conversation.isGroup && !conversation.partnerPicture\"/>\n    <img class=\"d-flex mr-3\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAZeElEQVR4Xu1de3RTR3q/c2XZ8kOSZVsYY4OoAUMgPJIsCc+NN17nxSZmQx5tk1NysmybPUsTSMhu9jSPJputs/SkC+RwsskmaTeQcqANbTchEFgeGwjhUUJIKGEhJkiWZcsYS5ZlWbKkOz1zj0ZnfD1zH9K1Itu6/1iWZubOfL/f9/u+mbkPwOWOMW0BMKZHnxs8lyPAGCdBjgA5AoxxC4zx4ecUIEeAMW6BMT78nALkCDDGLTDGh59TgBwBxrgFxvjwcwqQI8AYt8AYH35OAXIEGOMWGOPDzylAjgBj3AJjfPg5BcgRYOxZwGw2PwwAeBxCOEVm9P/L8/xTPT09p0azhcaMAthstrvi8fi7AMgPGUI4BG8I4Uu9vb3rRyMRRj0BbDbbTwRB+LUUPBoRaOCjevj7/Pz8GV1dXZ7RRIRRS4DS0tLvcRz3P2qAl5ZhqIBYDADwjN/v3zRaSDAqCWCz2VoghHaW3MuFATnwsRoAAH7n9/ufHA0kGDUEKC0tXcNx3AuAQFcKNM/zqjETBGFQWZIY6DMAoNnn8zWrbjBLC454AthsNhTff4LtS4IuBVwpASQxkioBSQj8m8Fg+E5XV9eFLMVWVbdGNAFsNlsgEZfFwZKAS8HWAj6Z+NFIgcmAiODz+SyqLJ2lhUYsAVjgY6DTJYCUBNIQgH5HRIAQtvl8vmuyFF/Fbo1IAthstlMAgGnY46WgqwFf7TSQBjz+jggFU71eb6eitbOwwIgjgNlsLjcajd8g8OWAJwHWKv8YJxr4pDIQRBC6urpKsxBfxS6NOAJUVFS4AACisVkEkANfDRmkCaDU42n/8zxf5fV6+xQtnmUFRhwB7HZ7MvFLhQAkcWhYsMBneL7YRKLOV52dnTdlGb6K3RlRBKisrGyCEG6Rej/N4/UIATTQpd+RatDZ2TniZgQjigATJkzYH4/H52MCqFEALYs/NHehzf9pIQB9F4/Hv9fV1TWidg9HFAEqKyup8k+b+qULvJQMUiLQSJCXl3fE7Xbfqai7WVRgxBEAgU0CLgVfb+BZRCAJQH7u6OgYUWEgWwjAl5eX38Jx3DSO44w8z3sMBsOfOjo6rpAAjB8/PoAJoAV4NZm/moSQLEOuBpIEaG9vV0MAYLVab+F5vg5CmA8h9MRisY/7+vq8mRaHb40Aie3ajQCAyTimkws7GDSe5xd0dHScQ2WqqqqSBCATQTmvTyUZlJsJyJEA1Wtvb7eiiYEUSLvd/t14PL6R4zjxKiRUlqEiF4xG4w+7u7tbM0GGjBOgtLT0RQjhGpqMSxd3UJmZM2cuPXjw4BlkjAkTJgzJAVjgpwK81OCshSA5EsTj8bler/cbXKaysvIZCOHPSLBZ6iEhRHswGJw+3CTIGAEsFsuDAIDXaBk8zfOx1E+bNm3J4cOHv0D1qqurkwRQAn64CEC7XoAEtLq6+rYTJ058ivqLQpactyf2EgYpgbS8wWCY4/f7Lw8XEYadAKWlpaWCILhowBMyLyZ2NFWYM2fOor17957FBEBlaOCzAFezL0AaV81CELkWgOtiEtTV1d1+4MCBozhksZJF9L1KJUBNLQgGg2IY1PsYNgKYzeZ/4zjuHlbGLgWfRZCGhobpW7dubUe/19TUiB4lt8+vlghKhlS7DyANB01NTbM3b97snDRpki0ajToV4r1qEgSDwUqO4/qV+q31d90JUFRUNJ/n+f1y0zXSi2nlMIgej2cZx3HHAABRNLBJkyaJBCAPNYtBmFxajKN1PwC37XQ6y1F/P/zww4JVq1aJsxiZhC8p/7RwIFWPYDCoZoahZZj6PifQYrHcJAjCPqnXq034SBU4d+5ck81mO5gA/gZBEF7lef5aNeDTANc6FVQTCkhgiX7d43K5/gghBI2NjW+dO3fuXrkwgAkiDQfSOuh/nudPBAKB72tCWKGwrgpQUlIyKEtnyT8t25eGAI/HYwUAwOrq6h8AAP6dFfdpkq817rNslKoK2O328lOnTkVffvnlJa+++uqHSgqAz4NVgFQNigqUcRwX04sEuhHAbDafhBBOT0f6MQmMRqPf5XJNSsR9dB1+CSvus0IATSm0Gi1VFXA6nSJ5582bt/jKlSu71SgALSlk1NN1eqgbAZD3K0m/dLrHUgij0djjdDonIsBqa2vdsVhsUOyTkkyN5A9XCMDeSvwVXC6XeL1CbW3tloGBAbSDyZzqSUFWCgWo3d7eXt1yAV0IUFxcfDsAYAcGJlXpJ+u1tbWJg1y6dOk7TqdzudSj5Tz/2woBCMzt27c/sHDhwt2J3CUglXU5eSfzAVoOgH8HAGwMBALPalU0WnldCFBRUbE7EoksJuO4lARyiz20epgA6AZOh8NxOh0C0BRCjfGUcgASTPTZZrOdOX369C1oFlBTU3Mzz/Pva5jrJ2cLSiqA+qXXjEAXApSUlPSgGzLkQoBa+Sfa2Ox2u3+RkFK0WVJCAilVACnILMlXCgW0lT5S5mmfUR2TydRx/vz5FQCAL7H3o78kmFLC4NBAi/WsaSGuk20EoO7SkbFaTeYvje1ut1sMAxDChQ6H4yM9CCBVEimgLGWgLQyRwI0bN+7gyZMnm1D9iRMnegAAImExCdQmgtIwwAoFgiAs7uvrE8mWzqGLApjNZtnpXyrgozqCILzidrtfQANctmzZb8+ePfvXWkiADaPk9WpAx2VIQMjvrFbr3jNnztxLej/ZLm2Kp6QIcipgsVj+sa2t7V/SAV+0ZboNoPqIADT5x99hArCSQ9ZCEWrb5XJhFZg0c+bMPaFQqGa45J+0BS0UyKlAVVXVtk8//fTvMkWA4uLiXR0dHX+VLn66EoCVA2ghAPZwct5PkOCGyZMni6uDSvP/VL1ejRrQiFBbW7v+wIEDL9EIQMsDSO9nhQfWwhAqb7PZDrtcLrRUntaRFQTAgEpBxSQQBOG/3W7336ByDodj0P2AJBmkn4czBGAA8d8FCxY8vH379p1qCaAmJ5AjgN1u/+OlS5fuSQv9TIcAEmiaB7MIQIYCWi5AA15vBSABp33es2fPX1xzzTVXa2pqynieH7R/r2YmQMsH5Agwd+7c9UeOHBEVJ50jbQWwWq1vC4JwL2sRKIXpnzge6dIvmQtMnjz5LAkw6zNpmFQIoTYPQOf55ptvxOXfurq6B8Lh8O+koLDWA5RCAZkIkiuKdrv9ty0tLT9LB3zRcdJpIN3lX5b0k8kj0b+3XC7XWjVhgBUK0hmr0r6A0+kUk9W6urq94XB4AXkupRyA5v1q9gYAAA2BQOBkOuNKmwBSWU91BZAEjUGA5Ixg5cqVvzx06NDjWqRfqwJoWRAaP3780ePHj9+O+kO7ZkEpBLBUgLYiSOYOgUCgGgDQm9UEwHIuXeRR8n4GCRwul8sHIRzvcDguDJf0Sw2qFArWrl27Ys2aNftoBFDr/TQVkCNAYWFhp9frnZoO+GmHgHHjxv05FApVseI/BlluIUjq+bQ5Ps4HeJ4/f/ny5RtRnXnz5h3u7u6eS/Nsrd6u1oisMOB0OisBAP2TJk1q4Djuv3B7cuBjwFnyj76XSwIDgUADACAt+U+bAF9//fXiefPm7ZZ6t/R/NRtBtBBAAonbIJLB2Q6H4xMSPDngUyGFmjBQUVHx2WeffVaf8H50LT+6L2DIHoAUcDngafGfLL9///4H5s+fL+44pnuklQNACPnJkycf7e7unilHAi0KQCMCOSOAEL7T2tq6GpWbNWvWF729vckbS2jGSAV4LSHA6XQuAgCIVy1L4/9wKUBPT08ZAECXq4LSIkCClXMtFsthGgHIEIA/q5n/K5GAUIHrHQ7HoeECnmyXpgZGo9HX0tLiQOUmTpz4S/T8YdL7WV6vRg3ktpEDgUB2XRACIVxqtVp30UAmkznpDEFJ9ln5AITwP1pbW3+E6tfX12+7dOlS2kuiqUip0+lsBAAcJ71f6vWpkkDumoCsIwAaJN4QYoFM5gFyaoCBoIFPyjmxVTzV4XB8lgqA6dQpKys7/fnnn9+c8P4nIYTP4/aUto6VSKF0EUlWEmD69Okftbe3L5STeGkuQFMApe+I33vcbrd43aDb7W5YtGhRMvtOB1i1dcnYj29YoQEvBzb+TbrFLEcAVDbrrglEA/nkk0/m33HHHfulAJKEoKkAWV4D+CJOEMKHPB7PH1Ayet99960/duzY30oBlLtzWC3YGBBcfvfu3ffOnj17L/q/urr6z+jGZRLodJRAacnYarVebG1tvUFt35XKpZ0EEoPNs1gs3awFHwwuiwRawcflBwYGJly5ciUIIay+/vrr3+ns7Ew+QkZp8Fp/R95XX1+/cevWreiZxLHq6ur7OY57U2mZWEoIVohQs2i0ePHil3fv3v1PWvvOKq8bAdAJGhoaNpw8efIRuWQvVRVgKYXb7bYBAOIJD5x17bXX/r6np6dOLwOR7UyZMmXXoUOH1gIAOtD311133bOdnZ1P6UUA1KbcVcTo956enmkAAN0eJKErASCENVarVbyLNRUSsFSABT6yl8fjGfSARgjh3BtvvPFFj8eD3heg21FeXv7FmTNnVgIAWgiPrqmurhbHK0cC8ne5BFFJ/vWO/6JddbNQoqHp06fvam9vX4pBoyWF6DepEqgFnyx3//33P79x48bfSMeA7lB67LHH7n7vvfd0uXYet+/xeIbMvxcuXPje5cuXG1MhAUkGNRtGTU1NzVu2bNH1EfW6EwBCOMNisZxQygVIEqQCPqrT3t4+GQDQTSMxhLB8woQJySd16EF02vN/IITTqqqqko+G0xoOyARTOhsg9//R50AgMAUAMOi5SemOS3cCoA7hXIAGrNxmj4zUi+Mk1wGWLVv28ttvv81MhmbNmpV/9erVrnQNRNZnPQFsxYoVzUeOHPkpERoGnZY1PVS7aITqNzY2bti5c+dzeo5nWEJAQgprLBbLOZb8s7ZxySmb0lav1+tlekNNTU1hLBZbDyFciQ2W6p4ACZ7X66UuwUIIqyorK9F0UBPwZNhg5Qb4+0AgUIeTTz1JMCwKkBjYoOVhlszTdvzklMBqtZ6/cOHCkwCAw6QhkMd3dXVt5jjuAZqB9CBAot09BoPhYY/HEyLPg5bDKysrxeVwmhLQ4j1JABoZcB2v1/uDwsLCj/UEPukYw9EobrO5ufnh5uZm8Q1bcku7LBKQ3ovuGG5ra0M3hqB7EMSnhqHDbrffDwB4U0/QaW3RNoN4nm/q6OgQL1PHpF++fPndR48eFe8PkAJMi/c04Mnv7rzzzle2bdv2IrrecDiwGjYFSAyi5MEHH1z7wQcfPEWSgEUIaRn0PwoLq1evXvf888+fwRsv6PuKiop/BgAkDU0aJ1VvV2NgxtVBTV1dXSIR0IIUujO8qqpqRywWK0bfKQEvJQo+x9SpU/ecOnVqNQBg2F5GMawESAxs3NNPP73itddeS768kaYGNPDLy8vPXrx48eccxyHwxfsBKioq0NROJBTtGE7waV5N9sFkMlW63W7xQU7ofsb6+vpHvvzyywe0zgwSxA/5fL4lAICv1RAz1TLDToCEMaw7duz4/qpVq/6VldxJv3/ooYee27Rp00Es92VlZQs5jhNvEKUdeqz5azGidH8A1wUAvHv16lXxLWYQQseWLVtufPzxx99iebnc93pu+jAdRsug0yk7derUm71e7/tqvN/n86H9fXTtvw+ds6ysDCV8c+XOny0EwH3s7u7G9zQWchz3nbKysl1Ku4VSMowqAlRVVf0qGAz+PeEpSTxJ7/f7/ctwhl9SUmLPy8trkZP1TEi+HPFY1w0m6izy+Xzi5WJolmCz2TSRQK9nAMj1f1hDgNlsfkQQhA2oA0oXeCAb9fT0oCeCidM7q9WKrvj5TTaDr5QTJH5f7ff738EkKC0t1UQCAMD2YDD443TUN6MEKCsrs4TDYTRIUbJZizskKdDnQCCQ9HyLxfIKx3E/Hgngk8ZlqQEA4Fm/34+eFC4qAbp8jhUOyDBA5hmJHcCbQ6GQrm8v100B0DOBI5EIepZPuZokjQS3t7c3Cb7Van1TEAQ0t6cS99uWfCVPZJEAQvhEb2+vuF5x6tSp2+vr63fIkYCVZHIc5+3v70fvVdDl0IUAhYWFf0Lb41qSNAxke3v7XWazGdVHsv9rQRDEDHoUEoAzGAxL/X7/GQhh3vLly1/Yv39/MieSEkeGAKKZBUF4IxKJrEuXBWkRoKCgAHnuNtwJNd5JhoRbb7311Z07dz6DVrnMZvNdEMJ3hxN8Nf1TGddl7S6jAsmne6H9g4qKiuPhcHjQ9QxKwJMhAn0Oh8NpXSKeMgEKCgqSXq+0cUOzVn5+fr/f70e3dqGra/ji4mJ/toCvJq4reZ4cCfr6+vAU8aaSkpJ9akCnEROfAwAwJxwOp/ROgZQIkJ+fL/uUDjkg8UBCodBtAADxxQqFhYXUx8BrURYaIFo8PlVA5erJkOBYKBS6FdVds2bNT19//XXFizyUkkZBEJbFYrFBG2RKYxJxUlOILIPBl4IsNbac8efOnbvj2LFjq1AbxcXF4gMm0GfWYk46QKZTN91wwCIA8ngAwAyU0UMIS81m89fxeDxfLZlYS8sQwluj0egxLZhqIoDRaNTs+TQAQqHQbACA+DIF7P2sTqe6wqcH8NI+KSz6UIcgJ+/9/f1iKAgEAkO2klnkU1KCaDSKXkql+qoh1QRA27GkYih5PA0A9N2SJUve2Ldvn5i9mkwmNwAgrSSGZvVUSaPGc7TEaxXtPdvf378RvVvAbrf/X19fXw2qI7d5pIYY0WhUtU1VEcBoNKJMf9D9d6kSIBQKXQMAaEPvBywsLLyqwkgpF9GTCDoDnxwTVgEI4Q1FRUV4S3nQmBlb0LJl1JJADQEq8/LyLkpjPj67EhHIetXV1UdbWlrER6mYTKavAABo73xMHxDC5nA4LCaBJSUlHbFYrEiNl6so0xyLxRSTS0UC5OXlMd/TQyLHknyyzFdffbWktrZWfAWcyWQa0i6LVKOFIawcAs/lN2/efO8TTzzxtprcQ40qSN+zQLOjLAEMBsOPAABDrrtXAzZNMdCiBwBAKCwsXAchZF7hOhwJXDaQSCaJbAqHwwchhIUmk2nIXT9qwKblDui+2VgsNlNu7EoESHqpWtBZoaK2tnbv+fPnxeleQUEB1fvTBT7d+mpJkspsgGybUj8SiUTsqMyUKVM+aG1t/S6tL3KLSyzViMfjsu8YYhIgLy+vHkL4B2nDqazRozqbNm267dFHHxUXfsi1BBZh1IKhR30t51IAUlNTJKADAwN4dXBGQUHBCYZHJ9tXSwYI4WVBEOawOsYkgMFgQBm6US8CRCKRCgDAQH5+PlKBIXFOL+/Vqx05JNNVABq4AIAXIpEI2gZPOojcedQSALUXj8eZ00ImAXieTz4Cnskexpat1CsNBkM0HA6L28RFRUXHotGoGJf0AkuvdjS5b6KwjmSA0WhUfMJYTU3NQa/XO+gZAKmQgSDaRkEQqPdJUglgMBj+EkL4BmkQJSPL/V5ZWXmira1NfOEhXk1Mxdh6kibV8yvVS4cQeO6+bt26H27YsOH3rHOpOQdle5mqAlQCFBQUnE4sKSb7oEQAOXAaGhp+8dFHH6G7djg0rVTTlpKhs40MakBRMabGaDR6HEJYbDQaxfcl0w4156IQoILjuIEhIZ12AiT/Sp1VCyIqd+jQoRlLlixBlzIV5uXlaX64gdpzKfU507+rAYrsU0FBwQd9fX3ia3GQo2ipr1SW5/n1sVhsyOPlqQqgJwHQYGKxmDj/Ly4uboxEIu+lA8RIIIMSGHLjx4s36ClkPT09aGNH1aF0TpSAx+NxpAKDDhoBxuG7UbQam1UeZ6EVFRXP+Xw+xcuYtJ5XlYWyoJASSKiL2FaNjY0/P3DgwD9Iu62mDVYdCOGQPGAIAcxm893BYHBrqjGWBh5BgB0+n0/cC1BzjBYiaAEtHo/bEA9Wrlx5w9atW5M3nmJ7aWlLWgdCWMlxnHjrGj6GEKC8vPyl7u7ux1gApQKKIAgi89AjZYPBYPIOn1TaUkOckVaGBJXn+TtjsdgRtCxsMBg050tyBDEajY8MDAz8pywBLBbL+729veITMGmHVtAMBkMkGo2Ky5xFRUXnwuGwuOedZKDMWsJIAzKV/koBKy0t3dDd3S3uk6jJxaTnlCNAUVHRrr6+vkGvmqOFgBPBYHBGKoOhAZuXl+cfGBgQXwWfn5/fGovFxMWO3EG3QFFR0clgMIjeOzCIAKlIv/QMPM93xuPxQS+ZGEIA5KX9/f2DvDQVsLBSmEymzlAoJJ7UaDR64/E4ulkydzAsYDAYfNFoVHwCeX5+PtrNw3sEuthMmggOIYDJZLoYiURQsqDLgV6q3N/fLz640WAwdAqCYNKl4VHcCAaprKzsY5/PN0+voSZUZNBMgEaAs7FYTIzZehz5+fneUCgk7kYZjUYXhLBAj3ZHcxuxWEx0wNra2jddLtddeo0VESAejw9y7iEEQNemoS17vU6Kbl4BAIiPc4cQ3oSEQMe2R2VTAICjCXvVchw3Xs9B4rZxm4qXhOl58lxb2WeBHAGyD5OM9ihHgIyaO/tOliNA9mGS0R7lCJBRc2ffyXIEyD5MMtqjHAEyau7sO1mOANmHSUZ7lCNARs2dfSfLESD7MMloj3IEyKi5s+9kOQJkHyYZ7VGOABk1d/adLEeA7MMkoz3KESCj5s6+k+UIkH2YZLRH/w+8l5dx7q/F3gAAAABJRU5ErkJggg==\" alt=\"###\" *ngIf=\"conversation.isGroup && !conversation.partnerPicture\">\n  </div>\n  <div class=\"media-body content\">\n    <h5 class=\"mt-0\">{{conversation.partnerName}}</h5>\n    <p>{{conversation.msg}}</p>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/private/connect/conversation/conversation.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConversationCard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__("./node_modules/angularfire2/database/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_utils_util__ = __webpack_require__("./src/app/utils/util.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ConversationCard = /** @class */ (function () {
    function ConversationCard(uiService, utilService, firebase) {
        this.uiService = uiService;
        this.utilService = utilService;
        this.firebase = firebase;
        this.name = '';
    }
    ConversationCard.prototype.ngOnInit = function () {
        this.name = __WEBPACK_IMPORTED_MODULE_0_underscore__["map"](this.conversation.participants, 'name').join(';');
    };
    ConversationCard.prototype.ngAfterViewInit = function () {
    };
    ConversationCard.prototype.select = function () {
        this.uiService.selectConversation(this.conversation.id);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ConversationCard.prototype, "conversation", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ConversationCard.prototype, "user", void 0);
    ConversationCard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'conversation',
            template: __webpack_require__("./src/app/private/connect/conversation/conversation.html"),
            styles: [__webpack_require__("./src/app/private/connect/conversation/conversation.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_app_services_ui__["a" /* UiService */], __WEBPACK_IMPORTED_MODULE_4_app_utils_util__["a" /* UtilService */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["AngularFireDatabase"]])
    ], ConversationCard);
    return ConversationCard;
}());



/***/ }),

/***/ "./src/app/private/connect/message/message.css":
/***/ (function(module, exports) {

module.exports = ".message {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.message > img {\n  width: 30%;\n  max-height: 30vh;\n}\n\n.message > p {\n  padding: 5px 10px;\n  border-radius: 20px;\n}\n\n.left {\n  margin: auto auto auto 15px;\n}\n\n.left > p {\n  background-color: #02a8f3;\n  color: white;\n  font-weight: 600;\n}\n\n.left > img {\n  margin: auto 5px auto 0px;\n}\n\n.right {\n  margin: auto 15px auto auto;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n      -ms-flex-direction: row-reverse;\n          flex-direction: row-reverse;\n}\n\n.right > p {\n  border: solid 1px #aeaeae;\n  font-weight: 600;\n}\n\n.right > img {\n  margin: auto 0px auto 5px;\n}"

/***/ }),

/***/ "./src/app/private/connect/message/message.html":
/***/ (function(module, exports) {

module.exports = "<div #chatScreen style=\"height: 100%; overflow: auto; position: relative;\" *ngIf=\"conversation\">\n  <div [ngClass]=\"[user.id == message.sender ? 'message left' : 'message right']\" *ngFor=\"let message of conversation.messages\">\n    <p *ngIf=\"message.content && message.contentType=='text'\">{{message.content}}</p>\n    <img *ngIf=\"message.content && message.contentType=='photo'\" src=\"{{message.content}}\" style=\"margin-bottom: 10px;\"/>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/private/connect/message/message.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Message; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__("./node_modules/angularfire2/database/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var Message = /** @class */ (function () {
    function Message(uiService, router, firebase, store) {
        var _this = this;
        this.uiService = uiService;
        this.router = router;
        this.firebase = firebase;
        this.store = store;
        store.select('user').subscribe(function (u) {
            _this.user = u;
        });
        store.select('conversations').subscribe(function (convs) {
            _this.conversation = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](convs, function (item) {
                return item.isSelected;
            });
            //Update data with Firebase
            if (!_this.conversation || !_this.user)
                return;
            if (!_this.conversation.isGroup) {
                _this.firebaseDB = _this.firebase.list('conversations/' + _this.user.id + '/' + _this.conversation.partnerID);
                _this.firebaseDB.valueChanges().subscribe(function (data) {
                    if (!_this.conversation.messages || (data.length > _this.conversation.messages.length)) {
                        _this.uiService.getMessages(_this.conversation.id, data);
                    }
                    if (_this.chatScreen) {
                        _this.chatScreen.nativeElement.scrollTop = _this.chatScreen.nativeElement.scrollHeight;
                    }
                });
            }
            else {
                _this.firebaseDB = _this.firebase.list('conversations/' + _this.user.id + '/' + _this.conversation.id);
                _this.firebaseDB.valueChanges().subscribe(function (data) {
                    if (!_this.conversation.messages || (data.length > _this.conversation.messages.length)) {
                        _this.uiService.getMessages(_this.conversation.id, data);
                    }
                    if (_this.chatScreen) {
                        _this.chatScreen.nativeElement.scrollTop = _this.chatScreen.nativeElement.scrollHeight;
                    }
                });
            }
        });
    }
    Message.prototype.ngOnInit = function () {
    };
    Message.prototype.ngAfterViewInit = function () {
    };
    Message.prototype.ngAfterViewChecked = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_11" /* ViewChild */])('chatScreen'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_core__["t" /* ElementRef */])
    ], Message.prototype, "chatScreen", void 0);
    Message = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'messages',
            template: __webpack_require__("./src/app/private/connect/message/message.html"),
            styles: [__webpack_require__("./src/app/private/connect/message/message.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_app_services_ui__["a" /* UiService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["AngularFireDatabase"], __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */]])
    ], Message);
    return Message;
}());



/***/ }),

/***/ "./src/app/private/connect/typing-message/typing.css":
/***/ (function(module, exports) {

module.exports = ".typing {\n  float: right;\n}\n\n.typing > img {\n  width: 50px;\n}"

/***/ }),

/***/ "./src/app/private/connect/typing-message/typing.html":
/***/ (function(module, exports) {

module.exports = "<p *ngIf=\"show\" class=\"typing\">\n  <img src=\"https://loading.io/spinners/message/index.messenger-typing-preloader.svg\" alt=\"\">\n</p>"

/***/ }),

/***/ "./src/app/private/connect/typing-message/typing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Typing; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__ = __webpack_require__("./node_modules/angularfire2/database/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_database___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angularfire2_database__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var Typing = /** @class */ (function () {
    function Typing(apiService, store, router, firebase) {
        var _this = this;
        this.apiService = apiService;
        this.store = store;
        this.router = router;
        this.firebase = firebase;
        this.show = false;
        this.time = 0;
        this.timeout = null;
        store.select('conversations').subscribe(function (convs) {
            _this.conversation = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](convs, function (item) {
                return item.isSelected;
            });
            if (!_this.conversation || !_this.user)
                return;
            var numOfMsg = _this.conversation.messages ? _this.conversation.messages.length : 0;
            //Tracking typing status
            _this.firebase.object('typing/' + _this.user.id + '/' + _this.conversation.partnerID).valueChanges().subscribe(function (data) {
                if (_this.time && data) {
                    _this.show = true;
                    if (_this.timeout) {
                        clearTimeout(_this.timeout);
                        _this.timeout = null;
                    }
                    if (!_this.timeout) {
                        _this.timeout = setTimeout(function () {
                            _this.show = false;
                            _this.timeout = null;
                        }, 3000);
                    }
                }
                _this.time = data;
            });
            //Disable typing status if new message was sent
            _this.firebase.list('conversations/' + _this.conversation.partnerID + '/' + _this.user.id).valueChanges().subscribe(function (data) {
                if (numOfMsg || data.length > numOfMsg) {
                    if (_this.timeout) {
                        _this.show = false;
                        clearTimeout(_this.timeout);
                        _this.timeout = null;
                    }
                }
            });
        });
    }
    Typing.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], Typing.prototype, "user", void 0);
    Typing = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'typing-message',
            template: __webpack_require__("./src/app/private/connect/typing-message/typing.html"),
            styles: [__webpack_require__("./src/app/private/connect/typing-message/typing.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_4_angularfire2_database__["AngularFireDatabase"]])
    ], Typing);
    return Typing;
}());

;


/***/ }),

/***/ "./src/app/private/infos/avatar/avatar.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"login textbox\">\n  <h2 class = \"registration_flow\">upload a profile picture.</h2>\n  <input style = \"background: none; border: none;\" type =\"file\" name='attachment' id='uploaded_file'>\n  <button class=\"login_form_btn\">Upload</button>\n    <div class=\"signup_link\"><a routerLink=\"/login/loginflow\">Or skip for now</a></div>\n</div>\n"

/***/ }),

/***/ "./src/app/private/infos/avatar/avatar.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AvatarInfo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AvatarInfo = /** @class */ (function () {
    function AvatarInfo(store, apiService, router) {
        this.store = store;
        this.apiService = apiService;
        this.router = router;
    }
    AvatarInfo.prototype.login = function () {
        this.apiService.login(this.email, this.password);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], AvatarInfo.prototype, "email", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], AvatarInfo.prototype, "password", void 0);
    AvatarInfo = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'avatar',
            template: __webpack_require__("./src/app/private/infos/avatar/avatar.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], AvatarInfo);
    return AvatarInfo;
}());



/***/ }),

/***/ "./src/app/private/infos/birthday/birthday.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"login textbox\">\n  <h2 class=\"registration_flow\">Choose your birthday.</h2>\n  <div class=\"md-form\">\n    <input mdbActive type=\"text\" id=\"bday\" class=\"form-control\" autofocus>\n    <label for=\"bday\">Add your birthday</label>\n  </div>\n  <button class=\"btn btn-secondary btn-lg\">OK</button>\n  <div class=\"signup_link\">\n    <a routerLink=\"/login/upload_photo\">Or skip for now</a>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/private/infos/birthday/birthday.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BirthdayInfo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BirthdayInfo = /** @class */ (function () {
    function BirthdayInfo(store, apiService, router) {
        this.store = store;
        this.apiService = apiService;
        this.router = router;
    }
    BirthdayInfo.prototype.login = function () {
        this.apiService.login(this.email, this.password);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], BirthdayInfo.prototype, "email", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], BirthdayInfo.prototype, "password", void 0);
    BirthdayInfo = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'birthday',
            template: __webpack_require__("./src/app/private/infos/birthday/birthday.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], BirthdayInfo);
    return BirthdayInfo;
}());



/***/ }),

/***/ "./src/app/private/infos/contact/contact.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"login textbox\">\n  <h2 class=\"registration_flow\">Import your contacts.</h2>\n  <button class=\"btn btn-secondary btn-lg\">Import</button>\n  <div class=\"signup_link\"><a routerLink=\"/login/import_contacts\">Or skip for now</a></div>\n</div>\n"

/***/ }),

/***/ "./src/app/private/infos/contact/contact.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactInfo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ContactInfo = /** @class */ (function () {
    function ContactInfo(store, apiService, router) {
        this.store = store;
        this.apiService = apiService;
        this.router = router;
    }
    ContactInfo.prototype.login = function () {
        this.apiService.login(this.email, this.password);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ContactInfo.prototype, "email", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ContactInfo.prototype, "password", void 0);
    ContactInfo = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'contact',
            template: __webpack_require__("./src/app/private/infos/contact/contact.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], ContactInfo);
    return ContactInfo;
}());



/***/ }),

/***/ "./src/app/private/infos/reset-password-success/reset-password-success.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"login textbox\">\n  <h2 class=\"password_reset_flow\">Your password has been reset successfully.</h2>\n  <button class=\"btn btn-secondary btn-lg\">Login</button>\n</div>\n"

/***/ }),

/***/ "./src/app/private/infos/reset-password-success/reset-password-success.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetPasswordSuccess; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ResetPasswordSuccess = /** @class */ (function () {
    function ResetPasswordSuccess(store, apiService, router) {
        this.store = store;
        this.apiService = apiService;
        this.router = router;
    }
    ResetPasswordSuccess.prototype.login = function () {
        this.apiService.login(this.email, this.password);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ResetPasswordSuccess.prototype, "email", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ResetPasswordSuccess.prototype, "password", void 0);
    ResetPasswordSuccess = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'reset-password-success',
            template: __webpack_require__("./src/app/private/infos/reset-password-success/reset-password-success.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], ResetPasswordSuccess);
    return ResetPasswordSuccess;
}());



/***/ }),

/***/ "./src/app/private/infos/reset-password-verify/reset-password-verify.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"login textbox\">\n  <h2 class=\"password_reset_flow\">Enter your verification code and choose a new password.</h2>\n  <div class=\"md-form\">\n    <input mdbActive type=\"text\" id=\"code\" class=\"form-control\" autofocus [(ngModel)]=\"email\">\n    <label for=\"code\">Verification Code</label>\n  </div>\n  <div class=\"md-form\">\n    <input mdbActive type=\"text\" id=\"pwd1\" class=\"form-control\" [(ngModel)]=\"password\">\n    <label for=\"pwd1\">New Password</label>\n  </div>\n  <div class=\"md-form\">\n    <input mdbActive type=\"text\" id=\"pwd2\" class=\"form-control\" [(ngModel)]=\"password\">\n    <label for=\"pwd2\">Repeat Password</label>\n  </div>\n  <button class=\"btn btn-secondary btn-lg\">Reset</button>\n</div>"

/***/ }),

/***/ "./src/app/private/infos/reset-password-verify/reset-password-verify.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetPasswordVerify; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ResetPasswordVerify = /** @class */ (function () {
    function ResetPasswordVerify(store, apiService, router) {
        this.store = store;
        this.apiService = apiService;
        this.router = router;
    }
    ResetPasswordVerify.prototype.login = function () {
        this.apiService.login(this.email, this.password);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ResetPasswordVerify.prototype, "email", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ResetPasswordVerify.prototype, "password", void 0);
    ResetPasswordVerify = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'reset-password-verify',
            template: __webpack_require__("./src/app/private/infos/reset-password-verify/reset-password-verify.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], ResetPasswordVerify);
    return ResetPasswordVerify;
}());



/***/ }),

/***/ "./src/app/private/infos/username/username.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"login textbox\">\n  <h2 class=\"registration_flow\">Choose your username.</h2>\n  <div class=\"md-form\">\n    <input mdbActive type=\"text\" id=\"name\" class=\"form-control\" autofocus>\n    <label for=\"name\">Your name</label>\n  </div>\n  <button class=\"btn btn-secondary btn-lg\">OK</button>\n  <div class=\"signup_link\">\n    <a routerLink=\"/login/birthday\">Or skip for now</a>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/private/infos/username/username.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsernameInfo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UsernameInfo = /** @class */ (function () {
    function UsernameInfo(store, apiService, router) {
        this.store = store;
        this.apiService = apiService;
        this.router = router;
    }
    UsernameInfo.prototype.login = function () {
        this.apiService.login(this.email, this.password);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], UsernameInfo.prototype, "email", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], UsernameInfo.prototype, "password", void 0);
    UsernameInfo = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'username',
            template: __webpack_require__("./src/app/private/infos/username/username.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], UsernameInfo);
    return UsernameInfo;
}());



/***/ }),

/***/ "./src/app/private/payment/payment.css":
/***/ (function(module, exports) {

module.exports = ".app-payment {\n  width: 100%;\n  height: 100vh;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.app-payment > div {\n  margin: 10px auto;\n  padding: 20px 40px;\n  width: 30vw;\n  border: solid 1px #9c9c9c;\n  background-color: white;\n}\n\n.app-payment > .list-payment {\n  width: 50vw;\n}\n\n.list-payment > a {\n  float: right;\n  cursor: pointer;\n  margin-bottom: 10px;\n  color: #0044fd;\n}\n\n.list-payment > a:hover {\n  text-decoration: underline;\n}\n\ntable {\n  font-family: arial, sans-serif;\n  border-collapse: collapse;\n  width: 100%;\n}\n\ntd, th {\n  border: 1px solid #dddddd;\n  text-align: left;\n  padding: 8px;\n}\n\n.hosted-field {\n  height: 50px;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  width: 100%;\n  padding: 12px;\n  display: inline-block;\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  font-weight: 600;\n  font-size: 14px;\n  border-radius: 6px;\n  border: 1px solid #dddddd;\n  line-height: 20px;\n  background: #fcfcfc;\n  margin-bottom: 12px;\n  background: -webkit-gradient(linear, left top, right top, color-stop(50%, white), color-stop(50%, #fcfcfc));\n  background: linear-gradient(to right, white 50%, #fcfcfc 50%);\n  background-size: 200% 100%;\n  background-position: right bottom;\n  -webkit-transition: all 300ms ease-in-out;\n  transition: all 300ms ease-in-out;\n}\n\n.hosted-fields--label {\n  font-family: courier, monospace;\n  text-transform: uppercase;\n  font-size: 14px;\n  display: block;\n  margin-bottom: 6px;\n}\n\n.button-container {\n  display: block;\n  text-align: center;\n}\n\n.button {\n  cursor: pointer;\n  font-weight: 500;\n  line-height: inherit;\n  position: relative;\n  text-decoration: none;\n  text-align: center;\n  border-style: solid;\n  border-width: 1px;\n  border-radius: 3px;\n  -webkit-appearance: none;\n  -moz-appearance: none;\n  display: inline-block;\n}\n\n.button--small {\n  padding: 10px 20px;\n  font-size: 0.875rem;\n}\n\n.button--green {\n  outline: none;\n  background-color: #64d18a;\n  border-color: #64d18a;\n  color: white;\n  -webkit-transition: all 200ms ease;\n  transition: all 200ms ease;\n}\n\n.button--green:hover {\n  background-color: #8bdda8;\n  color: white;\n}\n\n.braintree-hosted-fields-focused {\n  border: 1px solid #64d18a;\n  border-radius: 1px;\n  background-position: left bottom;\n}\n\n.braintree-hosted-fields-invalid {\n  border: 1px solid #ed574a;\n}\n\n.braintree-hosted-fields-valid {\n}\n\n#cardForm {\n  max-width: 50.75em;\n  margin: 0 auto;\n  padding: 1.875em;\n}"

/***/ }),

/***/ "./src/app/private/payment/payment.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-payment\">\n  <div class=\"demo-frame\">\n    <form action=\"/\" method=\"post\" id=\"cardForm\" (ngSubmit)=\"submit()\">\n      <label class=\"hosted-fields--label\" for=\"card-number\">Card Number</label>\n      <div id=\"card-number\" class=\"hosted-field\"></div>\n\n      <label class=\"hosted-fields--label\" for=\"expiration-date\">Expiration Date</label>\n      <div id=\"expiration-date\" class=\"hosted-field\"></div>\n\n      <label class=\"hosted-fields--label\" for=\"cvv\">CVV</label>\n      <div id=\"cvv\" class=\"hosted-field\"></div>\n\n      <label class=\"hosted-fields--label\" for=\"postal-code\">Postal Code</label>\n      <div id=\"postal-code\" class=\"hosted-field\"></div>\n\n      <div class=\"button-container\">\n        <input type=\"submit\" class=\"button button--small button--green\" value=\"Purchase 5 USD\" id=\"submit\" />\n      </div>\n    </form>\n  </div>\n  <div class=\"list-payment\">\n    <a (click)=\"listPayment()\">Refresh</a>\n    <div class=\"Ad content\">\n      <div>\n        <label>Ad Title: </label>\n        <b>{{(ads | async)[0]?.title}}</b>\n      </div>\n      <div>\n        <label>Ad Impressions: </label>\n        <b>{{(ads | async)[0]?.impressions}}</b>\n      </div>\n    </div>\n    <table>\n      <tr>\n        <th>ID</th>\n        <th>Amount</th>\n        <th>Status</th>\n        <th>Created Date</th>\n        <th>Payment ID</th>\n      </tr>\n      <tr *ngFor=\"let payment of (ads | async)[0]?.payments\">\n        <td>{{payment._id}}</td>\n        <td>{{payment.amount}} USD</td>\n        <td>{{payment.status}}</td>\n        <td>{{payment.createdAt | date:'fullDate'}}</td>\n        <td>{{payment.tid}}</td>\n      </tr>\n    </table>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/private/payment/payment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var braintree = __webpack_require__("./node_modules/braintree-web/dist/browser/index.js");
var PaymentPage = /** @class */ (function () {
    function PaymentPage(apiService, store) {
        this.apiService = apiService;
        this.store = store;
        // this.ads = store.select('ads');
    }
    PaymentPage.prototype.submit = function () {
        var _this = this;
        this.cardInput.tokenize(function (tokenizeErr, payload) {
            if (tokenizeErr) {
                console.error(tokenizeErr);
                return;
            }
            console.log('Got a nonce for creating transaction: ' + payload.nonce);
            //Send nonce to server and make transaction
            //For example
            _this.apiService.makePayment('5a7d670c51d1730e4c66ef55', payload.nonce, 5);
            //Reset value
            _this.cardInput.clear('number');
            _this.cardInput.clear('expirationDate');
            _this.cardInput.clear('cvv');
            _this.cardInput.clear('postalCode');
        });
    };
    PaymentPage.prototype.listPayment = function () {
        this.apiService.fetchPayment('5a7d670c51d1730e4c66ef55');
    };
    PaymentPage.prototype.ngOnInit = function () {
        var _this = this;
        var styles = {
            'input': {
                'font-size': '16px',
                'font-family': 'courier, monospace',
                'font-weight': 'lighter',
                'color': '#ccc'
            },
            ':focus': {
                'color': 'black'
            },
            '.valid': {
                'color': '#8bdda8'
            }
        };
        var fields = {
            number: {
                selector: '#card-number',
                placeholder: '4111 1111 1111 1111'
            },
            cvv: {
                selector: '#cvv',
                placeholder: '123'
            },
            expirationDate: {
                selector: '#expiration-date',
                placeholder: '10/2020'
            },
            postalCode: {
                selector: '#postal-code',
                placeholder: '11111'
            }
        };
        braintree.client.create({
            authorization: 'sandbox_8pvx5pyy_6rpmqzs9jyg8yn3n'
        }, function (err, clientInstance) {
            if (err) {
                console.error(err);
                return;
            }
            braintree.hostedFields.create({
                client: clientInstance,
                styles: styles,
                fields: fields
            }, function (cardFields, hostedFieldsInstance) {
                _this.cardInput = hostedFieldsInstance;
            });
        });
        this.apiService.fetchPayment('5a7d670c51d1730e4c66ef55');
    };
    PaymentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-payment',
            template: __webpack_require__("./src/app/private/payment/payment.html"),
            styles: [__webpack_require__("./src/app/private/payment/payment.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["a" /* Store */]])
    ], PaymentPage);
    return PaymentPage;
}());



/***/ }),

/***/ "./src/app/private/profile/calendar/calendar.css":
/***/ (function(module, exports) {

module.exports = ".calendar {\n  width: 100%;\n  padding: 5px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.calendar > .card {\n  margin: 5px;\n}"

/***/ }),

/***/ "./src/app/private/profile/calendar/calendar.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"calendar\">\n  <div class=\"card\">\n    <img class=\"img-fluid\" src=\"https://mdbootstrap.com/img/Photos/Others/images/43.jpg\" alt=\"Card image cap\">\n    <div class=\"card-body\">\n      <h4 class=\"card-title\">Card title</h4>\n      <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p>\n    </div>\n  </div>\n  <div class=\"card\">\n    <img class=\"img-fluid\" src=\"https://mdbootstrap.com/img/Photos/Others/images/43.jpg\" alt=\"Card image cap\">\n    <div class=\"card-body\">\n      <h4 class=\"card-title\">Card title</h4>\n      <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p>\n    </div>\n  </div>\n  <div class=\"card\">\n    <img class=\"img-fluid\" src=\"https://mdbootstrap.com/img/Photos/Others/images/43.jpg\" alt=\"Card image cap\">\n    <div class=\"card-body\">\n      <h4 class=\"card-title\">Card title</h4>\n      <p class=\"card-text\">Some quick example text to build on the card title and make up the bulk of the card's content.</p>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/private/profile/calendar/calendar.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CalendarInfo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CalendarInfo = /** @class */ (function () {
    function CalendarInfo(store, apiService, router) {
        this.store = store;
        this.apiService = apiService;
        this.router = router;
    }
    CalendarInfo = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'calendar-info',
            template: __webpack_require__("./src/app/private/profile/calendar/calendar.html"),
            styles: [__webpack_require__("./src/app/private/profile/calendar/calendar.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], CalendarInfo);
    return CalendarInfo;
}());



/***/ }),

/***/ "./src/app/private/profile/info/my-profile.css":
/***/ (function(module, exports) {

module.exports = "/* My Profile */\n.my-profile {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 100%;\n  height: 100%;\n}\n.my-profile > .avatar {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  width: 15vw;\n}\n.my-profile > .avatar > img {\n  width: 13vw;\n  height: 13vw;\n  margin: 5px auto;\n  border-radius: 50%;\n}\n.my-profile > .avatar > p {\n  width: 100%;\n  margin: 10px auto;\n  text-align: center;\n  font-weight: 800;\n}\n.my-profile > .info {\n  width: 50vw;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n.my-profile > .info > .friend,\n.my-profile > .info > .upload {\n  width: 100%;\n  padding: 5px;\n}\n.my-profile > .info > .friend > div {\n  padding: 5px;\n}\n.my-profile > .info > .friend > div > div {\n  width: 80px;\n  display: inline-block;\n}\n.my-profile > .info > .friend > div > div > img {\n  width: 60px;\n  height: 60px;\n  border-radius: 50%;\n  margin: auto 10px;\n}\n.my-profile > .info > .friend > div > div > p {\n  text-align: center;\n  font-size: 12px;\n}\n.my-profile > .info > .upload > div {\n  /* display: flex; */\n  /* justify-content: space-around; */\n}\n.my-profile > .info > .upload > div > .card {\n  display: inline-block;\n  width: 23vw;\n  margin: 5px;\n}\n.my-profile > .info > .upload > div > .card > .text-content,\n.my-profile > .info > .upload > div > .card > .photo-content,\n.my-profile > .info > .upload > div > .card > .video-content {\n  width: 100%;\n  height: 15vw;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n.my-profile > .info > .upload > div > .card > .text-content > p {\n  margin: auto;\n  font-size: 25px;\n  color: red;\n}\n.my-profile > .info > .upload > div > .card > img {\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n.my-profile > .info > .upload > div > .card > .card-body > h5 {\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n}\n"

/***/ }),

/***/ "./src/app/private/profile/info/my-profile.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"my-profile\" *ngIf=\"user\">\n  <div class=\"avatar\">\n    <img src=\"{{user.profile_pic ? user.profile_pic : 'https://i.pinimg.com/originals/5a/59/1c/5a591c4e208e1747894b41ec7f830beb.png'}}\" />\n    <p>{{user.username}}</p>\n  </div>\n  <div class=\"info\">\n    <div class=\"friend\">\n      <label>Friends</label>\n      <div>\n        <div *ngFor=\"let friend of user.friends\">\n          <img src=\"{{friend.picture ? friend.picture : 'https://i.pinimg.com/originals/5a/59/1c/5a591c4e208e1747894b41ec7f830beb.png'}}\" />\n          <p>{{!friend.username ? 'Unknown' : friend.username}}</p>\n        </div>\n      </div>\n    </div>\n    <div class=\"upload\">\n      <label>Uploads</label>\n      <div>\n        <div class=\"card\" *ngFor=\"let feed of user.uploads\">\n          <video class=\"video-content\" poster=\"{{feed.thumbnail}}\" src=\"{{feed.content}}\" alt=\"Card image cap\" *ngIf=\"feed.content_type == 'video'\"\n            controls></video>\n          <img class=\"img-fluid photo-content\" src=\"{{feed.content}}\" alt=\"Card image cap\" *ngIf=\"feed.content_type == 'photo'\" />\n          <div class=\"text-content\" *ngIf=\"feed.content_type == 'text'\">\n            <p style=\"text-align: center;\">{{feed.content}}</p>\n          </div>\n          <div class=\"card-body\">\n            <h5 class=\"card-title\">{{feed.caption && feed.caption != '' ? feed.caption : 'No caption for this'}}</h5>\n            <!-- <p class=\"card-text\">{{feed.}}</p> -->\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/private/profile/info/my-profile.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyProfileInfo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MyProfileInfo = /** @class */ (function () {
    function MyProfileInfo(store, apiService, router) {
        var _this = this;
        this.store = store;
        this.apiService = apiService;
        this.router = router;
        this.store.select('user').subscribe(function (u) {
            if (!_this.user && u) {
                _this.apiService.fetchFriends();
                _this.apiService.fetchUpload();
            }
            _this.user = u;
        });
    }
    MyProfileInfo.prototype.ngOnInit = function () {
    };
    MyProfileInfo = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'my-profile',
            template: __webpack_require__("./src/app/private/profile/info/my-profile.html"),
            styles: [__webpack_require__("./src/app/private/profile/info/my-profile.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], MyProfileInfo);
    return MyProfileInfo;
}());



/***/ }),

/***/ "./src/app/private/profile/invitation/invitation.css":
/***/ (function(module, exports) {

module.exports = ".invitation {\n  width: 100%;\n}\n\n.invitation > div > div {\n  padding: 5px;\n  text-align: center;\n  overflow: auto;\n  width: 100%;\n  white-space: nowrap;\n}\n\n.invitation > div > div > i {\n  color: #8a8a8a;\n}\n\n.invitation > div > h4 {\n  margin: 10px;\n  font-weight: 400;\n}\n\n.invitation > div > div > .card {\n  width: 15vw;\n  display: inline-block;\n  margin: 5px;\n}\n\n.invitation > div > div > .card > .default-invite-img,\n.invitation > div > div > .card > img {\n  width: 100%;\n  height: 15vw;\n}\n\n.invitation > div > div > .card > .default-invite-img {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.invitation > div > div > .card > .default-invite-img > a {\n  margin: auto;\n  font-size: 8vw;\n  color: #adadad;\n}\n\n.pending > div > div,\n.going > div > div,\n.not_going > div > div {\n  width: 80px;\n}\n\n.pending > div > div > img,\n.going > div > div > img,\n.not_going > div > div > img {\n  width: 50px;\n  height: 50px;\n  border-radius: 50%;\n  margin: auto 15px;\n}\n\n.pending > div > div > p,\n.going > div > div > p,\n.not_going > div > div > p {\n  text-align: center;\n  width: 100%;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  font-size: 12px;\n}"

/***/ }),

/***/ "./src/app/private/profile/invitation/invitation.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"invitation\" *ngIf=\"user\">\n  <div>\n    <h4>Received:</h4>\n    <div>\n      <i *ngIf=\"!user.receivedInviations || !user.receivedInviations.length\">You don't receive any invitation</i>\n      <div class=\"card\" *ngFor=\"let recInv of user.receivedInviations\">\n        <img class=\"img-fluid\" src=\"{{recInv.thumbnail}}\" alt=\"Card image cap\" *ngIf=\"recInv.thumbnail\">\n        <div class=\"default-invite-img\" *ngIf=\"!recInv.thumbnail\">\n          <a class=\"fas fa-envelope\"></a>\n        </div>\n        <div class=\"card-body\">\n          <h5 class=\"card-title\">{{recInv.title}}</h5>\n          <p class=\"card-text\">{{recInv.message}}</p>\n          <p class=\"card-text\">\n            <span>{{recInv.startTime | date}}</span> -\n            <span>{{recInv.endTime | date}}</span>\n          </p>\n        </div>\n      </div>\n    </div>\n  </div>\n  <hr/>\n  <div>\n    <h4>Sent:</h4>\n    <div>\n      <i *ngIf=\"!user.sentInviations || !user.sentInviations.length\">You don't sent any invitation</i>\n      <div class=\"card\" *ngFor=\"let sentInv of user.sentInviations\" (click)=\"showInvitationDetail(sentInv)\">\n        <img class=\"img-fluid\" src=\"{{sentInv.thumbnail}}\" alt=\"Card image cap\" *ngIf=\"sentInv.thumbnail\">\n        <div class=\"default-invite-img\" *ngIf=\"!sentInv.thumbnail\">\n          <a class=\"fas fa-envelope\"></a>\n        </div>\n        <div class=\"card-body\">\n          <h5 class=\"card-title\">{{sentInv.title}}</h5>\n          <p class=\"card-text\">{{sentInv.message}}</p>\n          <p class=\"card-text\">\n            <span>{{sentInv.startTime | date}}</span> -\n            <span>{{sentInv.endTime | date}}</span>\n          </p>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div mdbModal #invitation=\"mdb-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\" [config]=\"{show: false}\"\n  (onHidden)=\"onHidden()\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title w-100\">Invitation detail</h4>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"pending\">\n          <h6 style=\"font-weight: 400;\">User is pending: </h6>\n          <div *ngIf=\"invItem\">\n            <i *ngIf=\"!invItem.pending.length\" style=\"font-size: 13px; color: #636363;\">No user is pending</i>\n            <div *ngFor=\"let u of invItem.pending\">\n              <img src=\"{{u.receiverPicture ? u.receiverPicture : 'https://i.pinimg.com/originals/5a/59/1c/5a591c4e208e1747894b41ec7f830beb.png'}}\" />\n              <p>{{u.receiverName}}</p>\n            </div>\n          </div>\n        </div>\n        <br/>\n        <div class=\"going\">\n          <h6 style=\"font-weight: 400;\">User is going: </h6>\n          <div *ngIf=\"invItem\">\n            <i *ngIf=\"!invItem.going.length\" style=\"font-size: 13px; color: #636363;\">No user is going</i>\n            <div *ngFor=\"let u of invItem.going\">\n              <img src=\"{{u.receiverPicture ? u.receiverPicture : 'https://i.pinimg.com/originals/5a/59/1c/5a591c4e208e1747894b41ec7f830beb.png'}}\" />\n              <p>{{u.receiverName}}</p>\n            </div>\n          </div>\n        </div>\n        <br/>\n        <div class=\"not_going\">\n          <h6 style=\"font-weight: 400;\">User isn't going: </h6>\n          <div *ngIf=\"invItem\">\n            <i *ngIf=\"!invItem.not_going.length\" style=\"font-size: 13px; color: #636363;\">No user isn't going</i>\n            <div *ngFor=\"let u of invItem.not_going\">\n              <img src=\"{{u.receiverPicture ? u.receiverPicture : 'https://i.pinimg.com/originals/5a/59/1c/5a591c4e208e1747894b41ec7f830beb.png'}}\" />\n              <p>{{u.receiverName}}</p>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary waves-light\" aria-label=\"Close\" (click)=\"hideInvitationDetail()\" mdbRippleRadius>Close</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/private/profile/invitation/invitation.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InvitationSettingInfo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular_bootstrap_md__ = __webpack_require__("./node_modules/angular-bootstrap-md/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var InvitationSettingInfo = /** @class */ (function () {
    function InvitationSettingInfo(store, apiService, router) {
        var _this = this;
        this.store = store;
        this.apiService = apiService;
        this.router = router;
        this.store.select('user').subscribe(function (u) {
            if (!_this.user && u) {
                _this.apiService.fetchReceivedInvitations();
                _this.apiService.fetchSentInvitations();
            }
            _this.user = u;
        });
    }
    InvitationSettingInfo.prototype.showInvitationDetail = function (item) {
        this.invItem = item;
        this.invitationDetailModal.show();
    };
    InvitationSettingInfo.prototype.hideInvitationDetail = function () {
        this.invItem = null;
        this.invitationDetailModal.hide();
    };
    InvitationSettingInfo.prototype.onHidden = function () {
        this.invItem = null;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('invitation'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_angular_bootstrap_md__["b" /* ModalDirective */])
    ], InvitationSettingInfo.prototype, "invitationDetailModal", void 0);
    InvitationSettingInfo = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'invitation-setting',
            template: __webpack_require__("./src/app/private/profile/invitation/invitation.html"),
            styles: [__webpack_require__("./src/app/private/profile/invitation/invitation.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], InvitationSettingInfo);
    return InvitationSettingInfo;
}());



/***/ }),

/***/ "./src/app/private/profile/preference/pref.css":
/***/ (function(module, exports) {

module.exports = ".preferences > li > i {\n  font-size: 20px;\n  color: #03A9F4;\n}\n\n.preferences > li > a {\n  margin: auto auto auto 5px;\n  font-size: 13px;\n}\n\n.preferences > .category:hover {\n  background-color: #eaeaea;\n}\n\n.preferences > li > button {\n  margin: auto;\n}"

/***/ }),

/***/ "./src/app/private/profile/preference/pref.html":
/***/ (function(module, exports) {

module.exports = "<ul class=\"list-group preferences\" *ngIf=\"user\">\n  <li class=\"list-group-item d-flex justify-content-between align-items-center category\" *ngFor=\"let cat of categories\" (click)=\"onSelectCategory(cat.id)\">\n    <span>{{cat.name}}</span>\n    <a *ngIf=\"cat.sub_categories.length\">({{cat.sub_categories.length}} subs)</a>\n    <i class=\"fa fa-check\" *ngIf=\"isSelected(cat.id)\"></i>\n  </li>\n  <li class=\"list-group-item d-flex justify-content-between align-items-center\">\n    <button type=\"button\" class=\"btn btn-success waves-light\" aria-label=\"Update\" (click)=\"updatePref()\">Update</button>\n  </li>\n</ul>\n\n<div mdbModal #subCategory=\"mdb-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\" [config]=\"{show: false}\"\n  (onHidden)=\"onHidden()\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title w-100\">Sub Categories</h4>\n      </div>\n      <div class=\"modal-body\" *ngIf=\"sltCategory\">\n        <li class=\"list-group-item d-flex justify-content-between align-items-center\" *ngFor=\"let cat of sltCategory.sub_categories\" (click)=\"onSelectSubCategory(cat)\">\n          <span>{{cat.name}}</span>\n          <i class=\"fa fa-check\" *ngIf=\"isSubSelected(cat.id)\"></i>\n        </li>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary waves-light\" aria-label=\"Close\" (click)=\"hideSubCategory()\" mdbRippleRadius>Close</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/private/profile/preference/pref.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PreferenceSettingInfo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angular_bootstrap_md__ = __webpack_require__("./node_modules/angular-bootstrap-md/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_ui__ = __webpack_require__("./src/app/services/ui.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PreferenceSettingInfo = /** @class */ (function () {
    function PreferenceSettingInfo(store, uiService, apiService, router) {
        var _this = this;
        this.store = store;
        this.uiService = uiService;
        this.apiService = apiService;
        this.router = router;
        store.select('categories').subscribe(function (cat) {
            // console.log(cat)
            _this.categories = cat;
        });
        store.select('user').subscribe(function (usr) {
            // console.log(usr.preferences);
            _this.user = usr;
        });
        this.apiService.fetchCategories();
    }
    PreferenceSettingInfo.prototype.isSelected = function (catId) {
        var prefs = __WEBPACK_IMPORTED_MODULE_0_underscore__["map"](this.user.preferences, 'id');
        return __WEBPACK_IMPORTED_MODULE_0_underscore__["indexOf"](prefs, catId) != -1;
    };
    PreferenceSettingInfo.prototype.isSubSelected = function (catId) {
        var _this = this;
        var userPrefs = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](this.user.preferences, function (item) { return item.id == _this.sltCategory.id; });
        if (!userPrefs)
            return false;
        //Get sub categories
        var userSubs = __WEBPACK_IMPORTED_MODULE_0_underscore__["map"](userPrefs.sub_categories, 'id');
        return __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](userSubs, function (item) { return item == catId; });
    };
    PreferenceSettingInfo.prototype.hideSubCategory = function () {
        this.sltCategory = null;
        this.subCategoryModal.hide();
    };
    PreferenceSettingInfo.prototype.onHidden = function () {
        this.sltCategory = null;
        this.subCategoryModal.hide();
    };
    PreferenceSettingInfo.prototype.onSelectCategory = function (id) {
        this.sltCategory = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](this.categories, function (item) {
            return item.id == id;
        });
        if (!this.sltCategory)
            return;
        //Show sub category modal if category has sub items
        if (this.sltCategory.sub_categories && this.sltCategory.sub_categories.length) {
            this.subCategoryModal.show();
        }
        else {
            this.uiService.updatePref(this.sltCategory, null);
        }
    };
    PreferenceSettingInfo.prototype.onSelectSubCategory = function (sub) {
        this.uiService.updatePref(this.sltCategory, sub);
    };
    PreferenceSettingInfo.prototype.updatePref = function () {
        this.apiService.updatePref(this.user.preferences);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_11" /* ViewChild */])('subCategory'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5_angular_bootstrap_md__["b" /* ModalDirective */])
    ], PreferenceSettingInfo.prototype, "subCategoryModal", void 0);
    PreferenceSettingInfo = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'preference-setting',
            template: __webpack_require__("./src/app/private/profile/preference/pref.html"),
            styles: [__webpack_require__("./src/app/private/profile/preference/pref.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_6__services_ui__["a" /* UiService */], __WEBPACK_IMPORTED_MODULE_3_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]])
    ], PreferenceSettingInfo);
    return PreferenceSettingInfo;
}());



/***/ }),

/***/ "./src/app/private/profile/profile.css":
/***/ (function(module, exports) {

module.exports = ".profile {\n  width: 80vw;\n  padding: 10px 0px;\n  margin: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.profile > .menu {\n  width: 15vw;\n}\n\n.profile > .menu > ul {\n  margin: 0;\n  padding: 0;\n}\n\n.profile > .menu > ul > li {\n  list-style: none;\n}\n\n.profile > .menu > ul > li > a {\n  display: block;\n  padding: 8px 10px;\n  border-bottom: #e6e6e6 1px solid;\n  border-left: #eee 1px solid;\n}\n\n.profile > .menu > ul > li > a:hover {\n  color: #e52402;\n  border-left: #c00 1px solid;\n}\n\n.profile > .content {\n  width: 65vw;\n  background-color: white;\n}"

/***/ }),

/***/ "./src/app/private/profile/profile.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"profile\">\n  <div class=\"menu\">\n    <ul>\n      <li (click)=\"goto('my-profile')\">\n        <a [ngStyle]=\"route == 'my-profile' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n          <i class=\"fas fa-user-circle\"></i>\n          <span>My Profile</span>\n          <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n        </a>\n      </li>\n      <li (click)=\"goto('account-setting')\">\n        <a [ngStyle]=\"route == 'account-setting' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n          <i class=\"fas fa-cog\"></i>\n          <span>Account Settings</span>\n          <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n        </a>\n      </li>\n      <li (click)=\"goto('preference')\">\n        <a [ngStyle]=\"route == 'preference' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n          <i class=\"fas fa-link\"></i>\n          <span>Preferences</span>\n          <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n        </a>\n      </li>\n      <li (click)=\"goto('timeline')\">\n        <a [ngStyle]=\"route == 'timeline' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n          <i class=\"fas fa-indent\"></i>\n          <span>My Timeline</span>\n          <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n        </a>\n      </li>\n      <!-- <li (click)=\"goto('calendar')\">\n        <a [ngStyle]=\"route == 'calendar' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n          <i class=\"fas fa-calendar\"></i>\n          <span>My Calendar</span>\n          <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n        </a>\n      </li> -->\n      <li (click)=\"goto('invitation')\">\n        <a [ngStyle]=\"route == 'invitation' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n          <i class=\"fas fa-fighter-jet\"></i>\n          <span>My Invitations</span>\n          <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n        </a>\n      </li>\n      <!-- <li (click)=\"goto('save-item')\">\n        <a [ngStyle]=\"route == 'save-item' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n          <i class=\"fas fa-bookmark\"></i>\n          <span>My Saved Items</span>\n          <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n        </a>\n      </li> -->\n    </ul>\n  </div>\n  <div class=\"content\">\n    <my-profile *ngIf=\"route == 'my-profile'\"></my-profile>\n    <account-setting *ngIf=\"route == 'account-setting'\"></account-setting>\n    <preference-setting *ngIf=\"route == 'preference'\"></preference-setting>\n    <timeline-info *ngIf=\"route == 'timeline'\"></timeline-info>\n    <calendar-info *ngIf=\"route == 'calendar'\"></calendar-info>\n    <invitation-setting *ngIf=\"route == 'invitation'\"></invitation-setting>\n    <saved-item *ngIf=\"route =='save-item'\"></saved-item>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/private/profile/profile.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProfilePage = /** @class */ (function () {
    function ProfilePage(store, apiService, router) {
        this.store = store;
        this.apiService = apiService;
        this.router = router;
        this.route = 'account-setting';
    }
    ProfilePage.prototype.goto = function (route) {
        this.route = route;
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'profile',
            template: __webpack_require__("./src/app/private/profile/profile.html"),
            styles: [__webpack_require__("./src/app/private/profile/profile.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], ProfilePage);
    return ProfilePage;
}());



/***/ }),

/***/ "./src/app/private/profile/save-item/save-item.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/private/profile/save-item/save-item.html":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/private/profile/save-item/save-item.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SavedItemInfo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SavedItemInfo = /** @class */ (function () {
    function SavedItemInfo(store, apiService, router) {
        this.store = store;
        this.apiService = apiService;
        this.router = router;
    }
    SavedItemInfo = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'saved-item',
            template: __webpack_require__("./src/app/private/profile/save-item/save-item.html"),
            styles: [__webpack_require__("./src/app/private/profile/save-item/save-item.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], SavedItemInfo);
    return SavedItemInfo;
}());



/***/ }),

/***/ "./src/app/private/profile/setting/setting.css":
/***/ (function(module, exports) {

module.exports = ".setting {\n  margin: 5px;\n}\n\n.setting > .row {\n  width: 100%;\n}\n\n.setting > .row > div > div {\n  margin: 10px;\n}\n\n.setting > .row > button {\n  margin: 10px auto;\n}\n"

/***/ }),

/***/ "./src/app/private/profile/setting/setting.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"setting\" *ngIf=\"user\">\n  <div class=\"row\">\n    <div class=\"col-lg-6\">\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"name\" class=\"form-control\" [(ngModel)]=\"user.username\">\n        <label for=\"name\" class=\"\">Your Name</label>\n      </div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"email\" class=\"form-control\" [(ngModel)]=\"user.email\">\n        <label for=\"email\" class=\"\">Your Email</label>\n      </div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"phone\" class=\"form-control\" [(ngModel)]=\"user.phone\">\n        <label for=\"phone\" class=\"\">Your Phone</label>\n      </div>\n      <div class=\"btn-group\" dropdown style=\"display: block;\">\n        <h6 style=\"font-size: 13px;\">Who can see my friends?</h6>\n        <button dropdownToggle type=\"button\" class=\"btn btn-primary dropdown-toggle waves-light\" mdbRippleRadius>\n          {{getPermissionTitle('friends')}}\n        </button>\n        <div class=\"dropdown-menu dropdown-primary\">\n          <a class=\"dropdown-item\" (click)=\"updatePermission('friends', 'public')\">Public</a>\n          <a class=\"dropdown-item\" (click)=\"updatePermission('friends', 'friends')\">Friends</a>\n          <a class=\"dropdown-item\" (click)=\"updatePermission('friends', 'fof')\">Friends of Friends</a>\n          <a class=\"dropdown-item\" (click)=\"null\">Selected friends</a>\n          <a class=\"dropdown-item\" (click)=\"updatePermission('friends', 'onlyMe')\">Only me</a>\n        </div>\n      </div>\n      <div class=\"btn-group\" dropdown style=\"display: block;\">\n        <h6 style=\"font-size: 13px;\">Who can send friend requests to me?</h6>\n        <button dropdownToggle type=\"button\" class=\"btn btn-primary dropdown-toggle waves-light\" mdbRippleRadius>\n          {{getPermissionTitle('friend_request')}}\n        </button>\n        <div class=\"dropdown-menu dropdown-primary\">\n          <a class=\"dropdown-item\" (click)=\"updatePermission('friend_request', 'public')\">Public</a>\n          <a class=\"dropdown-item\" (click)=\"updatePermission('friend_request', 'friends')\">Friends</a>\n          <a class=\"dropdown-item\" (click)=\"updatePermission('friend_request', 'fof')\">Friends of Friends</a>\n          <a class=\"dropdown-item\" (click)=\"null\">Selected friends</a>\n          <a class=\"dropdown-item\" (click)=\"updatePermission('friend_request', 'onlyMe')\">Only me</a>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-lg-6\">\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"age\" class=\"form-control\" [(ngModel)]=\"user.age\">\n        <label for=\"age\" class=\"\">Your Age</label>\n      </div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"location\" class=\"form-control\" [(ngModel)]=\"user.location\">\n        <label for=\"location\" class=\"\">Your Location</label>\n      </div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"status\" class=\"form-control\" [(ngModel)]=\"user.birthday\">\n        <label for=\"birthday\" class=\"\">Your Birthday</label>\n      </div>\n      <div class=\"btn-group\" dropdown style=\"display: block;\">\n        <h6 style=\"font-size: 13px;\">Who can contact me?</h6>\n        <button dropdownToggle type=\"button\" class=\"btn btn-primary dropdown-toggle waves-light\" mdbRippleRadius>\n          {{getPermissionTitle('contact_me')}}\n        </button>\n        <div class=\"dropdown-menu dropdown-primary\">\n          <a class=\"dropdown-item\" (click)=\"updatePermission('contact_me', 'public')\">Public</a>\n          <a class=\"dropdown-item\" (click)=\"updatePermission('contact_me', 'friends')\">Friends</a>\n          <a class=\"dropdown-item\" (click)=\"updatePermission('contact_me', 'fof')\">Friends of Friends</a>\n          <a class=\"dropdown-item\" (click)=\"null\">Selected friends</a>\n          <a class=\"dropdown-item\" (click)=\"updatePermission('contact_me', 'onlyMe')\">Only me</a>\n        </div>\n      </div>\n      <div class=\"btn-group\" dropdown style=\"display: block;\">\n        <h6 style=\"font-size: 13px;\">Who can send message to me?</h6>\n        <button dropdownToggle type=\"button\" class=\"btn btn-primary dropdown-toggle waves-light\" mdbRippleRadius>\n          {{getPermissionTitle('message')}}\n        </button>\n        <div class=\"dropdown-menu dropdown-primary\">\n          <a class=\"dropdown-item\" (click)=\"updatePermission('message', 'public')\">Public</a>\n          <a class=\"dropdown-item\" (click)=\"updatePermission('message', 'friends')\">Friends</a>\n          <a class=\"dropdown-item\" (click)=\"updatePermission('message', 'fof')\">Friends of Friends</a>\n          <a class=\"dropdown-item\" (click)=\"null\">Selected friends</a>\n          <a class=\"dropdown-item\" (click)=\"updatePermission('message', 'onlyMe')\">Only me</a>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"row\">\n    <button type=\"button\" class=\"btn btn-success waves-light\" aria-label=\"Update\" (click)=\"updateAcc()\">Update</button>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/private/profile/setting/setting.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountSettingInfo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AccountSettingInfo = /** @class */ (function () {
    function AccountSettingInfo(store, apiService, router) {
        var _this = this;
        this.store = store;
        this.apiService = apiService;
        this.router = router;
        this.user = {};
        store.select('user').subscribe(function (usr) {
            console.log(usr);
            if (!usr)
                return;
            _this.user.username = usr.username;
            _this.user.age = usr.age;
            _this.user.email = usr.email;
            _this.user.phone = usr.phone;
            _this.user.birthday = new Date(usr.birthday).toDateString();
            _this.user.location = usr.location;
            _this.user.id = usr.id;
            _this.user.permissions = Object.assign({}, usr.permissions);
        });
    }
    AccountSettingInfo.prototype.getPermissionTitle = function (mode) {
        if (!this.user.permissions)
            return 'Unknown';
        var resource = '';
        switch (mode) {
            case 'contact_me':
                resource = this.user.permissions.contact_me[this.user.permissions.contact_me.length - 1];
                break;
            case 'message':
                resource = this.user.permissions.message[this.user.permissions.message.length - 1];
                break;
            case 'friends':
                resource = this.user.permissions.friends[this.user.permissions.friends.length - 1];
                break;
            case 'friend_request':
                resource = this.user.permissions.friend_request[this.user.permissions.friend_request.length - 1];
                break;
        }
        switch (resource) {
            case 'public':
                return 'Public';
            case 'onlyMe':
                return 'Only Me';
            case 'friends':
                return 'Friends';
            case 'fof':
                return 'Friends Of Friends';
        }
    };
    AccountSettingInfo.prototype.updateAcc = function () {
        this.apiService.updateAcc({
            username: this.user.username,
            birthday: this.user.birthday,
            phone: this.user.phone
        });
    };
    AccountSettingInfo.prototype.updatePermission = function (resource, mode) {
        this.apiService.updatePermission(resource, mode);
    };
    AccountSettingInfo = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'account-setting',
            template: __webpack_require__("./src/app/private/profile/setting/setting.html"),
            styles: [__webpack_require__("./src/app/private/profile/setting/setting.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], AccountSettingInfo);
    return AccountSettingInfo;
}());



/***/ }),

/***/ "./src/app/private/profile/timeline/timeline.css":
/***/ (function(module, exports) {

module.exports = ".timeline {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  position: relative;\n}\n\n.timeline > .item {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  padding: 10px;\n}\n\n.timeline > .item > .icon {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  width: 50px;\n  height: 50px;\n  min-width: 50px;\n  min-height: 50px;\n  padding: 10px;\n  margin: 5px;\n  border-radius: 50%;\n  background-color: #03A9F4;\n  z-index: 2;\n}\n\n.timeline > .item > .icon > i {\n  margin: auto;\n  color: white;\n  font-size: 25px;\n}\n\n.timeline > .item > .content {\n  padding: 10px;\n}\n\n.timeline > .item > .content > .time {\n  font-size: 13px;\n}\n\n.timeline > hr {\n  position: absolute;\n  height: 99%;\n  border: 1px solid #d5d5d5;\n  left: 40px;\n}"

/***/ }),

/***/ "./src/app/private/profile/timeline/timeline.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"timeline\" *ngIf=\"user\">\n  <hr/>\n  <div class=\"item\" *ngFor=\"let item of user.uploads\">\n    <a class=\"icon\">\n      <i class=\"fas fa-images\" aria-hidden=\"true\" *ngIf=\"item.content_type == 'photo'\"></i>\n      <i class=\"fas fa-video\" aria-hidden=\"true\" *ngIf=\"item.content_type == 'video'\"></i>\n      <i class=\"fas fa-comment\" aria-hidden=\"true\" *ngIf=\"item.content_type == 'text'\"></i>\n    </a>\n    <div class=\"content\">\n      <h5 class=\"font-weight-bold\">{{item.caption && item.caption != '' ? item.caption : 'No caption'}}</h5>\n      <p class=\"time\" style=\"margin-bottom: 0.3rem; font-size: 0.7rem; color: #565656;\">\n        <i class=\"fa fa-clock-o\" aria-hidden=\"true\"></i>\n        <span>{{item.date_time * 1000 | date}}</span>\n      </p>\n      <p class=\"description\" *ngIf=\"item.content_type == 'text'\" style=\"margin-bottom: 0;\">\n        {{item.content}}\n      </p>\n      <img style=\"width: 50%;\" src=\"{{item.content}}\" alt=\"Card image cap\" *ngIf=\"item.content_type == 'photo'\" />\n      <video style=\"width: 50%;\" poster=\"{{item.thumbnail}}\" src=\"{{item.content}}\" alt=\"Card image cap\" *ngIf=\"item.content_type == 'video'\" controls></video>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/private/profile/timeline/timeline.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimelineInfo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TimelineInfo = /** @class */ (function () {
    function TimelineInfo(store, apiService, router) {
        var _this = this;
        this.store = store;
        this.apiService = apiService;
        this.router = router;
        this.store.select('user').subscribe(function (u) {
            if (!_this.user && u) {
                _this.apiService.fetchFriends();
                _this.apiService.fetchUpload();
            }
            _this.user = u;
        });
    }
    TimelineInfo = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'timeline-info',
            template: __webpack_require__("./src/app/private/profile/timeline/timeline.html"),
            styles: [__webpack_require__("./src/app/private/profile/timeline/timeline.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], TimelineInfo);
    return TimelineInfo;
}());



/***/ }),

/***/ "./src/app/private/upload/create-upload/create.css":
/***/ (function(module, exports) {

module.exports = ".galery {\n  width: 100%;\n  height: 30vh;\n  border: solid 1px #d5d5d5;\n  margin: 0px;\n  margin-top: 20px;\n  background-color: white;\n}\n\nagm-map {\n  height: 300px;\n}\n\n.confirm-modal .info {\n  height: 30vh;\n  padding: 10px;\n  margin: 5px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.info > img {\n  min-width: 200px;\n  width: 30%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  border: solid 1px #d5d5d5;\n}\n\n.info > .content {\n  position: relative;\n  width: 70%;\n  padding: 5px 10px;\n  background-color: white;\n}\n\n.info > .content > .name {\n  font-weight: 700;\n}\n\n.info > .content > p {\n  font-size: 0.9rem;\n  margin-bottom: 0.5rem;\n}\n\n.info > .content > .address {\n  color: #0275d8;\n}\n\n.info > .content > .address:hover {\n  text-decoration: underline;\n}\n\n.info > .content > .status {\n  position: absolute;\n  bottom: 20px;\n  right: 20px;\n  padding: 5px 20px;\n  border-radius: 5px;\n  color: white;\n}\n\n.info > .content > .pending {\n  background-color: #ffe500;\n}\n\n.info > .content > .confirm {\n  background-color: #61ff6d;\n}\n\n.info > .content > .reject {\n  background-color: red;\n}\n"

/***/ }),

/***/ "./src/app/private/upload/create-upload/create.html":
/***/ (function(module, exports) {

module.exports = "<div>\n  <div class=\"row\" style=\"width: 100%; margin-top: 20px;\">\n    <div class=\"col-lg-6\">\n      <!-- Name -->\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"name\" class=\"form-control\" [(ngModel)]=\"feed.name\" placeholder=\"\">\n        <label style=\"right: 0; left: auto; color: red;\">*</label>\n        <label for=\"name\" class=\"\">Name</label>\n      </div>\n      <!-- Country -->\n      <div class=\"btn-group\" dropdown style=\"display: block; top: -10px; margin-bottom: 15px;\">\n        <h6 style=\"font-size: 13px; color: #757575;\">Country</h6>\n        <button style=\"padding: 10px;\" dropdownToggle type=\"button\" class=\"btn btn-primary dropdown-toggle waves-light\"\n          mdbRippleRadius>\n          {{feed.country}}\n        </button>\n        <div class=\"dropdown-menu dropdown-primary\">\n          <a class=\"dropdown-item\" (click)=\"setCountry(c)\" *ngFor=\"let c of countries\">{{c}}</a>\n        </div>\n      </div>\n      <!-- Postcode -->\n      <div class=\"md-form\">\n        <input style=\"margin-bottom: 0;\" mdbActive type=\"text\" id=\"postcode\" class=\"form-control\" [(ngModel)]=\"feed.postcode\"\n          placeholder=\"\">\n        <label style=\"right: 0; left: auto; color: red;\">*</label>\n        <label for=\"postcode\" class=\"\">Postcode</label>\n        <!-- <a style=\"font-size: 0.8rem; color: #0275d8;\" (click)=\"openSuggestionAddress()\">Suggest address? (only UK)</a> -->\n      </div>\n      <!-- Address with UK -->\n      <!-- <div class=\"md-form\" *ngIf=\"feed.country === 'UK' || !editAddress\">\n        <input style=\"margin-bottom: 0;\" disabled mdbActive type=\"text\" id=\"fullAddress\" class=\"form-control\" placeholder=\"Click Find with Postcode to get address or click Enter manually to input (only UK)\" [value]=\"feed.address1 ? ((feed.address1 ? (feed.address1 + ', ') : '') + (feed.address2 ? (feed.address2 + ', ') : '') + (feed.city ? (feed.city + ', ') : '') + (feed.county ? feed.county : '')) : ''\">\n        <label for=\"fullAddress\" class=\"\">Full Address</label>\n      </div> -->\n      <!-- Find button -->\n      <!-- <div class=\"btn-group\" dropdown style=\"display: block; margin-bottom: 15px;\">\n        <button type=\"button\" class=\"btn btn-primary waves-light\" *ngIf=\"feed.country === 'UK' && !editAddress\" mdbRippleRadius style=\"padding: 10px 40px;\" (click)=\"findAddress()\">\n          <label style=\"margin-bottom: 0;\">Find</label>\n        </button>\n        <a style=\"font-size: 1rem; color: #0275d8; text-decoration: underline;\" (click)=\"editAddressByManually()\" *ngIf=\"feed.country === 'UK' && !editAddress\">Enter manually</a>\n        <a style=\"font-size: 1rem; color: #0275d8; text-decoration: underline;\" (click)=\"exitAddressByManually()\" *ngIf=\"feed.country === 'UK' && editAddress\">Back</a>\n      </div> -->\n      <!-- Address1 -->\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"address1\" class=\"form-control\" [(ngModel)]=\"feed.address1\" placeholder=\"\">\n        <label style=\"right: 0; left: auto; color: red;\">*</label>\n        <label for=\"address1\" class=\"\">Address1</label>\n      </div>\n      <!-- Address2 -->\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"address2\" class=\"form-control\" [(ngModel)]=\"feed.address2\" placeholder=\"\">\n        <label for=\"address2\" class=\"\">Address2</label>\n      </div>\n      <!-- City -->\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"city\" class=\"form-control\" [(ngModel)]=\"feed.city\" placeholder=\"\">\n        <label style=\"right: 0; left: auto; color: red;\">*</label>\n        <label for=\"city\" class=\"\">City</label>\n      </div>\n      <!-- County -->\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"county\" class=\"form-control\" [(ngModel)]=\"feed.county\" placeholder=\"\">\n        <label style=\"right: 0; left: auto; color: red;\">*</label>\n        <label for=\"county\" class=\"\">County</label>\n      </div>\n      <!-- Preview map -->\n      <div class=\"btn-group\" dropdown style=\"display: block; margin-bottom: 15px;\">\n        <button type=\"button\" class=\"btn btn-primary waves-light\" mdbRippleRadius style=\"padding: 10px 40px;\" (click)=\"xpMap.show()\">\n          <label style=\"margin-bottom: 0;\">Show on map</label>\n        </button>\n        <div *ngIf=\"feed.lat && feed.long\">(latitude: {{feed.lat}}, longitude: {{feed.long}})</div>\n      </div>\n      <!-- Image -->\n      <div class=\"btn-group\" dropdown style=\"display: block; margin-bottom: 15px;\">\n        <button type=\"button\" class=\"btn btn-primary waves-light\" mdbRippleRadius style=\"padding: 10px 40px;\">\n          <label for=\"imageSlt\" class=\"fas fa-images\" style=\"margin-bottom: 0; font-size: 0.8rem;\">\n            <span style=\"margin-left: 10px; font: 400 13.3333px Arial;\">Add Image</span>\n          </label>\n        </button>\n        <label style=\"right: 0; left: auto; color: red;\">*</label>\n        <input id=\"imageSlt\" style=\"opacity: 0; position: absolute; z-index: -1;\" type=\"file\" accept=\".jpg,.png\"\n          (change)=\"onImageSlt($event)\" />\n      </div>\n      <div style=\"width: 100%; height: 210px; border: solid 1px #d5d5d5;\">\n        <img *ngIf=\"imageSlt\" src=\"{{imageSlt}}\" style=\"width: 100%; height: 100%; object-fit: contain;\" />\n      </div>\n    </div>\n    <div class=\"col-lg-6\">\n      <!-- Category -->\n      <div class=\"btn-group\" dropdown style=\"display: block; top: -10px; margin-bottom: 15px;\">\n        <h6 style=\"font-size: 13px; color: #757575;\">Category</h6>\n        <button style=\"padding: 10px;\" dropdownToggle type=\"button\" class=\"btn btn-primary dropdown-toggle waves-light\"\n          mdbRippleRadius>\n          {{feed.category.name}}\n        </button>\n        <div class=\"dropdown-menu dropdown-primary\">\n          <a class=\"dropdown-item\" (click)=\"setFeedCategory(cat.id)\" *ngFor=\"let cat of categories\">{{cat.name}}</a>\n        </div>\n      </div>\n      <!-- Description -->\n      <div class=\"md-form\">\n        <textarea mdbActive type=\"text\" id=\"description\" class=\"form-control\" [(ngModel)]=\"feed.description\" rows=\"4\"\n          style=\"height: 100%\" placeholder=\"\"></textarea>\n        <label style=\"right: 0; left: auto; color: red;\">*</label>\n        <label for=\"description\" class=\"\" style=\"position: absolute; bottom: 70px;\">Description</label>\n      </div>\n      <!-- Tag -->\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"tag\" class=\"form-control\" [(ngModel)]=\"feed.tag\" placeholder=\"\">\n        <label for=\"tag\" class=\"\">Tag</label>\n      </div>\n      <!-- Phone -->\n      <div class=\"md-form\">\n        <input mdbActive type=\"tel\" id=\"phone\" #phoneInput class=\"form-control\" placeholder=\"\" (input)=\"onPhoneChange($event)\">\n        <label for=\"phone\" class=\"\">Phone</label>\n      </div>\n      <!-- Email -->\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"email\" class=\"form-control\" style=\"margin-bottom: 0px;\" [(ngModel)]=\"feed.email\" placeholder=\"\">\n        <label for=\"phone\" class=\"\">Email</label>\n        <i style=\"font-size: 0.7rem; color: red;\" *ngIf=\"feed.email && validateEmail(feed.email) === false\">Your email is invalid</i>\n      </div>\n      <!-- Website -->\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"website\" class=\"form-control\" [(ngModel)]=\"feed.website\" placeholder=\"\">\n        <label for=\"website\" class=\"\">Website</label>\n      </div>\n      <!-- Social media -->\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"social\" class=\"form-control\" [(ngModel)]=\"feed.social\" placeholder=\"\">\n        <label for=\"tag\" class=\"\">Social media</label>\n      </div>\n      <!-- Type (event/place) -->\n      <div class=\"btn-group\" dropdown style=\"display: block; top: -10px; margin-bottom: 15px;\">\n        <h6 style=\"font-size: 13px; color: #757575;\">Type</h6>\n        <button style=\"padding: 10px;\" dropdownToggle type=\"button\" class=\"btn btn-primary dropdown-toggle waves-light\"\n          mdbRippleRadius>\n          {{feed.type}}\n        </button>\n        <div class=\"dropdown-menu dropdown-primary\">\n          <a class=\"dropdown-item\" (click)=\"setFeedType(type.id)\" *ngFor=\"let type of types\">{{type.name}}</a>\n        </div>\n      </div>\n      <!-- Opening times -->\n      <div class=\"md-form\" *ngIf=\"feed.type === 'place'\">\n        <input mdbActive type=\"text\" id=\"openTime\" class=\"form-control\" [(ngModel)]=\"feed.openingTimes\" placeholder=\"\">\n        <label for=\"openTime\" class=\"\">Opening times</label>\n      </div>\n      <!-- Start time, End time -->\n      <div *ngIf=\"feed.type === 'event'\" style=\"display: flex;\">\n        <div class=\"md-form\" style=\"margin-right: 15px;\">\n          <input mdbActive type=\"text\" id=\"startTime\" class=\"form-control\" [(ngModel)]=\"feed.startTime\" placeholder=\"\">\n          <label for=\"startTime\" class=\"\">Start time</label>\n        </div>\n        <div class=\"md-form\">\n          <input mdbActive type=\"text\" id=\"startTime\" class=\"form-control\" [(ngModel)]=\"feed.endTime\" placeholder=\"\">\n          <label for=\"endTime\" class=\"\">End time</label>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div>\n    <i style=\"color: red;\" *ngIf=\"isEmpty\">(*) is required. Please fill your information.</i>\n    <i style=\"color: red;\" *ngIf=\"!isEmpty && feed.email && validateEmail(feed.email) === false\">Some fields are invalid. Please check and upload again</i>\n  </div>\n  <div class=\"row\">\n      <!-- [ngStyle]=\"{'pointer-events': isEmpty || (feed.email && validateEmail(feed.email) === false) ? 'none' : 'auto', 'opacity': isEmpty || (feed.email && validateEmail(feed.email) === false) ? '0.5' : '1'}\" -->\n    <button type=\"button\" class=\"btn btn-primary waves-light\" style=\"margin: 1rem auto;\" mdbRippleRadius (click)=\"gotoConfirm()\">Upload\n      Feed</button>\n  </div>\n</div>\n\n<div mdbModal #xpMap=\"mdb-modal\" (onShown)=\"onMapShow()\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\"\n  [config]=\"{backdrop: false, ignoreBackdropClick: true}\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title w-100\">Preview feed on map</h4>\n      </div>\n      <div class=\"modal-body\">\n        <agm-map #map [zoom]=\"17\" [latitude]=\"feed.lat || 51.5414494\" [longitude]=\"feed.long || -0.0964918\">\n          <agm-marker [latitude]=\"feed.lat || 51.5414494\" [longitude]=\"feed.long || -0.0964918\" [markerDraggable]=\"true\" (dragEnd)=\"onDragMarker($event)\"></agm-marker>\n        </agm-map>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary waves-light\" aria-label=\"Close\" (click)=\"xpMap.hide()\"\n          mdbRippleRadius>Close</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div mdbModal #suggestAddressModal=\"mdb-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\"\n  [config]=\"{backdrop: false, ignoreBackdropClick: true}\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title w-100\">Choose your address</h4>\n      </div>\n      <div class=\"modal-body\">\n        <select (change)=\"onSelectSuggestAddress($event)\">\n          <option *ngFor=\"let item of suggestAddress; let i = index\" [value]=\"i\">{{item.addressline1}},\n            {{item.addressline2}}</option>\n        </select>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-primary waves-light\" aria-label=\"Close\" (click)=\"select()\" mdbRippleRadius>Select</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div mdbModal #confirm=\"mdb-modal\" class=\"modal fade confirm-modal\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\"\n  [config]=\"{backdrop: false, ignoreBackdropClick: true}\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\" style=\"width: 800px;\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title w-100\">Confirm your XP Feed</h4>\n      </div>\n      <div class=\"modal-body\">\n        <div class=\"info\">\n          <img src=\"{{imageSlt}}\" class=\"img-fluid\" alt=\"Responsive image\" *ngIf=\"imageSlt\">\n          <img class=\"img-fluid\" src=\"assets/graphics/placeholders2.png\" alt=\"Card image cap\" *ngIf=\"!imageSlt\">\n          <div class=\"content\">\n            <h3 class=\"name\">{{feed.name}}</h3>\n            <p class=\"address\" *ngIf=\"feed.address\">\n              <i class=\"fa fa-location-arrow pr-2\"></i>\n              <a>{{feed.address}}</a>\n            </p>\n            <p class=\"city\" *ngIf=\"feed.city\">\n              <i class=\"fa fa-circle pr-2\" style=\"font-size: 10px;\"></i>\n              <a>{{feed.city}}</a>\n            </p>\n            <p class=\"phone\" *ngIf=\"feed.phone\">\n              <i class=\"fa fa-phone-square blue-text pr-2\"></i>{{feed.phone}}\n            </p>\n            <p class=\"website\" *ngIf=\"feed.website\">\n              <i class=\"fab fa-chrome blue-text pr-2\"></i>\n              <a href=\"{{feed.website}}\" target=\"_blank\">{{feed.website}}</a>\n            </p>\n            <label class=\"status pending\">Pending</label>\n          </div>\n        </div>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-primary waves-light\" aria-label=\"Close\" (click)=\"createFeed()\" mdbRippleRadius>Confirm</button>\n        <button type=\"button\" style=\"color: black !important;\" class=\"btn btn-light waves-light\" aria-label=\"Close\" (click)=\"confirm.hide()\" mdbRippleRadius>Cancel</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<style>\n#fullAddress::placeholder {\n  color: #cacaca;\n  font-size: 0.7rem;\n}\n.md-form {\n  margin-bottom: 1rem;\n}\n</style>"

/***/ }),

/***/ "./src/app/private/upload/create-upload/create.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateNewUpload; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__agm_core__ = __webpack_require__("./node_modules/@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_app_utils_util__ = __webpack_require__("./src/app/utils/util.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PHONE_REGEX = new RegExp(/^(1\s?)?((\([0-9]{3}\))|[0-9]{3})[\s\\-]?[\0-9]{3}[\s\\-]?[0-9]{4}$/);
var PHONE_FORMAT_REGEX = new RegExp(/(\d{3})?(\d{3})?(\d{4})?/);
var CreateNewUpload = /** @class */ (function () {
    function CreateNewUpload(apiService, utilService, store, router) {
        var _this = this;
        this.apiService = apiService;
        this.utilService = utilService;
        this.store = store;
        this.router = router;
        this.feed = {
            name: null,
            postcode: null,
            address1: null,
            address2: null,
            city: null,
            county: null,
            country: 'UK',
            email: null,
            phone: null,
            website: null,
            privateAddress: false,
            openingTimes: null,
            startTime: null,
            endTime: null,
            category: {
                id: null,
                name: null
            },
            lat: null,
            long: null,
            image: null,
            tag: null,
            social: null,
            description: null,
            type: 'place'
        };
        this.types = [{
                id: 1,
                name: 'event'
            }, {
                id: 2,
                name: 'place'
            }];
        this.imageSlt = null;
        this.isEmpty = false;
        this.isInvalid = false;
        this.suggestAddress = [];
        this.sltSuggestAddress = null;
        this.countries = ["UK", "US"];
        this.editAddress = false;
        console.log('Upload');
        //Fetching all categories
        this.apiService.fetchCategories();
        //Get all categories
        store.select('categories').subscribe(function (items) {
            if (!items)
                return;
            _this.categories = items;
            _this.feed.category = {
                id: _this.categories[0] ? _this.categories[0].id : '',
                name: _this.categories[0] ? _this.categories[0].name : ''
            };
        });
    }
    CreateNewUpload.prototype.setFeedType = function (typeId) {
        var sltType = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](this.types, function (item) { return item.id == typeId; });
        this.feed.type = sltType.name;
    };
    CreateNewUpload.prototype.setFeedCategory = function (catId) {
        var sltCategory = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](this.categories, function (item) { return item.id == catId; });
        if (!sltCategory)
            return;
        this.feed.category = {
            id: sltCategory.id,
            name: sltCategory.name
        };
    };
    CreateNewUpload.prototype.setCountry = function (c) {
        this.feed.country = c;
        this.feed.address1 = null;
        this.feed.address2 = null;
        this.feed.city = null;
        this.feed.county = null;
        this.editAddress = false;
    };
    CreateNewUpload.prototype.editAddressByManually = function () {
        this.editAddress = true;
    };
    CreateNewUpload.prototype.onImageSlt = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var src = e.target.result;
                _this.imageSlt = src;
            };
            reader.readAsDataURL(event.target.files[0]);
            this.feed.image = event.target.files[0];
        }
    };
    CreateNewUpload.prototype.gotoConfirm = function () {
        if (!this.feed.name || !this.feed.address1 || !this.feed.postcode || !this.feed.city || !this.feed.county || !this.feed.image || !this.feed.description) {
            this.isEmpty = true;
            return;
        }
        this.isEmpty = false;
        if (!this.validateEmail(this.feed.email)) {
            this.isInvalid = true;
            return;
        }
        this.confirmModalElement.show();
    };
    CreateNewUpload.prototype.createFeed = function () {
        var _this = this;
        if (this.feed.lat && this.feed.long) {
            this.apiService.createXPFeed(this.feed);
        }
        else {
            this.apiService.findLatLongByAddress(this.feed.address1 + ', ' + this.feed.address2 + ', ' + this.feed.city + ', ' + this.feed.county + ', ' + this.feed.country).then(function (res) {
                _this.feed.lat = res.lat;
                _this.feed.long = res.long;
                _this.apiService.createXPFeed(_this.feed);
            }).catch(function (err) {
                console.log(err);
            });
        }
        this.feed = {
            name: null,
            postcode: null,
            address1: null,
            address2: null,
            city: null,
            county: null,
            country: 'UK',
            email: null,
            phone: null,
            website: null,
            privateAddress: false,
            openingTimes: null,
            startTime: null,
            endTime: null,
            category: {
                id: null,
                name: null
            },
            lat: null,
            long: null,
            image: null,
            tag: null,
            social: null,
            description: null,
            type: 'place'
        };
        this.phoneInput.nativeElement.value = '';
        this.editAddress = false;
        this.confirmModalElement.hide();
    };
    CreateNewUpload.prototype.onMapShow = function () {
        var _this = this;
        this.apiService.findLatLongByPostcode(this.feed.postcode).then(function (res) {
            console.log(res);
            _this.feed.lat = res.lat;
            _this.feed.long = res.long;
            _this.map.triggerResize();
        }).catch(function (err) {
            console.log(err);
        });
    };
    CreateNewUpload.prototype.onDragMarker = function (marker) {
        console.log('move marker', marker);
        this.feed.lat = Number(marker.coords.lat).toFixed(6);
        this.feed.long = Number(marker.coords.lng).toFixed(6);
        // this.apiService.findAddressByLatLong(this.feed.lat, this.feed.long).then((res: any) => {
        //   var address = res.result
        //   if(!address.address_components.length) return
        //   this.feed.address1 = address.address_components[0] ? address.address_components[0].long_name : ''
        //   this.feed.address2 = address.address_components[1] ? address.address_components[1].long_name : ''
        //   this.feed.city = address.address_components[2] ? address.address_components[2].long_name : ''
        //   this.feed.county = address.address_components[3] ? address.address_components[3].long_name : ''
        // }).catch(e => {
        //   console.log(e)
        // })
        // this.apiService.findPostcodeByLatLong(this.feed.lat, this.feed.long).then((res: any) => {
        //   this.feed.postcode = res.data.postcode
        // }).catch(e => {
        //   console.log(e)
        // })
    };
    CreateNewUpload.prototype.onSelectSuggestAddress = function (e) {
        this.sltSuggestAddress = this.suggestAddress.find(function (item, index) { console.log(index, Number(e.target.value)); return index == Number(e.target.value); });
    };
    CreateNewUpload.prototype.openSuggestionAddress = function () {
        var _this = this;
        if (!this.feed.postcode) {
            return;
        }
        this.utilService.getAddressByPostcode(this.feed.postcode).then(function (addresses) {
            _this.suggestAddress = addresses;
            if (_this.suggestAddress && _this.suggestAddress.length) {
                _this.sltSuggestAddress = _this.suggestAddress[0];
            }
            _this.suggestAddressElement.show();
        }).catch(function (err) {
            console.log(err);
        });
    };
    CreateNewUpload.prototype.select = function () {
        if (this.sltSuggestAddress) {
            this.feed.address1 = this.sltSuggestAddress.addressline1;
            this.feed.address2 = this.sltSuggestAddress.addressline2;
            this.feed.street = this.sltSuggestAddress.premise + ', ' + this.sltSuggestAddress.street;
            this.feed.city = this.sltSuggestAddress.posttown + ', ' + this.sltSuggestAddress.county;
            this.feed.country = 'UK';
        }
        this.suggestAddressElement.hide();
    };
    // findAddress() {
    //   this.apiService.findAddressByLatLong(this.feed.lat, this.feed.long).then((res: any) => {
    //     this.feed.lat = res.data.latitude
    //     this.feed.long = res.data.longitude
    //     this.feed.address1 = res.data.ccg
    //     this.feed.address2 = res.data.admin_ward
    //     this.feed.city = res.data.region
    //     this.feed.county = res.data.admin_county
    //   }).catch((err) => {
    //     console.log(err)
    //   })
    // }
    CreateNewUpload.prototype.exitAddressByManually = function () {
        this.editAddress = false;
    };
    CreateNewUpload.prototype.onPhoneChange = function (evt) {
        var number = evt.target.value.replace(/[^\d]/g, '');
        if (!number.length) {
            evt.target.value = this.feed.phone = '';
        }
        if (number.length > 10) {
            number = number.substr(0, 10);
        }
        if (evt) {
            evt.target.value = this.feed.phone = number.replace(PHONE_FORMAT_REGEX, '$1-$2-$3').replace('--', '-').replace(/^-/, '').replace(/-$/, '');
        }
    };
    CreateNewUpload.prototype.validateEmail = function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_11" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5__agm_core__["b" /* AgmMap */])
    ], CreateNewUpload.prototype, "map", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_11" /* ViewChild */])('suggestAddressModal'),
        __metadata("design:type", Object)
    ], CreateNewUpload.prototype, "suggestAddressElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_11" /* ViewChild */])('confirm'),
        __metadata("design:type", Object)
    ], CreateNewUpload.prototype, "confirmModalElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_11" /* ViewChild */])('phoneInput'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_core__["t" /* ElementRef */])
    ], CreateNewUpload.prototype, "phoneInput", void 0);
    CreateNewUpload = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'create-new-upload',
            template: __webpack_require__("./src/app/private/upload/create-upload/create.html"),
            styles: [__webpack_require__("./src/app/private/upload/create-upload/create.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_6_app_utils_util__["a" /* UtilService */], __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]])
    ], CreateNewUpload);
    return CreateNewUpload;
}());



/***/ }),

/***/ "./src/app/private/upload/latest-uploads/latest.css":
/***/ (function(module, exports) {

module.exports = ".latest-feed {\n  width: 60vw;\n  min-width: 900px;\n  margin: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.latest-feed > .info {\n  height: 30vh;\n  padding: 10px;\n  margin: 5px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.info > img {\n  min-width: 200px;\n  width: 30%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.info > .content {\n  position: relative;\n  width: 70%;\n  padding: 5px 10px;\n  background-color: white;\n}\n\n.info > .content > .name {\n  font-weight: 700;\n}\n\n.info > .content > p {\n  font-size: 0.9rem;\n  margin-bottom: 0.5rem;\n}\n\n.info > .content > .address {\n  color: #0275d8;\n}\n\n.info > .content > .address:hover {\n  text-decoration: underline;\n}\n\n.info > .content > .status {\n  position: absolute;\n  bottom: 20px;\n  right: 20px;\n  padding: 5px 20px;\n  border-radius: 5px;\n  color: white;\n}\n\n.info > .content > .pending {\n  background-color: #ffe500;\n}\n\n.info > .content > .confirm {\n  background-color: #61ff6d;\n}\n\n.info > .content > .reject {\n  background-color: red;\n}\n"

/***/ }),

/***/ "./src/app/private/upload/latest-uploads/latest.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"latest-feed\" *ngIf=\"xpFeeds.length\">\n  <div class=\"info\" *ngFor=\"let feed of xpFeeds\">\n    <img src=\"{{feed.image}}\" class=\"img-fluid\" alt=\"Responsive image\" *ngIf=\"feed.image\">\n    <img class=\"img-fluid\" src=\"assets/graphics/placeholders2.png\" alt=\"Card image cap\" *ngIf=\"!feed.image\">\n    <div class=\"content\">\n      <h3 class=\"name\">{{feed.name}}</h3>\n      <p class=\"address\" *ngIf=\"feed.address\">\n        <i class=\"fa fa-location-arrow pr-2\"></i>\n        <a>{{feed.address}}</a>\n      </p>\n      <p class=\"city\" *ngIf=\"feed.city\">\n        <i class=\"fa fa-circle pr-2\" style=\"font-size: 10px;\"></i>\n        <a>{{feed.city}}</a>\n      </p>\n      <p class=\"phone\" *ngIf=\"feed.phone\">\n        <i class=\"fa fa-phone-square blue-text pr-2\"></i>{{feed.phone}}\n      </p>\n      <p class=\"website\" *ngIf=\"feed.website\">\n        <i class=\"fab fa-chrome blue-text pr-2\"></i>\n        <a href=\"{{feed.website}}\" target=\"_blank\">{{feed.website}}</a>\n      </p>\n      <label class=\"{{feed.status === 0 ? 'status pending' : (feed.status === 1 ? 'status confirm' : 'status reject')}}\">\n        {{feed.status === 0 ? 'Pending' : (feed.status === 1 ? 'Confirmed' : 'Rejected')}}\n      </label>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/private/upload/latest-uploads/latest.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LatestUpload; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LatestUpload = /** @class */ (function () {
    function LatestUpload(apiService, uiService, store, router) {
        var _this = this;
        this.apiService = apiService;
        this.uiService = uiService;
        this.store = store;
        this.router = router;
        console.log('Upload');
        //Fetching all categories
        this.apiService.fetchCategories();
        //Fetching all categories
        this.apiService.getAllFeeds();
        //Get all xp feeds
        store.select('xpFeeds').subscribe(function (items) {
            if (!items)
                return;
            _this.xpFeeds = items;
            console.log(_this.xpFeeds);
        });
    }
    LatestUpload = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'latest-upload',
            template: __webpack_require__("./src/app/private/upload/latest-uploads/latest.html"),
            styles: [__webpack_require__("./src/app/private/upload/latest-uploads/latest.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_4_app_services_ui__["a" /* UiService */], __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], LatestUpload);
    return LatestUpload;
}());



/***/ }),

/***/ "./src/app/private/upload/my-uploads/uploads.css":
/***/ (function(module, exports) {

module.exports = ".upload-feed {\n  width: 60vw;\n  min-width: 900px;\n  margin: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.upload-feed > .info {\n  height: 30vh;\n  padding: 10px;\n  margin: 5px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.info > img {\n  min-width: 200px;\n  width: 30%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.info > .content {\n  position: relative;\n  width: 70%;\n  padding: 5px 10px;\n  background-color: white;\n}\n\n.info > .content > .name {\n  font-weight: 700;\n}\n\n.info > .content > p {\n  font-size: 0.9rem;\n  margin-bottom: 0.5rem;\n}\n\n.info > .content > .address {\n  color: #0275d8;\n}\n\n.info > .content > .address:hover {\n  text-decoration: underline;\n}\n\n.info > .content > .status {\n  position: absolute;\n  bottom: 20px;\n  right: 20px;\n  padding: 5px 20px;\n  border-radius: 5px;\n  color: white;\n}\n\n.info > .content > .pending {\n  background-color: #ffe500;\n}\n\n.info > .content > .confirm {\n  background-color: #61ff6d;\n}\n\n.info > .content > .reject {\n  background-color: red;\n}\n"

/***/ }),

/***/ "./src/app/private/upload/my-uploads/uploads.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"upload-feed\" *ngIf=\"xpFeeds.length\">\n  <div class=\"info\" *ngFor=\"let feed of xpFeeds\">\n    <img src=\"{{feed.image}}\" class=\"img-fluid\" alt=\"Responsive image\" *ngIf=\"feed.image\">\n    <img class=\"img-fluid\" src=\"assets/graphics/placeholders2.png\" alt=\"Card image cap\" *ngIf=\"!feed.image\">\n    <div class=\"content\">\n      <h3 class=\"name\">{{feed.name}}</h3>\n      <p class=\"address\" *ngIf=\"feed.address\">\n        <i class=\"fa fa-location-arrow pr-2\"></i>\n        <a>{{feed.address}}</a>\n      </p>\n      <p class=\"city\" *ngIf=\"feed.city\">\n        <i class=\"fa fa-circle pr-2\" style=\"font-size: 10px;\"></i>\n        <a>{{feed.city}}</a>\n      </p>\n      <p class=\"phone\" *ngIf=\"feed.phone\">\n        <i class=\"fa fa-phone-square blue-text pr-2\"></i>{{feed.phone}}\n      </p>\n      <p class=\"website\" *ngIf=\"feed.website\">\n        <i class=\"fab fa-chrome blue-text pr-2\"></i>\n        <a href=\"{{feed.website}}\" target=\"_blank\">{{feed.website}}</a>\n      </p>\n      <label class=\"{{feed.status === 0 ? 'status pending' : (feed.status === 1 ? 'status confirm' : 'status reject')}}\">\n        {{feed.status === 0 ? 'Pending' : (feed.status === 1 ? 'Confirmed' : 'Rejected')}}\n      </label>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/private/upload/my-uploads/uploads.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyUpload; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyUpload = /** @class */ (function () {
    function MyUpload(apiService, uiService, store, router) {
        var _this = this;
        this.apiService = apiService;
        this.uiService = uiService;
        this.store = store;
        this.router = router;
        console.log('My uploads');
        //Fetching all categories
        this.apiService.getMyFeed();
        //Get all xp feeds
        store.select('xpFeeds').subscribe(function (items) {
            if (!items)
                return;
            _this.xpFeeds = items;
            console.log(_this.xpFeeds);
        });
    }
    MyUpload = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'my-upload',
            template: __webpack_require__("./src/app/private/upload/my-uploads/uploads.html"),
            styles: [__webpack_require__("./src/app/private/upload/my-uploads/uploads.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_4_app_services_ui__["a" /* UiService */], __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], MyUpload);
    return MyUpload;
}());



/***/ }),

/***/ "./src/app/private/upload/upload.css":
/***/ (function(module, exports) {

module.exports = ".upload {\n  width: 80vw;\n  padding: 10px 0px;\n  margin: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.upload > .menu {\n  width: 15vw;\n}\n\n.upload > .menu > ul {\n  margin: 0;\n  padding: 0;\n}\n\n.upload > .menu > ul > li {\n  list-style: none;\n}\n\n.upload > .menu > ul > li > a {\n  display: block;\n  padding: 8px 10px;\n  border-bottom: #e6e6e6 1px solid;\n  border-left: #eee 1px solid;\n}\n\n.upload > .menu > ul > li > a:hover {\n  color: #e52402;\n  border-left: #c00 1px solid;\n}\n\n.upload > .content {\n  width: 65vw;\n  background-color: white;\n  padding: 0px 20px;\n}"

/***/ }),

/***/ "./src/app/private/upload/upload.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"upload\">\n  <div class=\"menu\">\n    <ul>\n      <li (click)=\"goto('my-upload')\">\n        <a [ngStyle]=\"route == 'my-upload' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n          <i class=\"fas fa-user-circle\"></i>\n          <span>My Upload</span>\n          <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n        </a>\n      </li>\n      <li (click)=\"goto('new-upload')\">\n        <a [ngStyle]=\"route == 'new-upload' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n          <i class=\"fas fa-cog\"></i>\n          <span>Create new upload</span>\n          <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n        </a>\n      </li>\n      <li (click)=\"goto('latest-upload')\">\n        <a [ngStyle]=\"route == 'latest-upload' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n          <i class=\"fas fa-link\"></i>\n          <span>Latest upload</span>\n          <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n        </a>\n      </li>\n    </ul>\n  </div>\n  <div class=\"content\">\n    <create-new-upload *ngIf=\"route == 'new-upload'\"></create-new-upload>\n    <latest-upload *ngIf=\"route == 'latest-upload'\"></latest-upload>\n    <my-upload *ngIf=\"route == 'my-upload'\"></my-upload>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/private/upload/upload.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UploadPage = /** @class */ (function () {
    function UploadPage(store, apiService, router) {
        this.store = store;
        this.apiService = apiService;
        this.router = router;
        this.route = 'new-upload';
    }
    UploadPage.prototype.goto = function (route) {
        this.route = route;
    };
    UploadPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/private/upload/upload.html"),
            styles: [__webpack_require__("./src/app/private/upload/upload.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_3_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], UploadPage);
    return UploadPage;
}());



/***/ }),

/***/ "./src/app/public/business/business.css":
/***/ (function(module, exports) {

module.exports = ".app-business {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.app-business > .header {\n  height: 130px;\n  background-color: #5603AD;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.app-business > .header > h2 {\n  text-align: center;\n\tfont-weight: 800;\n  font-size: 60px;\n  width: 100%;\n  color: white;\n  margin: auto;\n}\n\n.app-business > .title {\n  font-size: 20px;\n  font-weight: 500;\n  margin: 30px auto 10px auto;\n}\n\n.app-business > .form {\n  color: #5603AD;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin: 10px auto;\n}\n\n.app-business > .form > div {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  width: 30vw;\n  margin: 10px;\n}"

/***/ }),

/***/ "./src/app/public/business/business.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-business\">\n  <div class=\"header\">\n    <h2>Business registration.</h2>\n  </div>\n  <h2 class=\"title\">Add your business to Spinshare</h2>\n  <div class=\"form\">\n    <div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"firstName\" class=\"form-control\">\n        <label for=\"firstName\">First Name</label>\n      </div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"lastName\" class=\"form-control\">\n        <label for=\"firstName\">Last Name</label>\n      </div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"bussinessName\" class=\"form-control\">\n        <label for=\"bussinessName\">Name of the Business</label>\n      </div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"address1\" class=\"form-control\">\n        <label for=\"address1\">Address (Line 1)</label>\n      </div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"address2\" class=\"form-control\">\n        <label for=\"address2\">Address (Line 2)</label>\n      </div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"postcode\" class=\"form-control\">\n        <label for=\"postcode\">Postcode</label>\n      </div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"region\" class=\"form-control\">\n        <label for=\"region\">Region</label>\n      </div>\n    </div>\n    <div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"country\" class=\"form-control\">\n        <label for=\"country\">Country</label>\n      </div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"landline\" class=\"form-control\">\n        <label for=\"landline\">Phone number (Landline)</label>\n      </div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"mobile\" class=\"form-control\">\n        <label for=\"mobile\">Phone number (Mobile)</label>\n      </div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"email\" class=\"form-control\">\n        <label for=\"email\">Email</label>\n      </div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"openHour\" class=\"form-control\">\n        <label for=\"openHour\">Opening hours</label>\n      </div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"url\" class=\"form-control\">\n        <label for=\"url\">URL</label>\n      </div>\n      <div class=\"md-form\">\n        <input mdbActive type=\"text\" id=\"estSince\" class=\"form-control\">\n        <label for=\"estSince\">Established since</label>\n      </div>\n    </div>\n  </div>\n  <button type=\"text\" class=\"btn btn-secondary btn-lg\" style=\"width: 200px; margin: auto;\">Submit</button>\n</div>"

/***/ }),

/***/ "./src/app/public/business/business.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BusinessPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BusinessPage = /** @class */ (function () {
    function BusinessPage(store, router) {
        this.store = store;
        this.router = router;
    }
    BusinessPage.prototype.ngOnInit = function () {
    };
    BusinessPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/public/business/business.html"),
            styles: [__webpack_require__("./src/app/public/business/business.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], BusinessPage);
    return BusinessPage;
}());



/***/ }),

/***/ "./src/app/public/cinema/cinema-showtime/item.css":
/***/ (function(module, exports) {

module.exports = "agm-map {\n  height: 300px;\n}\n\n.feed {\n  width: 60vw;\n  min-width: 900px;\n  margin: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.feed > .info {\n  height: 30vh;\n  padding: 10px;\n  margin: 5px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.info > img {\n  width: 30%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n\n.info > .content {\n  position: relative;\n  width: 70%;\n  padding: 5px 10px;\n  background-color: white;\n}\n\n.info > .content > .name {\n  font-weight: 700;\n}\n\n.info > .content > p {\n  font-size: 0.9rem;\n  margin-bottom: 0.5rem;\n}\n\n.info > .content > .address {\n  color: #0275d8;\n}\n\n.info > .content > .address:hover {\n  text-decoration: underline;\n}\n\n.info > .content > .status {\n  color: #239839;\n}\n\n.info > .content > .more {\n  position: absolute;\n  bottom: 0px;\n  right: 5px;\n}\n\n.feed > .detail {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 5px 15px;\n}\n\n.detail > .menu {\n  width: 30%;\n}\n\n.detail > .menu > ul {\n  margin: 0;\n  padding: 0;\n}\n\n.detail > .menu > ul > li {\n  list-style: none;\n}\n\n.detail > .menu > ul > li > a {\n  display: block;\n  padding: 8px 10px;\n  border-bottom: #e6e6e6 1px solid;\n  border-left: #eee 1px solid;\n}\n\n.detail > .menu > ul > li > a:hover {\n  color: #e52402;\n  border-left: #c00 1px solid;\n}\n\n.detail > .content {\n  width: 70%;\n  background-color: white;\n}\n\n.detail > .content > .galery {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.detail > .content > .galery > div {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: start;\n}\n\n.detail > .content > .galery > div > div {\n  width: 11vw;\n  height: 11vw;\n  margin: 0.5vw;\n}\n\n.detail > .content > .galery > div > div > img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.detail > .content > .happening {\n  padding: 10px;\n}\n\n.detail > .content > .showtime {\n  padding: 10px;\n}\n\n.detail > .content > .showtime > div {\n  padding-top: 20px;\n}\n\n.detail > .content > .showtime > div > .film-name {\n  font-weight: 500;\n}\n\n.detail > .content > .showtime > div > .film-info {\n  width: 100%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.detail > .content > .showtime > div > .film-info > a > img {\n  width: 150px;\n  min-width: 150px;\n  height: 200px;\n  min-height: 200px;\n  margin-right: 10px;\n  margin-bottom: 5px;\n}\n\n.detail > .content > .showtime > div > .film-info > p {\n  font-size: 0.85rem;\n  padding-left: 10px;\n}\n\n.detail > .content > .showtime > div > .showing-time {\n  clear: both;\n}\n\n.detail > .content > .showtime > div > .showing-time > h6 {\n  font-weight: 500;\n}\n\n.detail > .content > .showtime > div > .showing-time > div > label {\n  border: 1px solid #dddddd;\n  border-radius: 5px;\n  padding: 5px 10px;\n  background-color: #3c3c3c;\n  color: white;\n  font-weight: 900;\n}\n\n.detail > .content > .showtime > p {\n  clear: both;\n  text-align: center;\n  margin-bottom: 0;\n  padding-top: 20px;\n}"

/***/ }),

/***/ "./src/app/public/cinema/cinema-showtime/item.html":
/***/ (function(module, exports) {

module.exports = "<div mdbModal #xpMap=\"mdb-modal\" (onShown)=\"onMapShow()\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\"\n  [config]=\"{backdrop: false, ignoreBackdropClick: true}\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title w-100\">{{title}}</h4>\n      </div>\n      <div class=\"modal-body\">\n        <agm-map #map [zoom]=\"15\" [latitude]=\"cinema ? cinema.ll[0] : 0\" [longitude]=\"cinema ? cinema.ll[1] : 0\">\n          <agm-marker [latitude]=\"cinema ? cinema.ll[0] : 0\" [longitude]=\"cinema ? cinema.ll[1] : 0\"></agm-marker>\n        </agm-map>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary waves-light\" aria-label=\"Close\" (click)=\"xpMap.hide()\" mdbRippleRadius>Close</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"feed\" *ngIf=\"cinema\">\n  <div class=\"info\">\n    <img *ngIf=\"cinema.logo\" class=\"img-fluid\" src=\"{{cinema.logo}}\" alt=\"Card image cap\">\n    <h3 *ngIf=\"!cinema.logo\" style=\"width: 100%; display: flex; font-weight: 800; background-color: white; margin-bottom: 0;\">\n      <label style=\"margin: auto\">{{cinema.name}}</label>\n    </h3>\n    <div class=\"content\">\n      <h3 class=\"name\">{{cinema.name}}</h3>\n      <p class=\"address\" *ngIf=\"cinema.address\">\n        <i class=\"fa fa-location-arrow pr-2\"></i>\n        <a (click)=\"xpMap.show()\">{{cinema.address + ' ' + cinema.county}}</a>\n      </p>\n      <p class=\"status\" *ngIf=\"cinema.time_status\">\n        <i class=\"fa fa-circle pr-2\" style=\"font-size: 10px;\"></i>\n        <a>{{cinema.time_status}}</a>\n      </p>\n      <p class=\"phone\" *ngIf=\"cinema.phone\">\n        <i class=\"fa fa-phone-square blue-text pr-2\"></i>{{cinema.phone}}\n      </p>\n      <p class=\"website\" *ngIf=\"cinema.website\">\n        <i class=\"fab fa-chrome blue-text pr-2\"></i>\n        <a href=\"{{cinema.website}}\" target=\"_blank\">{{cinema.website}}</a>\n      </p>\n      <p class=\"twitter\" *ngIf=\"cinema.twitter\">\n        <i class=\"fab fa-twitter-square blue-text pr-2\"></i>\n        <a href=\"{{cinema.twitter}}\" target=\"_blank\">{{cinema.twitter}}</a>\n      </p>\n      <p class=\"facebook\" *ngIf=\"cinema.facebook\">\n        <i class=\"fab fa-facebook-square blue-text pr-2\"></i>\n        <a href=\"{{cinema.facebook}}\" target=\"_blank\">{{cinema.facebook}}</a>\n      </p>\n      <p class=\"more\" *ngIf=\"user\">\n        <span>\n          <i class=\"fas fa-thumbs-up\" [ngStyle]=\"cinema.isLiked ? {'color': '#1e9bff', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n            (click)=\"like()\"></i> {{cinema.like_count}}\n        </span>\n        <span style=\"margin: auto 5px;\">\n          <i class=\"fas fa-thumbs-down\" [ngStyle]=\"cinema.isDisliked ? {'color': 'red', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n            (click)=\"dislike()\"></i> {{cinema.dislike_count}}\n        </span>\n        <span style=\"margin: auto 5px;\">\n          <i class=\"fas fa-users\" [ngStyle]=\"cinema.isFollowed ? {'color': '#00941b', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n            (click)=\"follow()\"></i> {{cinema.following_count}}\n        </span>\n      </p>\n    </div>\n  </div>\n  <div class=\"detail\">\n    <div class=\"menu\">\n      <ul>\n        <li (click)=\"goto('showtime')\">\n          <a [ngStyle]=\"(ui | async)?.cinemaMenu == 'showtime' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n            Show Times\n            <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n          </a>\n        </li>\n        <!-- <li (click)=\"goto('galery')\">\n          <a [ngStyle]=\"(ui | async)?.cinemaMenu == 'galery' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n            Galery\n            <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n          </a>\n        </li>\n        <li (click)=\"goto('happening')\">\n          <a [ngStyle]=\"(ui | async)?.cinemaMenu == 'happening' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n            Happening Now\n            <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n          </a>\n        </li> -->\n      </ul>\n    </div>\n    <div class=\"content\">\n      <section class=\"showtime\" *ngIf=\"(ui | async)?.cinemaMenu == 'showtime'\">\n        <ul class=\"pagination pg-blue mb-0\" style=\"float: right;\">\n          <li class=\"page-item\" [ngClass]=\"{'active' : d.active}\" *ngFor=\"let d of days\" style=\"margin: auto 2px;\" (click)=\"fltFilms(d.id)\">\n            <a class=\"page-link\" mdbWavesEffect>{{d.id == 0 ? 'Today' : d.value.split(' ')[0]}}</a>\n          </li>\n        </ul>\n        <div *ngFor=\"let film of films\" style=\"clear: both;\">\n          <!-- <img src={{film.poster}} width=\"50%\"/> -->\n          <h4 class=\"film-name\">{{film.name}}\n            <label style=\"font-size: 0.9rem\">({{film.runningTime}})</label>\n          </h4>\n          <div class=\"film-info\">\n            <a href=\"{{film.trailer}}\" target=\"_blank\">\n              <img src=\"{{film.poster}}\" />\n            </a>\n            <div>\n              <p style=\"margin-bottom: 15px;\">{{film.synopsis}}</p>\n              <p style=\"margin-bottom: 5px; font-size: 0.8rem;\">\n                <label style=\"font-weight: 600; margin-bottom: 0;\">Director: </label>\n                <span style=\"color: #656565;\">{{film.director.join('; ')}}</span>\n              </p>\n              <p style=\"margin-bottom: 5px; font-size: 0.8rem;\">\n                <label style=\"font-weight: 600; margin-bottom: 0;\">Starrings: </label>\n                <span style=\"color: #656565;\">{{film.starring.join('; ')}}</span>\n              </p>\n              <p style=\"margin-bottom: 5px; font-size: 0.8rem;\">\n                <label style=\"font-weight: 600; margin-bottom: 0;\">Rating: </label>\n                <span style=\"color: #656565;\">{{film.rating}}/10</span>\n              </p>\n            </div>\n          </div>\n          <div *ngFor=\"let showtime of film.showTimes\" class=\"showing-time\">\n            <h6>{{showtime.date}}</h6>\n            <div>\n              <label *ngFor=\"let time of showtime.times\">{{time.replace('pm', '').replace('am', '')}}</label>\n            </div>\n          </div>\n        </div>\n        <p *ngIf=\"!films || !films.length\">No films on this day</p>\n      </section>\n      <!-- <section class=\"galery\" *ngIf=\"(ui | async)?.cinemaMenu == 'galery'\">\n        <div *ngFor=\"let row of photos\">\n          <div *ngFor=\"let col of row\">\n            <img src=\"{{col}}\" />\n          </div>\n        </div>\n      </section>\n      <section class=\"happening\" *ngIf=\"(ui | async)?.cinemaMenu == 'happening'\">\n        <h6>All shared feeds</h6>\n        <div class=\"media\" *ngFor=\"let happening of cinema.happenings\" style=\"margin-bottom: 0.8rem;\">\n          <img class=\"d-flex mr-3\" style=\"border-radius: 50%;\" src=\"{{happening.user_picture ? happening.user_picture : 'https://i.pinimg.com/originals/5a/59/1c/5a591c4e208e1747894b41ec7f830beb.png'}}\"\n            width=\"40\" height=\"40\" alt=\"Generic placeholder image\">\n          <div class=\"media-body\">\n            <p *ngIf=\"happening.content_type == 'photo'\" style=\"margin-bottom: 0;\">\n              <b style=\"color: #006dc3;\">{{happening.user_name}}</b>\n              <span style=\"color: #565656; font-size: 0.9rem;\">{{happening.caption}}</span>\n            </p>\n            <img src=\"{{happening.content}}\" style=\"width: 50%;\" *ngIf=\"happening.content_type == 'photo'\" />\n            <p *ngIf=\"happening.content_type == 'text'\" style=\"margin-bottom: 0;\">\n              <b style=\"color: #006dc3;\">{{happening.user_name}}</b>\n              <span style=\"color: #565656; font-size: 0.9rem;\">{{happening.content || happening.caption}}</span>\n            </p>\n            <p style=\"margin-bottom: 0; font-size: 0.7rem; color: #8e8e8e;\">\n              {{happening.date_time * 1000 | date}}\n            </p>\n          </div>\n        </div>\n        <create-share-feed [xp]=\"feed\" *ngIf=\"user\"></create-share-feed>\n        <div *ngIf=\"!user\" style=\"text-align: center\">\n          <a routerLink=\"/login\">Login to share your comment</a>\n        </div>\n      </section> -->\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/public/cinema/cinema-showtime/item.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CinemaShowTimePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__agm_core__ = __webpack_require__("./node_modules/@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CinemaShowTimePage = /** @class */ (function () {
    function CinemaShowTimePage(apiService, uiService, store, activatedRoute) {
        var _this = this;
        this.apiService = apiService;
        this.uiService = uiService;
        this.store = store;
        this.activatedRoute = activatedRoute;
        this.films = [];
        this.days = [];
        this.ui = store.select('ui');
        this.activatedRoute.params.subscribe(function (params) {
            _this.apiService.getDetailCinema(params['id']);
            for (var i = 0; i < 7; i++) {
                var next = Date.now() + i * 24 * 60 * 60 * 1000;
                var day = new Date(next).getDay();
                var date = new Date(next).toDateString();
                _this.days.push({
                    id: i,
                    active: i == 0 ? true : false,
                    value: date
                });
            }
            _this.days.push({
                id: 7,
                active: false,
                value: 'All Films'
            });
        });
        this.store.select('detailCinema').subscribe(function (cinema) {
            _this.cinema = cinema;
            if (cinema && cinema.films.length) {
                var activeDate = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](_this.days, function (d) {
                    return d.active;
                });
                _this.films = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](cinema.films, function (f) {
                    return f.date == activeDate.value;
                }) ? __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](cinema.films, function (f) {
                    return f.date == activeDate.value;
                }).films : [];
            }
        });
        this.store.select('user').subscribe(function (u) { return _this.user = u; });
    }
    CinemaShowTimePage.prototype.ngOnInit = function () {
    };
    CinemaShowTimePage.prototype.goto = function (mode) {
        this.uiService.changeCinemaMenu(mode);
    };
    CinemaShowTimePage.prototype.like = function () {
        // this.apiService.likeXpItem(this.feed.id, 'place', this.feed.scope);
    };
    CinemaShowTimePage.prototype.dislike = function () {
        // this.apiService.dislikeXpItem(this.feed.id, 'place', this.feed.scope);
    };
    CinemaShowTimePage.prototype.follow = function () {
        // this.apiService.followXpItem(this.feed.id, 'place', this.feed.scope);
    };
    CinemaShowTimePage.prototype.onMapShow = function () {
        this.map.triggerResize();
    };
    CinemaShowTimePage.prototype.fltFilms = function (sltItem) {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_0_underscore__["each"](this.days, function (d) {
            d.active = false;
        });
        var activeDate = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](this.days, function (d) {
            return d.id == sltItem;
        });
        activeDate.active = true;
        if (!this.cinema)
            return;
        this.films = [];
        if (sltItem == 7) {
            var tmp = [];
            __WEBPACK_IMPORTED_MODULE_0_underscore__["each"](JSON.parse(JSON.stringify(this.cinema.films)), function (f) {
                tmp = tmp.concat(f.films);
            });
            __WEBPACK_IMPORTED_MODULE_0_underscore__["each"](tmp, function (item) {
                var f = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](_this.films, function (i) { return i.name == item.name; });
                if (!f) {
                    _this.films.push(item);
                }
                else {
                    f.showTimes = f.showTimes.concat(item.showTimes);
                }
            });
        }
        else {
            this.films = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](this.cinema.films, function (f) {
                return f.date == activeDate.value;
            }) ? __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](this.cinema.films, function (f) {
                return f.date == activeDate.value;
            }).films : [];
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["_11" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__agm_core__["b" /* AgmMap */])
    ], CinemaShowTimePage.prototype, "map", void 0);
    CinemaShowTimePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/public/cinema/cinema-showtime/item.html"),
            styles: [__webpack_require__("./src/app/public/cinema/cinema-showtime/item.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_6_app_services_ui__["a" /* UiService */], __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */]])
    ], CinemaShowTimePage);
    return CinemaShowTimePage;
}());



/***/ }),

/***/ "./src/app/public/cinema/cinemacard/item.css":
/***/ (function(module, exports) {

module.exports = ".grid {\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  margin: 6px;\n}\n\n.card {\n  max-height: 100%;\n}\n\n.card img {\n  -o-object-fit: contain;\n     object-fit: contain;\n  width: 100%;\n  height: 20vh;\n}\n\n.card .card-body {\n  width: 100%;\n  min-height: 15vh;\n}\n\n.card .card-body h5 {\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  overflow: hidden;\n}\n\n.card .card-body p {\n  overflow: hidden;\n  position: relative; \n  line-height: 1.2em;\n  max-height: 2.4em; \n  text-align: justify;  \n  margin-right: -1em;\n  padding-right: 1em;\n}\n\n.card .card-body p:before {\n  content: '...';\n  position: absolute;\n  right: 0;\n  bottom: 0;\n}\n\n.card .card-body p:after {\n  content: '';\n  position: absolute;\n  right: 0;\n  width: 1em;\n  height: 1em;\n  margin-top: 0.2em;\n  background: white;\n}\n"

/***/ }),

/***/ "./src/app/public/cinema/cinemacard/item.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card grid\">\n  <div class=\"view hm-white-slight waves-light\" mdbRippleRadius (click)=\"showDetail(data._id)\">\n    <img *ngIf=\"data.logo\" style=\"padding: 0px 5px;\" class=\"img-fluid\" src=\"{{data.logo}}\" alt=\"Card image cap\">\n    <h4 *ngIf=\"!data.logo\" style=\"width: 100%; height: 20vh; display: flex; font-weight: 800; text-align: center; margin-bottom: 0;\">\n      <label style=\"margin: auto\">{{data.name}}</label>\n    </h4>\n    <a>\n      <div class=\"mask\"></div>\n    </a>\n  </div>\n  <div class=\"card-body\">\n    <h5 class=\"card-title\">\n      <label style=\"font-weight: 800;\">{{data.name}}</label>\n      <!-- <span>({{data.films.length}} films)</span> -->\n    </h5>\n    <p class=\"card-text\">\n      <i class=\"fas fa-location-arrow\"></i>\n      <span style=\"justify-content: left\">{{data.address + ' ' + data.county}}</span>\n    </p>\n  </div>\n  <p class=\"card-text\" style=\"padding: 10px;\">\n    <a style=\"margin: auto 5px;\">\n      <i class=\"fas fa-thumbs-up\" [ngStyle]=\"data.isLiked ? {'color': '#1e9bff', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n        (click)=\"like(data.placeID, 'place', data.scope)\"></i> {{data.like_count}}</a>\n    <a style=\"margin: auto 5px;\">\n      <i class=\"fas fa-thumbs-down\" [ngStyle]=\"data.isDisliked ? {'color': 'red', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n        (click)=\"dislike(data.placeID, 'place', data.scope)\"></i> {{data.dislike_count}}</a>\n    <a style=\"margin: auto 5px;\">\n      <i class=\"fas fa-users\" [ngStyle]=\"data.isFollowed ? {'color': '#00941b', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n        (click)=\"follow(data.placeID, 'place', data.scope)\"></i> {{data.following_count}}</a>\n  </p>\n</div>"

/***/ }),

/***/ "./src/app/public/cinema/cinemacard/item.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CinemaCardItem; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CinemaCardItem = /** @class */ (function () {
    function CinemaCardItem(store, router, apiService, uiService) {
        this.store = store;
        this.router = router;
        this.apiService = apiService;
        this.uiService = uiService;
    }
    CinemaCardItem.prototype.ngOnInit = function () {
    };
    CinemaCardItem.prototype.ngOnDestroy = function () {
    };
    CinemaCardItem.prototype.like = function (id, type, scope) {
        // this.apiService.likeXpItem(id, type, scope);
    };
    CinemaCardItem.prototype.dislike = function (id, type, scope) {
        // this.apiService.dislikeXpItem(id, type, scope);
    };
    CinemaCardItem.prototype.follow = function (id, type, scope) {
        // this.apiService.followXpItem(id, type, scope);
    };
    CinemaCardItem.prototype.showDetail = function (id) {
        this.router.navigate(['/cinema/' + id + '/showtimes']);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], CinemaCardItem.prototype, "data", void 0);
    CinemaCardItem = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'cinema-card',
            template: __webpack_require__("./src/app/public/cinema/cinemacard/item.html"),
            styles: [__webpack_require__("./src/app/public/cinema/cinemacard/item.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_4_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_3_app_services_ui__["a" /* UiService */]])
    ], CinemaCardItem);
    return CinemaCardItem;
}());



/***/ }),

/***/ "./src/app/public/cinema/item.css":
/***/ (function(module, exports) {

module.exports = ".cinema {\n  width: 80vw;\n  min-width: 1200px;\n  margin: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.cinema > div {\n  /* margin: 20px auto auto auto; */\n  background-color: white;\n  border: 1px solid #d7d7d7;\n  padding: 10px;\n}\n\n.cinema > .results {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.cinema > div > div {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.cinema > div > nav {\n  margin: 10px auto;\n}\n\n.cinema > div > nav > ul {\n  float: right;\n}\n\n.cinema > .filter {\n  width: 100%;\n  margin: 10px auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  background-color: white;\n  border-radius: 3px;\n  padding: 15px;\n}\n\n.cinema > .filter > div {\n  -webkit-box-flex: 1;\n      -ms-flex: 1;\n          flex: 1;\n  margin-bottom: 0;\n  margin: auto 5px;\n}\n\n.cinema > .filter > button {\n  height: 40px;\n  width: 150px;\n  padding-top: 10px;\n  border-radius: 30px;\n}"

/***/ }),

/***/ "./src/app/public/cinema/item.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"cinema\">\n  <div class=\"filter\">\n    <div class=\"md-form\" [ngStyle]=\"{'width':category != 145 ? '45%':'100%'}\">\n      <input mdbActive type=\"text\" id=\"location\" [(ngModel)]=\"location\" class=\"form-control\">\n      <label for=\"location\">Address</label>\n    </div>\n    <button class=\"btn btn-primary waves-light\" mdbRippleRadius (click)=\"search()\">\n      Search\n    </button>\n  </div>\n  <div *ngIf=\"cinemaList.length\" class=\"results\">\n    <nav>\n      <ul class=\"pagination\">\n        <li class=\"page-item\">\n          <a class=\"page-link\" mdbRippleRadius aria-label=\"Previous\" (click)=\"prevPage()\">\n            <span aria-hidden=\"true\">&laquo;</span>\n            <span class=\"sr-only\">Previous</span>\n          </a>\n        </li>\n        <li [ngClass]=\"cinemaPage == i ? 'page-item active' : 'page-item'\" *ngFor=\"let i of pages\" (click)=\"sltPage(i)\">\n          <a class=\"page-link\" mdbRippleRadius>{{i}}</a>\n        </li>\n        <li class=\"page-item\" (click)=\"nextPage()\">\n          <a class=\"page-link\" mdbRippleRadius aria-label=\"Next\">\n            <span aria-hidden=\"true\">&raquo;</span>\n            <span class=\"sr-only\">Next</span>\n          </a>\n        </li>\n      </ul>\n    </nav>\n    <div *ngFor=\"let cinemaRow of cinemaList\" [ngStyle]=\"cinemaRow.length < 5 && {'justify-content' :'start'}\">\n      <cinema-card [data]=\"cinema\" *ngFor=\"let cinema of cinemaRow\" style=\"width: 20%;\"></cinema-card>\n    </div>\n    <nav>\n      <ul class=\"pagination\">\n        <li class=\"page-item\">\n          <a class=\"page-link\" mdbRippleRadius aria-label=\"Previous\" (click)=\"prevPage()\">\n            <span aria-hidden=\"true\">&laquo;</span>\n            <span class=\"sr-only\">Previous</span>\n          </a>\n        </li>\n        <li [ngClass]=\"cinemaPage == i ? 'page-item active' : 'page-item'\" *ngFor=\"let i of pages\" (click)=\"sltPage(i)\">\n          <a class=\"page-link\" mdbRippleRadius>{{i}}</a>\n        </li>\n        <li class=\"page-item\" (click)=\"nextPage()\">\n          <a class=\"page-link\" mdbRippleRadius aria-label=\"Next\">\n            <span aria-hidden=\"true\">&raquo;</span>\n            <span class=\"sr-only\">Next</span>\n          </a>\n        </li>\n      </ul>\n    </nav>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/public/cinema/item.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CinemaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CinemaPage = /** @class */ (function () {
    function CinemaPage(apiService, uiService, router, store, activatedRoute) {
        var _this = this;
        this.apiService = apiService;
        this.uiService = uiService;
        this.router = router;
        this.store = store;
        this.activatedRoute = activatedRoute;
        this.cinemaList = [];
        this.pages = [];
        this.cinemaPage = 1;
        this.cinemaTotalPage = 1;
        this.cinemaItems = store.select('cinemaItems');
        //Subcribe to show cinemas
        this.cinemaItems.subscribe(function (allCinemas) {
            // console.log(allCinemas)
            _this.cinemaList = [];
            for (var row = 0; row < allCinemas.length / 5; row++) {
                _this.cinemaList[row] = [];
                for (var col = 0; col < 5; col++) {
                    if (allCinemas[row * 5 + col]) {
                        _this.cinemaList[row][col] = allCinemas[row * 5 + col];
                    }
                }
            }
        });
        //Subcribe discover page to load new xps
        store.select('ui').subscribe(function (uiState) {
            _this.ui = uiState;
            //Fetching in loading page firstly
            if (uiState.cinemaLocation != '' && _this.location != uiState.cinemaLocation) {
                console.log('fetching cinema');
                _this.location = uiState.cinemaLocation;
                _this.apiService.fetchCinema(1, uiState.cinemaLocation);
            }
            //Reset page array
            _this.pages = [];
            //Set current page
            _this.cinemaPage = uiState.cinemaPage;
            _this.cinemaTotalPage = uiState.cinemaTotalPage;
            //Create array of page number
            for (var i = 1; i <= _this.cinemaTotalPage; i++) {
                _this.pages.push(i);
            }
        });
    }
    CinemaPage.prototype.ngOnInit = function () {
    };
    CinemaPage.prototype.search = function () {
        this.store.dispatch({
            type: 'FILTER_CINEMA',
            payload: {
                location: this.location,
            }
        });
        this.apiService.fetchCinema(1, this.location);
    };
    CinemaPage.prototype.sltPage = function (page) {
        this.apiService.fetchCinema(page, this.location);
    };
    CinemaPage.prototype.prevPage = function () {
        if (this.cinemaPage == 1)
            return;
        this.apiService.fetchCinema(this.cinemaPage - 1, this.location);
    };
    CinemaPage.prototype.nextPage = function () {
        if (this.cinemaPage == this.cinemaTotalPage)
            return;
        this.apiService.fetchCinema(this.cinemaPage + 1, this.location);
    };
    CinemaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/public/cinema/item.html"),
            styles: [__webpack_require__("./src/app/public/cinema/item.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_4_app_services_ui__["a" /* UiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]])
    ], CinemaPage);
    return CinemaPage;
}());



/***/ }),

/***/ "./src/app/public/discover/discover.css":
/***/ (function(module, exports) {

module.exports = ".discover {\n  position: relative;\n  margin: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.header {\n  width: 100vw;\n  height: 35vh;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  background-size: cover;\n  /* background-image: url('/assets/graphics/girls_party.jpg'); */\n}\n\n.header > div {\n  margin: auto;\n  width: 50vw;\n}\n\n.header > div > label {\n  font-size: 1.25rem;\n  left: 10px;\n}\n\n.header > div > input {\n  background-color: white;\n  padding: 10px;\n  font-size: 1.25rem;\n}\n\n.discover > .trending,\n.discover > .special {\n  margin: 20px auto auto auto;\n  background-color: white;\n  border: 1px solid #d7d7d7;\n  padding: 10px;\n  width: 80vw;\n  min-width: 1200px;\n}\n\n.trending > div,\n.special > div {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.trending > nav,\n.special > nav {\n  margin: 10px auto;\n}\n\n.trending > nav > ul,\n.special > nav > ul {\n  float: right;\n}"

/***/ }),

/***/ "./src/app/public/discover/discover.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"discover\">\n  <div class=\"header\">\n    <div style=\"display: flex;\">\n      <div class=\"md-form\" style=\"margin: auto 5px; width: 80%;\">\n        <input mdbActive type=\"text\" id=\"search\" class=\"form-control\" [(ngModel)]=\"location\">\n        <label for=\"search\">Search experiences in your nearby area</label>\n      </div>\n      <button class=\"btn btn-primary waves-light\" mdbRippleRadius (click)=\"search()\" style=\"margin: auto 5px; width: 20%; height: 2.35rem; padding: 0; border-radius: 20px;\">\n        Search\n      </button>\n    </div>\n  </div>\n  <!-- <div class=\"trending\" *ngIf=\"trendingList.length\">\n    <h4>Trending Feeds</h4>\n    <div *ngFor=\"let trendingRow of trendingList\" [ngStyle]=\"trendingRow.length < 4 && {'justify-content' :'start'}\">\n      <trending *ngFor=\"let trending of trendingRow\" [trending]=\"trending\" style=\"flex: 1; max-width: 25%;\"></trending>\n    </div>\n    <nav>\n      <ul class=\"pagination\">\n        <li class=\"page-item\">\n          <a class=\"page-link\" mdbRippleRadius aria-label=\"Previous\" (click)=\"prevTrendingPage()\">\n            <span aria-hidden=\"true\">&laquo;</span>\n            <span class=\"sr-only\">Previous</span>\n          </a>\n        </li>\n        <li [ngClass]=\"trendingPage == i ? 'page-item active' : 'page-item'\" *ngFor=\"let i of tpages\" (click)=\"sltTrendingPage(i)\">\n          <a class=\"page-link\" mdbRippleRadius>{{i}}</a>\n        </li>\n        <li class=\"page-item\" (click)=\"nextTrendingPage()\">\n          <a class=\"page-link\" mdbRippleRadius aria-label=\"Next\">\n            <span aria-hidden=\"true\">&raquo;</span>\n            <span class=\"sr-only\">Next</span>\n          </a>\n        </li>\n      </ul>\n    </nav>\n  </div> -->\n  <div class=\"special\" *ngIf=\"specialList.length\">\n    <h4>Deals</h4>\n    <div *ngFor=\"let specialRow of specialList\" [ngStyle]=\"specialRow.length < 4 && {'justify-content' :'start'}\">\n      <special *ngFor=\"let special of specialRow\" [special]=\"special\" style=\"flex: 1; max-width: 25%;\"></special>\n    </div>\n    <nav>\n      <ul class=\"pagination\">\n        <li class=\"page-item\">\n          <a class=\"page-link\" mdbRippleRadius aria-label=\"Previous\" (click)=\"prevSpecialPage()\">\n            <span aria-hidden=\"true\">&laquo;</span>\n            <span class=\"sr-only\">Previous</span>\n          </a>\n        </li>\n        <li [ngClass]=\"specialPage == i ? 'page-item active' : 'page-item'\" *ngFor=\"let i of spages\" (click)=\"sltSpecialPage(i)\">\n          <a class=\"page-link\" mdbRippleRadius>{{i}}</a>\n        </li>\n        <li class=\"page-item\" (click)=\"nextSpecialPage()\">\n          <a class=\"page-link\" mdbRippleRadius aria-label=\"Next\">\n            <span aria-hidden=\"true\">&raquo;</span>\n            <span class=\"sr-only\">Next</span>\n          </a>\n        </li>\n      </ul>\n    </nav>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/public/discover/discover.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DiscoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DiscoverPage = /** @class */ (function () {
    function DiscoverPage(store, router, apiService, uiService) {
        var _this = this;
        this.store = store;
        this.router = router;
        this.apiService = apiService;
        this.uiService = uiService;
        this.location = '';
        this.trendingList = [];
        this.trendingPage = 1;
        this.trendingTotalPage = 1;
        this.tpages = [];
        this.specialList = [];
        this.specialPage = 1;
        this.specialTotalPage = 1;
        this.spages = [];
        console.log('Discover');
        this.trendings = store.select('trendings');
        this.specials = store.select('specials');
        //Subcribe to show trendings
        this.trendings.subscribe(function (allTrendings) {
            _this.trendingList = [];
            for (var row = 0; row < allTrendings.length / 4; row++) {
                _this.trendingList[row] = [];
                for (var col = 0; col < 4; col++) {
                    if (allTrendings[row * 4 + col]) {
                        _this.trendingList[row][col] = allTrendings[row * 4 + col];
                    }
                }
            }
        });
        //Subcribe to show specials
        this.specials.subscribe(function (allSpecials) {
            _this.specialList = [];
            for (var row = 0; row < allSpecials.length / 4; row++) {
                _this.specialList[row] = [];
                for (var col = 0; col < 4; col++) {
                    if (allSpecials[row * 4 + col]) {
                        _this.specialList[row][col] = allSpecials[row * 4 + col];
                    }
                }
            }
        });
        //Subcribe discover page to load new trending
        store.select('ui').subscribe(function (uiState) {
            if (uiState.discoverLocation != '' && _this.location != uiState.discoverLocation) {
                console.log('fetching discover');
                _this.location = uiState.discoverLocation;
                //Fetching the trending
                _this.apiService.fetchTrendings(_this.trendingPage, uiState.discoverLocation);
                //Fetching the special
                _this.apiService.fetchSpecials(_this.specialPage, uiState.discoverLocation);
            }
            // console.log(uiState);
            //Reset page array
            _this.tpages = [];
            _this.spages = [];
            //Set current page
            _this.trendingPage = uiState.trendingPage;
            _this.trendingTotalPage = uiState.trendingTotalPage;
            _this.specialPage = uiState.specialPage;
            _this.specialTotalPage = uiState.specialTotalPage;
            //Create array of page number
            for (var i = 1; i <= _this.trendingTotalPage; i++) {
                _this.tpages.push(i);
            }
            for (var i = 1; i <= _this.specialTotalPage; i++) {
                _this.spages.push(i);
            }
        });
    }
    DiscoverPage.prototype.sltTrendingPage = function (i) {
        this.apiService.fetchTrendings(i, this.location);
    };
    DiscoverPage.prototype.nextTrendingPage = function () {
        if (this.trendingPage + 1 > this.trendingTotalPage)
            return;
        this.apiService.fetchTrendings(this.trendingPage + 1, this.location);
    };
    DiscoverPage.prototype.prevTrendingPage = function () {
        if (this.trendingPage - 1 < 1)
            return;
        this.apiService.fetchTrendings(this.trendingPage - 1, this.location);
    };
    DiscoverPage.prototype.sltSpecialPage = function (i) {
        this.apiService.fetchSpecials(i, this.location);
    };
    DiscoverPage.prototype.nextSpecialPage = function () {
        if (this.specialPage + 1 > this.specialTotalPage)
            return;
        this.apiService.fetchSpecials(this.specialPage + 1, this.location);
    };
    DiscoverPage.prototype.prevSpecialPage = function () {
        if (this.specialPage - 1 < 1)
            return;
        this.apiService.fetchSpecials(this.specialPage - 1, this.location);
    };
    DiscoverPage.prototype.search = function () {
        this.store.dispatch({
            type: 'FILTER_DISCOVER',
            payload: {
                location: this.location
            }
        });
        this.apiService.fetchTrendings(1, this.location);
        this.apiService.fetchSpecials(1, this.location);
    };
    DiscoverPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/public/discover/discover.html"),
            styles: [__webpack_require__("./src/app/public/discover/discover.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_4_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_3_app_services_ui__["a" /* UiService */]])
    ], DiscoverPage);
    return DiscoverPage;
}());



/***/ }),

/***/ "./src/app/public/discover/special/card/item.css":
/***/ (function(module, exports) {

module.exports = ".card {\n  max-height: 100%;\n}\n\n.card img {\n  -o-object-fit: cover;\n     object-fit: cover;\n  width: 100%;\n  height: 15vh;\n}\n\n.card .card-body {\n  min-height: 160px;\n}\n\n.card .card-body h5 {\n  width: 150px;\n  max-width: 150px;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  overflow: hidden;\n}\n\n.card .card-body p {\n  overflow: hidden;\n  position: relative; \n  line-height: 1.2em;\n  max-height: 2.4em; \n  text-align: justify;  \n  margin-right: -1em;\n  padding-right: 1em;\n}\n\n.card .card-body p:before {\n  content: '...';\n  position: absolute;\n  right: 0;\n  bottom: 0;\n}\n\n.card .card-body p:after {\n  content: '';\n  position: absolute;\n  right: 0;\n  width: 1em;\n  height: 1em;\n  margin-top: 0.2em;\n  background: white;\n}\n\n.card > .deal-icon {\n  width: 50px;\n  height: 50px;\n  position: absolute;\n  right: 0;\n}\n"

/***/ }),

/***/ "./src/app/public/discover/special/card/item.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\" (click)=\"showDetail(data.id)\">\n  <div class=\"view hm-white-slight waves-light\" mdbRippleRadius>\n    <img class=\"img-fluid\" src=\"{{data.photo}}\" alt=\"Card image cap\" *ngIf=\"data.photo\">\n    <img class=\"img-fluid\" src=\"assets/graphics/placeholders2.png\" alt=\"Card image cap\" *ngIf=\"!data.photo\">\n    <a>\n      <div class=\"mask\"></div>\n    </a>\n  </div>\n  <div class=\"card-body\">\n    <h5 class=\"card-title\">{{data.url}}</h5>\n    <p class=\"card-text\">{{data.message}}</p>\n  </div>\n  <img class=\"deal-icon\" src=\"assets/graphics/deal.png\"/>\n</div>"

/***/ }),

/***/ "./src/app/public/discover/special/card/item.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpecialCardItem; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SpecialCardItem = /** @class */ (function () {
    function SpecialCardItem(store, router, apiService, uiService) {
        this.store = store;
        this.router = router;
        this.apiService = apiService;
        this.uiService = uiService;
    }
    SpecialCardItem.prototype.ngOnInit = function () {
    };
    SpecialCardItem.prototype.ngOnDestroy = function () {
    };
    SpecialCardItem.prototype.showDetail = function (id) {
        this.router.navigate(['/special/feed/' + id]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], SpecialCardItem.prototype, "data", void 0);
    SpecialCardItem = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'special-card-item',
            template: __webpack_require__("./src/app/public/discover/special/card/item.html"),
            styles: [__webpack_require__("./src/app/public/discover/special/card/item.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_4_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_3_app_services_ui__["a" /* UiService */]])
    ], SpecialCardItem);
    return SpecialCardItem;
}());



/***/ }),

/***/ "./src/app/public/discover/special/detail-special/item.css":
/***/ (function(module, exports) {

module.exports = "agm-map {\n  height: 300px;\n}\n\n.feed {\n  width: 60vw;\n  min-width: 900px;\n  margin: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.feed > .info {\n  height: 30vh;\n  padding: 10px;\n  margin: 5px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  position: relative;\n}\n\n.info > img {\n  min-width: 200px;\n  width: 30%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.info > .content {\n  position: relative;\n  width: 70%;\n  padding: 5px 10px;\n  background-color: white;\n}\n\n.info > .content > .name {\n  font-weight: 700;\n}\n\n.info > .content > p {\n  font-size: 0.9rem;\n  margin-bottom: 0.5rem;\n}\n\n.info > .content > .address {\n  color: #0275d8;\n}\n\n.info > .content > .address:hover {\n  text-decoration: underline;\n}\n\n.info > .content > .postcode {\n  color: #239839;\n}\n\n.info > .content > .more {\n  position: absolute;\n  bottom: 0px;\n  right: 5px;\n}\n\n.info > .content > .more > a {\n  color: #0275d8;\n}\n\n.info > .content > .more > a:hover {\n  text-decoration: underline;\n}\n\n.feed > .detail {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 5px 15px;\n}\n\n.detail > .menu {\n  width: 30%;\n  min-width: 200px;\n}\n\n.detail > .menu > ul {\n  margin: 0;\n  padding: 0;\n}\n\n.detail > .menu > ul > li {\n  list-style: none;\n}\n\n.detail > .menu > ul > li > a {\n  display: block;\n  padding: 8px 10px;\n  border-bottom: #e6e6e6 1px solid;\n  border-left: #eee 1px solid;\n}\n\n.detail > .menu > ul > li > a:hover {\n  color: #e52402;\n  border-left: #c00 1px solid;\n}\n\n.detail > .content {\n  width: 70%;\n  background-color: white;\n}\n\n.detail > .content > .galery {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.detail > .content > .galery > div {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: start;\n}\n\n.detail > .content > .galery > div > div {\n  width: 25%;\n  height: 10vw;\n}\n\n.detail > .content > .galery > div > div > img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  padding: 5px;\n}\n\n.detail > .content > .happening {\n  padding: 10px;\n}"

/***/ }),

/***/ "./src/app/public/discover/special/detail-special/item.html":
/***/ (function(module, exports) {

module.exports = "<div mdbModal #xpMap=\"mdb-modal\" (onShown)=\"onMapShow()\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\"\n  [config]=\"{backdrop: false, ignoreBackdropClick: true}\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title w-100\">{{title}}</h4>\n      </div>\n      <div class=\"modal-body\">\n        <agm-map #map [zoom]=\"15\" [latitude]=\"deal ? deal.place.lat : 0\" [longitude]=\"deal ? deal.place.long : 0\">\n          <agm-marker [latitude]=\"deal ? deal.place.lat : 0\" [longitude]=\"deal ? deal.place.long : 0\"></agm-marker>\n        </agm-map>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary waves-light\" aria-label=\"Close\" (click)=\"xpMap.hide()\" mdbRippleRadius>Close</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"feed\" *ngIf=\"deal\">\n  <div class=\"info\">\n    <img src=\"{{deal.photo}}\" class=\"img-fluid\" alt=\"Responsive image\" *ngIf=\"deal\">\n    <img class=\"img-fluid\" src=\"assets/graphics/placeholders2.png\" alt=\"Card image cap\" *ngIf=\"!deal\">\n    <div class=\"content\">\n      <h3 class=\"name\">{{deal.place.name}}</h3>\n      <p class=\"address\" *ngIf=\"deal.place.location\">\n        <i class=\"fa fa-location-arrow pr-2\"></i>\n        <a (click)=\"xpMap.show()\">{{deal.place.location}}</a>\n      </p>\n      <p class=\"postcode\" *ngIf=\"deal.place.postcode\">\n        <i class=\"fa fa-compass pr-2\"></i>\n        <a>{{deal.place.postcode}}</a>\n      </p>\n      <p class=\"phone\" *ngIf=\"deal.place.contact\">\n        <i class=\"fa fa-phone-square blue-text pr-2\"></i>{{deal.place.contact}}\n      </p>\n      <p class=\"website\" *ngIf=\"deal.url\">\n        <i class=\"fab fa-chrome blue-text pr-2\"></i>\n        <a href=\"{{deal.url}}\" target=\"_blank\">{{deal.url}}</a>\n      </p>\n      <p class=\"message\" *ngIf=\"deal.message\">\n        {{deal.message}}\n      </p>\n      <!-- <p class=\"twitter\" *ngIf=\"feed.twitter\">\n        <i class=\"fab fa-twitter-square blue-text pr-2\"></i>\n        <a href=\"{{feed.twitter}}\" target=\"_blank\">{{feed.twitter}}</a>\n      </p>\n      <p class=\"facebook\" *ngIf=\"feed.facebook\">\n        <i class=\"fab fa-facebook-square blue-text pr-2\"></i>\n        <a href=\"{{feed.facebook}}\" target=\"_blank\">{{feed.facebook}}</a>\n      </p> -->\n      <p class=\"more\">\n        <!-- <span>\n          <i class=\"fas fa-thumbs-up\" [ngStyle]=\"feed.isLiked ? {'color': '#1e9bff', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n            (click)=\"like()\"></i> {{deal.like_count}}\n        </span>\n        <span style=\"margin: auto 5px;\">\n          <i class=\"fas fa-thumbs-down\" [ngStyle]=\"feed.isDisliked ? {'color': 'red', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n            (click)=\"dislike()\"></i> {{deal.dislike_count}}\n        </span>\n        <span style=\"margin: auto 5px;\">\n          <i class=\"fas fa-users\" [ngStyle]=\"feed.isFollowed ? {'color': '#00941b', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n            (click)=\"follow()\"></i> {{deal.following_count}}\n        </span> -->\n        <a (click)=\"gotoPlace(deal.place.placeID)\">Go to place</a>\n      </p>\n    </div>\n    <img style=\"min-width: 50px; width: 50px; height: 50px; position: absolute; left: 10px; margin-right: 15px; margin-top: 5px;\" src=\"assets/graphics/deal.png\"/>\n  </div>\n  <!-- <div class=\"detail\">\n    <div class=\"menu\">\n      <ul>\n        <li (click)=\"goto('galery')\">\n          <a [ngStyle]=\"(ui | async)?.feedMenu == 'galery' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n            Galery\n            <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n          </a>\n        </li>\n        <li (click)=\"goto('happening')\">\n          <a [ngStyle]=\"(ui | async)?.feedMenu == 'happening' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n            Happening Now\n            <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n          </a>\n        </li>\n      </ul>\n    </div>\n    <div class=\"content\">\n      <section class=\"galery\" *ngIf=\"(ui | async)?.feedMenu == 'galery'\">\n        <div *ngFor=\"let row of photos\">\n          <div *ngFor=\"let col of row\">\n            <img src=\"{{col}}\" />\n          </div>\n        </div>\n      </section>\n      <section class=\"happening\" *ngIf=\"(ui | async)?.feedMenu == 'happening'\">\n        <h6>All shared feeds</h6>\n        <div class=\"media\" *ngFor=\"let happening of feed.happenings\" style=\"margin-bottom: 0.8rem;\">\n          <img class=\"d-flex mr-3\" style=\"border-radius: 50%;\" src=\"{{happening.user_picture ? happening.user_picture : 'https://i.pinimg.com/originals/5a/59/1c/5a591c4e208e1747894b41ec7f830beb.png'}}\"\n            width=\"40\" height=\"40\" alt=\"Generic placeholder image\">\n          <div class=\"media-body\">\n            <p *ngIf=\"happening.content_type == 'photo'\" style=\"margin-bottom: 0;\">\n              <b style=\"color: #006dc3;\">{{happening.user_name}}</b>\n              <span style=\"color: #565656; font-size: 0.9rem;\">{{happening.caption}}</span>\n            </p>\n            <img src=\"{{happening.content}}\" style=\"width: 50%;\" *ngIf=\"happening.content_type == 'photo'\" />\n            <p *ngIf=\"happening.content_type == 'text'\" style=\"margin-bottom: 0;\">\n              <b style=\"color: #006dc3;\">{{happening.user_name}}</b>\n              <span style=\"color: #565656; font-size: 0.9rem;\">{{happening.content || happening.caption}}</span>\n            </p>\n            <p style=\"margin-bottom: 0; font-size: 0.7rem; color: #8e8e8e;\">\n              {{happening.date_time * 1000 | date}}\n            </p>\n          </div>\n        </div>\n        <create-share-feed [xp]=\"feed\"></create-share-feed>\n      </section>\n    </div>\n  </div> -->\n</div>"

/***/ }),

/***/ "./src/app/public/discover/special/detail-special/item.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailSpecialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__agm_core__ = __webpack_require__("./node_modules/@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DetailSpecialPage = /** @class */ (function () {
    function DetailSpecialPage(apiService, router, uiService, store, activatedRoute) {
        var _this = this;
        this.apiService = apiService;
        this.router = router;
        this.uiService = uiService;
        this.store = store;
        this.activatedRoute = activatedRoute;
        this.photos = [];
        this.ui = store.select('ui');
        this.specials = store.select('specials');
        this.activatedRoute.params.subscribe(function (params) {
            _this.specials.subscribe(function (allItems) {
                _this.deal = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](allItems, function (item) { return item.id == params['id']; });
            });
        });
    }
    DetailSpecialPage.prototype.ngOnInit = function () {
    };
    DetailSpecialPage.prototype.goto = function (mode) {
        this.uiService.changeFeedMenu(mode);
    };
    DetailSpecialPage.prototype.like = function () {
        // this.apiService.likeXpItem(this.feed.id, 'place', this.feed.scope);
    };
    DetailSpecialPage.prototype.dislike = function () {
        // this.apiService.dislikeXpItem(this.feed.id, 'place', this.feed.scope);
    };
    DetailSpecialPage.prototype.follow = function () {
        // this.apiService.followXpItem(this.feed.id, 'place', this.feed.scope);
    };
    DetailSpecialPage.prototype.onMapShow = function () {
        this.map.triggerResize();
    };
    DetailSpecialPage.prototype.gotoPlace = function (id) {
        this.router.navigate(['/experience/feed/' + id]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["_11" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__agm_core__["b" /* AgmMap */])
    ], DetailSpecialPage.prototype, "map", void 0);
    DetailSpecialPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/public/discover/special/detail-special/item.html"),
            styles: [__webpack_require__("./src/app/public/discover/special/detail-special/item.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_6_app_services_ui__["a" /* UiService */], __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */]])
    ], DetailSpecialPage);
    return DetailSpecialPage;
}());



/***/ }),

/***/ "./src/app/public/discover/special/special.css":
/***/ (function(module, exports) {

module.exports = ".grid {\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  margin: 8px;\n}\n\n::-webkit-scrollbar {\n  width: 10px;\n  height: 8px;\n}\n\n::-webkit-scrollbar-track {\n  -webkit-box-shadow: inset 0 0 5px #adadad;\n          box-shadow: inset 0 0 5px #adadad; \n  border-radius: 20px;\n  background-clip: content-box;\n}\n\n::-webkit-scrollbar-thumb {\n  background: #b8b8b8; \n  border-radius: 20px;\n}"

/***/ }),

/***/ "./src/app/public/discover/special/special.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"grid\">\n  <special-card-item [data]=\"special\"></special-card-item>\n</div>"

/***/ }),

/***/ "./src/app/public/discover/special/special.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpecialItem; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SpecialItem = /** @class */ (function () {
    function SpecialItem(store, router, apiService, uiService) {
        this.store = store;
        this.router = router;
        this.apiService = apiService;
        this.uiService = uiService;
        this.ui = store.select('ui');
    }
    SpecialItem.prototype.ngOnInit = function () {
    };
    SpecialItem.prototype.showMore = function (id, scope) {
        // this.uiService.showSpecialMore(id);
        // this.apiService.fetchSpecialDetail(id, scope);
    };
    SpecialItem.prototype.ngOnDestroy = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], SpecialItem.prototype, "special", void 0);
    SpecialItem = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'special',
            template: __webpack_require__("./src/app/public/discover/special/special.html"),
            styles: [__webpack_require__("./src/app/public/discover/special/special.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_3_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_4_app_services_ui__["a" /* UiService */]])
    ], SpecialItem);
    return SpecialItem;
}());



/***/ }),

/***/ "./src/app/public/discover/trending/card/item.css":
/***/ (function(module, exports) {

module.exports = ".card {\n  max-height: 100%;\n}\n\n.card img {\n  -o-object-fit: cover;\n     object-fit: cover;\n  width: 100%;\n  height: 15vh;\n}\n\n.card .card-body {\n  min-height: 160px;\n}\n\n.card .card-body h5 {\n  width: 150px;\n  max-width: 150px;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  overflow: hidden;\n}\n\n.card .card-body p {\n  overflow: hidden;\n  position: relative; \n  line-height: 1.2em;\n  max-height: 2.4em; \n  text-align: justify;  \n  margin-right: -1em;\n  padding-right: 1em;\n}\n\n.card .card-body p:before {\n  content: '...';\n  position: absolute;\n  right: 0;\n  bottom: 0;\n}\n\n.card .card-body p:after {\n  content: '';\n  position: absolute;\n  right: 0;\n  width: 1em;\n  height: 1em;\n  margin-top: 0.2em;\n  background: white;\n}\n"

/***/ }),

/***/ "./src/app/public/discover/trending/card/item.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\" (click)=\"showDetail(data.placeID)\">\n  <div class=\"view hm-white-slight waves-light\" mdbRippleRadius>\n    <img class=\"img-fluid\" src=\"{{data.picture}}\" alt=\"Card image cap\" *ngIf=\"data.picture\">\n    <img class=\"img-fluid\" src=\"assets/graphics/placeholders2.png\" alt=\"Card image cap\" *ngIf=\"!data.picture\">\n    <a>\n      <div class=\"mask\"></div>\n    </a>\n  </div>\n  <div class=\"card-body\">\n    <h5 class=\"card-title\">{{data.name}}</h5>\n    <p class=\"card-text\">{{data.location}}</p>\n    <p class=\"card-text\">\n      <a style=\"margin: auto 5px;\">\n        <i class=\"fas fa-thumbs-up\" [ngStyle]=\"data.isLiked ? {'color': '#1e9bff', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"></i> {{data.like_count}}</a>\n      <a style=\"margin: auto 5px;\">\n        <i class=\"fas fa-thumbs-down\" [ngStyle]=\"data.isLiked ? {'color': 'red', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"></i> {{data.dislike_count}}</a>\n      <a style=\"margin: auto 5px;\">\n        <i class=\"fas fa-users\" [ngStyle]=\"data.isLiked ? {'color': '#00941b', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"></i> {{data.following_count}}</a>\n    </p>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/public/discover/trending/card/item.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrendingCardItem; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TrendingCardItem = /** @class */ (function () {
    function TrendingCardItem(store, router, apiService, uiService) {
        this.store = store;
        this.router = router;
        this.apiService = apiService;
        this.uiService = uiService;
    }
    TrendingCardItem.prototype.ngOnInit = function () {
    };
    TrendingCardItem.prototype.ngOnDestroy = function () {
    };
    TrendingCardItem.prototype.showDetail = function (id) {
        this.router.navigate(['/trending/feed/' + id]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], TrendingCardItem.prototype, "data", void 0);
    TrendingCardItem = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'trending-card-item',
            template: __webpack_require__("./src/app/public/discover/trending/card/item.html"),
            styles: [__webpack_require__("./src/app/public/discover/trending/card/item.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_4_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_3_app_services_ui__["a" /* UiService */]])
    ], TrendingCardItem);
    return TrendingCardItem;
}());



/***/ }),

/***/ "./src/app/public/discover/trending/detail-feed/item.css":
/***/ (function(module, exports) {

module.exports = "agm-map {\n  height: 300px;\n}\n\n.feed {\n  width: 60vw;\n  min-width: 900px;\n  margin: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.feed > .info {\n  height: 30vh;\n  padding: 10px;\n  margin: 5px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.info > img {\n  min-width: 200px;\n  width: 30%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.info > .content {\n  position: relative;\n  width: 70%;\n  padding: 5px 10px;\n  background-color: white;\n}\n\n.info > .content > .name {\n  font-weight: 700;\n}\n\n.info > .content > p {\n  font-size: 0.9rem;\n  margin-bottom: 0.5rem;\n}\n\n.info > .content > .address {\n  color: #0275d8;\n}\n\n.info > .content > .address:hover {\n  text-decoration: underline;\n}\n\n.info > .content > .status {\n  color: #239839;\n}\n\n.info > .content > .more {\n  position: absolute;\n  bottom: 0px;\n  right: 5px;\n}\n\n.feed > .detail {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 5px 15px;\n}\n\n.detail > .menu {\n  width: 30%;\n  min-width: 200px;\n}\n\n.detail > .menu > ul {\n  margin: 0;\n  padding: 0;\n}\n\n.detail > .menu > ul > li {\n  list-style: none;\n}\n\n.detail > .menu > ul > li > a {\n  display: block;\n  padding: 8px 10px;\n  border-bottom: #e6e6e6 1px solid;\n  border-left: #eee 1px solid;\n}\n\n.detail > .menu > ul > li > a:hover {\n  color: #e52402;\n  border-left: #c00 1px solid;\n}\n\n.detail > .content {\n  width: 70%;\n  background-color: white;\n}\n\n.detail > .content > .galery {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.detail > .content > .galery > div {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: start;\n}\n\n.detail > .content > .galery > div > div {\n  width: 25%;\n  height: 10vw;\n}\n\n.detail > .content > .galery > div > div > img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  padding: 5px;\n}\n\n.detail > .content > .happening {\n  padding: 10px;\n}"

/***/ }),

/***/ "./src/app/public/discover/trending/detail-feed/item.html":
/***/ (function(module, exports) {

module.exports = "<div mdbModal #xpMap=\"mdb-modal\" (onShown)=\"onMapShow()\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\"\n  [config]=\"{backdrop: false, ignoreBackdropClick: true}\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title w-100\">{{title}}</h4>\n      </div>\n      <div class=\"modal-body\">\n        <agm-map #map [zoom]=\"15\" [latitude]=\"feed ? feed.latitude : 0\" [longitude]=\"feed ? feed.longitude : 0\">\n          <agm-marker [latitude]=\"feed ? feed.latitude : 0\" [longitude]=\"feed ? feed.longitude : 0\"></agm-marker>\n        </agm-map>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary waves-light\" aria-label=\"Close\" (click)=\"xpMap.hide()\" mdbRippleRadius>Close</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"feed\" *ngIf=\"feed\">\n  <div class=\"info\">\n    <img src=\"{{feed.picture}}\" class=\"img-fluid\" alt=\"Responsive image\" *ngIf=\"feed.picture\">\n    <img class=\"img-fluid\" src=\"assets/graphics/placeholders2.png\" alt=\"Card image cap\" *ngIf=\"!feed.picture\">\n    <div class=\"content\">\n      <h3 class=\"name\">{{feed.name}}</h3>\n      <p class=\"address\" *ngIf=\"feed.address\">\n        <i class=\"fa fa-location-arrow pr-2\"></i>\n        <a (click)=\"xpMap.show()\">{{feed.address}}</a>\n      </p>\n      <p class=\"status\" *ngIf=\"feed.time_status\">\n        <i class=\"fa fa-circle pr-2\" style=\"font-size: 10px;\"></i>\n        <a>{{feed.time_status}}</a>\n      </p>\n      <p class=\"phone\" *ngIf=\"feed.phone\">\n        <i class=\"fa fa-phone-square blue-text pr-2\"></i>{{feed.phone}}\n      </p>\n      <p class=\"website\" *ngIf=\"feed.website\">\n        <i class=\"fab fa-chrome blue-text pr-2\"></i>\n        <a href=\"{{feed.website}}\" target=\"_blank\">{{feed.website}}</a>\n      </p>\n      <p class=\"twitter\" *ngIf=\"feed.twitter\">\n        <i class=\"fab fa-twitter-square blue-text pr-2\"></i>\n        <a href=\"{{feed.twitter}}\" target=\"_blank\">{{feed.twitter}}</a>\n      </p>\n      <p class=\"facebook\" *ngIf=\"feed.facebook\">\n        <i class=\"fab fa-facebook-square blue-text pr-2\"></i>\n        <a href=\"{{feed.facebook}}\" target=\"_blank\">{{feed.facebook}}</a>\n      </p>\n      <p class=\"more\" *ngIf=\"user\">\n        <span>\n          <i class=\"fas fa-thumbs-up\" [ngStyle]=\"feed.isLiked ? {'color': '#1e9bff', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n            (click)=\"like()\"></i> {{feed.like_count}}\n        </span>\n        <span style=\"margin: auto 5px;\">\n          <i class=\"fas fa-thumbs-down\" [ngStyle]=\"feed.isDisliked ? {'color': 'red', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n            (click)=\"dislike()\"></i> {{feed.dislike_count}}\n        </span>\n        <span style=\"margin: auto 5px;\">\n          <i class=\"fas fa-users\" [ngStyle]=\"feed.isFollowed ? {'color': '#00941b', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n            (click)=\"follow()\"></i> {{feed.following_count}}\n        </span>\n      </p>\n    </div>\n  </div>\n  <div class=\"detail\">\n    <div class=\"menu\">\n      <ul>\n        <li (click)=\"goto('galery')\">\n          <a [ngStyle]=\"(ui | async)?.feedMenu == 'galery' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n            Gallery\n            <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n          </a>\n        </li>\n        <li (click)=\"goto('happening')\">\n          <a [ngStyle]=\"(ui | async)?.feedMenu == 'happening' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n            Happening Now\n            <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n          </a>\n        </li>\n      </ul>\n    </div>\n    <div class=\"content\">\n      <section class=\"galery\" *ngIf=\"(ui | async)?.feedMenu == 'galery'\">\n        <div *ngFor=\"let row of photos\">\n          <div *ngFor=\"let col of row\">\n            <img src=\"{{col}}\" />\n          </div>\n        </div>\n      </section>\n      <section class=\"happening\" *ngIf=\"(ui | async)?.feedMenu == 'happening'\">\n        <h6>All shared feeds</h6>\n        <div class=\"media\" *ngFor=\"let happening of feed.happenings\" style=\"margin-bottom: 0.8rem;\">\n          <img class=\"d-flex mr-3\" style=\"border-radius: 50%;\" src=\"{{happening.user_picture ? happening.user_picture : 'https://i.pinimg.com/originals/5a/59/1c/5a591c4e208e1747894b41ec7f830beb.png'}}\"\n            width=\"40\" height=\"40\" alt=\"Generic placeholder image\">\n          <div class=\"media-body\">\n            <p *ngIf=\"happening.content_type == 'photo'\" style=\"margin-bottom: 0;\">\n              <b style=\"color: #006dc3;\">{{happening.user_name}}</b>\n              <span style=\"color: #565656; font-size: 0.9rem;\">{{happening.caption}}</span>\n            </p>\n            <img src=\"{{happening.content}}\" style=\"width: 50%;\" *ngIf=\"happening.content_type == 'photo'\" />\n            <p *ngIf=\"happening.content_type == 'text'\" style=\"margin-bottom: 0;\">\n              <b style=\"color: #006dc3;\">{{happening.user_name}}</b>\n              <span style=\"color: #565656; font-size: 0.9rem;\">{{happening.content || happening.caption}}</span>\n            </p>\n            <p style=\"margin-bottom: 0; font-size: 0.7rem; color: #8e8e8e;\">\n              {{happening.date_time * 1000 | date}}\n            </p>\n          </div>\n        </div>\n        <create-share-feed [xp]=\"feed\" *ngIf=\"user\"></create-share-feed>\n        <div *ngIf=\"!user\" style=\"text-align: center\">\n          <a routerLink=\"/login\">Login to share your comment</a>\n        </div>\n      </section>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/public/discover/trending/detail-feed/item.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailTrendingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__agm_core__ = __webpack_require__("./node_modules/@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DetailTrendingPage = /** @class */ (function () {
    function DetailTrendingPage(apiService, uiService, store, activatedRoute) {
        var _this = this;
        this.apiService = apiService;
        this.uiService = uiService;
        this.store = store;
        this.activatedRoute = activatedRoute;
        this.photos = [];
        this.ui = store.select('ui');
        this.feedObs = store.select('feed');
        this.activatedRoute.params.subscribe(function (params) {
            // this.apiService.getDetailFeed(params['id'], 'global');
        });
        this.feedObs.subscribe(function (f) {
            // if (!f) return;
            // if (!this.feed && f || this.feed.id != f.id) {
            //   this.feed = f;
            //   this.photos = _.toArray(_.groupBy(this.feed.photos, function (element, index) {
            //     return Math.floor(index / 4);
            //   }));
            //   if (!this.feed.happenings) {
            //     this.apiService.fetchXpHappening(this.feed.id, 'place', this.feed.scope, 1);
            //   }
            // }
        });
        store.select('user').subscribe(function (u) { return _this.user = u; });
    }
    DetailTrendingPage.prototype.ngOnInit = function () {
    };
    DetailTrendingPage.prototype.goto = function (mode) {
        this.uiService.changeFeedMenu(mode);
    };
    // like() {
    //   this.apiService.likeXpItem(this.feed.id, 'place', this.feed.scope);
    // }
    // dislike() {
    //   this.apiService.dislikeXpItem(this.feed.id, 'place', this.feed.scope);
    // }
    // follow() {
    //   this.apiService.followXpItem(this.feed.id, 'place', this.feed.scope);
    // }
    DetailTrendingPage.prototype.onMapShow = function () {
        this.map.triggerResize();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_11" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__agm_core__["b" /* AgmMap */])
    ], DetailTrendingPage.prototype, "map", void 0);
    DetailTrendingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/public/discover/trending/detail-feed/item.html"),
            styles: [__webpack_require__("./src/app/public/discover/trending/detail-feed/item.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_5_app_services_ui__["a" /* UiService */], __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]])
    ], DetailTrendingPage);
    return DetailTrendingPage;
}());



/***/ }),

/***/ "./src/app/public/discover/trending/map/map.css":
/***/ (function(module, exports) {

module.exports = "agm-map {\n  height: 300px;\n}"

/***/ }),

/***/ "./src/app/public/discover/trending/map/map.html":
/***/ (function(module, exports) {

module.exports = "<div mdbModal #xpMap=\"mdb-modal\" (onShown)=\"onMapShow()\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\" [config]=\"{backdrop: false, ignoreBackdropClick: true}\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title w-100\">{{title}}</h4>\n      </div>\n      <div class=\"modal-body\">\n        <agm-map #map [zoom]=\"15\" [latitude]=\"lat\" [longitude]=\"long\">\n          <agm-marker [latitude]=\"lat\" [longitude]=\"long\"></agm-marker>\n        </agm-map>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary waves-light\" aria-label=\"Close\" (click)=\"xpMap.hide()\" mdbRippleRadius>Close</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<p style=\"color: #0e8ff5; cursor: pointer;\" (click)=\"xpMap.show()\">\n  <i class=\"fas fa-map\"></i>\n  <a>Show map</a>\n</p>"

/***/ }),

/***/ "./src/app/public/discover/trending/map/map.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrendingDetailMap; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__agm_core__ = __webpack_require__("./node_modules/@agm/core/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TrendingDetailMap = /** @class */ (function () {
    function TrendingDetailMap() {
    }
    TrendingDetailMap.prototype.ngOnInit = function () {
    };
    TrendingDetailMap.prototype.onMapShow = function () {
        this.map.triggerResize();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Number)
    ], TrendingDetailMap.prototype, "lat", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Number)
    ], TrendingDetailMap.prototype, "long", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], TrendingDetailMap.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__agm_core__["b" /* AgmMap */])
    ], TrendingDetailMap.prototype, "map", void 0);
    TrendingDetailMap = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'trending-detail-map',
            template: __webpack_require__("./src/app/public/discover/trending/map/map.html"),
            styles: [__webpack_require__("./src/app/public/discover/trending/map/map.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TrendingDetailMap);
    return TrendingDetailMap;
}());



/***/ }),

/***/ "./src/app/public/discover/trending/trending.css":
/***/ (function(module, exports) {

module.exports = ".grid {\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  margin: 8px;\n}\n\n::-webkit-scrollbar {\n  width: 10px;\n  height: 8px;\n}\n\n::-webkit-scrollbar-track {\n  -webkit-box-shadow: inset 0 0 5px #adadad;\n          box-shadow: inset 0 0 5px #adadad; \n  border-radius: 20px;\n  background-clip: content-box;\n}\n\n::-webkit-scrollbar-thumb {\n  background: #b8b8b8; \n  border-radius: 20px;\n}"

/***/ }),

/***/ "./src/app/public/discover/trending/trending.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"grid\">\n  <trending-card-item [data]=\"trending\"></trending-card-item>\n</div>"

/***/ }),

/***/ "./src/app/public/discover/trending/trending.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrendingItem; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TrendingItem = /** @class */ (function () {
    function TrendingItem(store, router, apiService, uiService) {
        this.store = store;
        this.router = router;
        this.apiService = apiService;
        this.uiService = uiService;
        this.ui = store.select('ui');
    }
    TrendingItem.prototype.ngOnInit = function () {
    };
    TrendingItem.prototype.showMore = function (id, scope) {
        this.uiService.showTrendingMore(id);
        this.apiService.fetchTrendingDetail(id, scope);
    };
    TrendingItem.prototype.ngOnDestroy = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], TrendingItem.prototype, "trending", void 0);
    TrendingItem = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'trending',
            template: __webpack_require__("./src/app/public/discover/trending/trending.html"),
            styles: [__webpack_require__("./src/app/public/discover/trending/trending.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_3_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_4_app_services_ui__["a" /* UiService */]])
    ], TrendingItem);
    return TrendingItem;
}());



/***/ }),

/***/ "./src/app/public/experience/category/category.css":
/***/ (function(module, exports) {

module.exports = ".category-item {\n  margin: auto 10px;\n  width: 100%;\n  border: solid 1px #dddddd;\n  background-color: white;\n  padding: 5px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  position: relative;\n  background: -webkit-gradient(linear, left top, left bottom, from(white), to(#c5c5c5));\n  background: linear-gradient(white, #c5c5c5);\n  cursor: pointer;\n}\n\n.category-item > .photo {\n  height: 200px;\n}\n\n.category-item > p {\n  text-align: center;\n  font-weight: bold;\n}"

/***/ }),

/***/ "./src/app/public/experience/category/category.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"category-item\" (click)='showDetail(category.id)' *ngIf='category'>\n  <div class=\"photo\" [ngStyle]=\"{'background': 'url(assets/categories/' + category.id + '.jpg) center center no-repeat', 'background-size': 'cover'}\"></div>\n  <p>{{category.name}}</p>\n</div>"

/***/ }),

/***/ "./src/app/public/experience/category/category.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryItem; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CategoryItem = /** @class */ (function () {
    function CategoryItem(store, router, apiService) {
        this.store = store;
        this.router = router;
        this.apiService = apiService;
        this.routes = {
            '101': 'art-entertainment',
            '102': 'music',
            '103': 'film',
            '104': 'sport-fitness',
            '110': 'community-politic',
            '111': 'creativy',
            '117': 'lifestyle',
            '121': 'science-technology',
            '122': 'college-study',
            '123': 'spiritualy-religion',
            '130': 'business',
            '131': 'fashion-beauty',
            '132': 'food-drink',
            '133': 'nightlife',
            '134': 'street-events',
            '135': 'fitness-health-wellbeing',
            '136': 'outdoors',
            '137': 'travel',
            '138': 'shopping',
            '139': 'kids',
            '140': 'auto-boat-air',
            '141': 'animals',
            '142': 'others',
            '143': 'museums',
            '144': 'theatres',
            '145': 'cinemas'
        };
    }
    CategoryItem.prototype.ngOnInit = function () {
    };
    CategoryItem.prototype.ngOnDestroy = function () {
    };
    CategoryItem.prototype.showDetail = function (id) {
        switch (id) {
            case '145':
                this.router.navigate(['/cinema']);
                break;
            default:
                this.router.navigate(['/experience/category/' + this.routes[id.toString()]]);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], CategoryItem.prototype, "category", void 0);
    CategoryItem = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'category-item',
            template: __webpack_require__("./src/app/public/experience/category/category.html"),
            styles: [__webpack_require__("./src/app/public/experience/category/category.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_3_app_services_api__["a" /* ApiService */]])
    ], CategoryItem);
    return CategoryItem;
}());



/***/ }),

/***/ "./src/app/public/experience/detail-feed/create-review/index.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/public/experience/detail-feed/create-review/index.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"media\">\n  <img class=\"d-flex mr-3\" src=\"{{user.profile_pic ? user.profile_pic : 'https://i.pinimg.com/originals/5a/59/1c/5a591c4e208e1747894b41ec7f830beb.png'}}\" width=\"40\" height=\"40\" style=\"border-radius: 50%;\" alt=\"Unknown\">\n  <div class=\"media-body\">\n    <h6 class=\"mt-0\" style=\"font-weight: 500; color: #006dc3;\">{{user ? user.username : ''}}</h6>\n    <div class=\"md-form input-group\" style=\"padding-left: 0px;\">\n      <input type=\"text\" class=\"form-control\" placeholder=\"Your comment ...\" style=\"height: 1rem; font-size: 0.9rem;\" [(ngModel)]=\"content\" (change)=\"onCommentChange()\">\n      <div class=\"input-group-btn\" style=\"display: flex; cursor: pointer;\" (click)=\"createReview()\" [ngStyle]=\"{'pointer-events' : !review.content && !review.caption ? 'none' : '', 'opacity' : !review.content && !review.caption ? '0.3' : '1'}\">\n        <i class=\"fas fa-paper-plane\" style=\"margin: auto 5px; font-size: 1.3rem;\"></i>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/public/experience/detail-feed/create-review/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShareFeedReview; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ShareFeedReview = /** @class */ (function () {
    function ShareFeedReview(apiService, uiService, store, element) {
        var _this = this;
        this.apiService = apiService;
        this.uiService = uiService;
        this.store = store;
        this.element = element;
        this.review = {
            content: '',
            rating: 0
        };
        store.select('user').subscribe(function (usr) {
            _this.user = usr;
        });
    }
    ShareFeedReview.prototype.ngOnInit = function () {
    };
    ShareFeedReview.prototype.onCommentChange = function () {
        this.review.content = this.content;
    };
    ShareFeedReview.prototype.createReview = function () {
        console.log(this.review);
        if (!this.review.content) {
            console.log('Lack information for feed');
            return;
        }
        this.apiService.createXPReview(this.review, this.user, this.xp._id);
        this.review = {
            content: '',
            rating: 0
        };
        this.content = null;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], ShareFeedReview.prototype, "xp", void 0);
    ShareFeedReview = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'create-review',
            template: __webpack_require__("./src/app/public/experience/detail-feed/create-review/index.html"),
            styles: [__webpack_require__("./src/app/public/experience/detail-feed/create-review/index.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_3_app_services_ui__["a" /* UiService */], __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], ShareFeedReview);
    return ShareFeedReview;
}());



/***/ }),

/***/ "./src/app/public/experience/detail-feed/create-share-feed/index.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/public/experience/detail-feed/create-share-feed/index.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"media\">\n  <img class=\"d-flex mr-3\" src=\"{{user.profile_pic ? user.profile_pic : 'https://i.pinimg.com/originals/5a/59/1c/5a591c4e208e1747894b41ec7f830beb.png'}}\" width=\"40\" height=\"40\" style=\"border-radius: 50%;\" alt=\"Unknown\">\n  <div class=\"media-body\">\n    <h6 class=\"mt-0\" style=\"font-weight: 500; color: #006dc3;\">{{user ? user.username : ''}}</h6>\n    <div *ngIf=\"media\" style=\"position: relative;\">\n      <img src=\"{{media}}\" style=\"width:60%;\" />\n      <i class=\"fas fa-times-circle\" style=\"position: absolute; right: 41%; top: 5px; font-size: 1.3rem; cursor: pointer;\" (click)=\"onRemoveMedia()\"></i>\n    </div>\n    <div class=\"md-form input-group\" style=\"padding-left: 0px;\">\n      <div class=\"input-group-btn\" style=\"display: flex;\">\n        <label for=\"imageSlt\" class=\"fas fa-images\" style=\"cursor: pointer; margin: auto 5px; font-size: 1.3rem; position: relative; top: 0; color: black;\"></label>\n      </div>\n      <input type=\"text\" class=\"form-control\" placeholder=\"Your comment ...\" style=\"height: 1rem; font-size: 0.9rem;\" [(ngModel)]=\"caption\" (change)=\"onCaptionChange()\">\n      <div class=\"input-group-btn\" style=\"display: flex; cursor: pointer;\" (click)=\"createFeed()\" [ngStyle]=\"{'pointer-events' : !sharefeed.content && !sharefeed.caption ? 'none' : '', 'opacity' : !sharefeed.content && !sharefeed.caption ? '0.3' : '1'}\">\n        <i class=\"fas fa-paper-plane\" style=\"margin: auto 5px; font-size: 1.3rem;\"></i>\n      </div>\n    </div>\n  </div>\n  <input id=\"imageSlt\" style=\"opacity: 0; position: absolute; z-index: -1;\" type=\"file\" accept=\".jpg,.png\" (change)=\"onMediaSlt($event)\"\n  />\n</div>"

/***/ }),

/***/ "./src/app/public/experience/detail-feed/create-share-feed/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewShareFeed; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NewShareFeed = /** @class */ (function () {
    function NewShareFeed(apiService, uiService, store, element) {
        var _this = this;
        this.apiService = apiService;
        this.uiService = uiService;
        this.store = store;
        this.element = element;
        this.media = null;
        this.sharefeed = {
            content: null,
            type: 'text',
            thumbnail: null,
            caption: null
        };
        store.select('user').subscribe(function (usr) {
            _this.user = usr;
        });
    }
    NewShareFeed.prototype.ngOnInit = function () {
    };
    NewShareFeed.prototype.onMediaSlt = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var src = e.target.result;
                _this.media = src;
            };
            reader.readAsDataURL(event.target.files[0]);
            this.sharefeed.content = event.target.files[0];
            this.sharefeed.type = 'photo';
        }
    };
    NewShareFeed.prototype.onRemoveMedia = function () {
        this.media = null;
        this.sharefeed.content = this.caption;
        this.sharefeed.type = 'text';
    };
    NewShareFeed.prototype.onCaptionChange = function () {
        this.sharefeed.caption = this.caption;
    };
    NewShareFeed.prototype.createFeed = function () {
        if (!this.media) {
            this.sharefeed.content = this.caption;
        }
        if (!this.sharefeed.content) {
            console.log('Lack information for feed');
            return;
        }
        this.apiService.createXPSharefeed(this.sharefeed, this.user, this.xp._id);
        this.sharefeed = {
            content: null,
            type: 'text',
            thumbnail: null,
            caption: null
        };
        this.media = null;
        this.caption = null;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NewShareFeed.prototype, "xp", void 0);
    NewShareFeed = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'create-share-feed',
            template: __webpack_require__("./src/app/public/experience/detail-feed/create-share-feed/index.html"),
            styles: [__webpack_require__("./src/app/public/experience/detail-feed/create-share-feed/index.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_3_app_services_ui__["a" /* UiService */], __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], NewShareFeed);
    return NewShareFeed;
}());



/***/ }),

/***/ "./src/app/public/experience/detail-feed/item.css":
/***/ (function(module, exports) {

module.exports = "agm-map {\n  height: 300px;\n}\n\n.feed {\n  width: 60vw;\n  min-width: 900px;\n  margin: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.feed > .info {\n  height: 30vh;\n  padding: 10px;\n  margin: 5px;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.info > img {\n  min-width: 200px;\n  width: 30%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.info > .content {\n  position: relative;\n  width: 70%;\n  padding: 5px 10px;\n  background-color: white;\n}\n\n.info > .content > .name {\n  font-weight: 700;\n}\n\n.info > .content > p {\n  color: black;\n  font-size: 0.9rem;\n  margin-bottom: 0.5rem;\n}\n\n.info > .content > p a {\n  color: black;\n}\n\n.info > .content > .address {\n  color: black;\n}\n\n.info > .content > .address:hover {\n  text-decoration: underline;\n}\n\n.info > .content > .status {\n  color: black;\n}\n\n.info > .content > .more {\n  position: absolute;\n  bottom: 0px;\n  right: 5px;\n}\n\n.feed > .detail {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  padding: 5px 15px;\n}\n\n.detail > .menu {\n  width: 30%;\n  min-width: 200px;\n}\n\n.detail > .menu > ul {\n  margin: 0;\n  padding: 0;\n}\n\n.detail > .menu > ul > li {\n  list-style: none;\n}\n\n.detail > .menu > ul > li > a {\n  display: block;\n  padding: 8px 10px;\n  border-bottom: #e6e6e6 1px solid;\n  border-left: #eee 1px solid;\n}\n\n.detail > .menu > ul > li > a:hover {\n  color: #e52402;\n  border-left: #c00 1px solid;\n}\n\n.detail > .content {\n  width: 70%;\n  background-color: white;\n}\n\n.detail > .content > .galery {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-flow: wrap;\n      flex-flow: wrap;\n}\n\n.detail > .content > .galery > div {\n  width: 25%;\n  padding: 10px;\n}\n\n.detail > .content > .galery > div > img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.detail > .content > .happening {\n  padding: 10px;\n}"

/***/ }),

/***/ "./src/app/public/experience/detail-feed/item.html":
/***/ (function(module, exports) {

module.exports = "<div mdbModal #xpMap=\"mdb-modal\" (onShown)=\"onMapShow()\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\"\n  [config]=\"{backdrop: false, ignoreBackdropClick: true}\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title w-100\">{{title}}</h4>\n      </div>\n      <div class=\"modal-body\">\n        <agm-map #map [zoom]=\"15\" [latitude]=\"feed ? feed.latitude : 0\" [longitude]=\"feed ? feed.longitude : 0\">\n          <agm-marker [latitude]=\"feed ? feed.latitude : 0\" [longitude]=\"feed ? feed.longitude : 0\"></agm-marker>\n        </agm-map>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary waves-light\" aria-label=\"Close\" (click)=\"xpMap.hide()\" mdbRippleRadius>Close</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"feed\" *ngIf=\"feed\">\n  <div class=\"info\">\n    <img src=\"{{feed.image}}\" class=\"img-fluid\" alt=\"Responsive image\" *ngIf=\"feed.image\">\n    <img class=\"img-fluid\" src=\"assets/graphics/placeholders2.png\" alt=\"Card image cap\" *ngIf=\"!feed.image\">\n    <div class=\"content\">\n      <h3 class=\"name\">{{feed.name}}</h3>\n      <p class=\"address\" *ngIf=\"feed.address\">\n        <i class=\"fa fa-location-arrow pr-2\"></i>\n        <a (click)=\"xpMap.show()\">{{feed.address}}</a>\n      </p>\n      <p class=\"status\" *ngIf=\"feed.time_status\">\n        <i class=\"fa fa-circle pr-2\" style=\"font-size: 10px;\"></i>\n        <a>{{feed.time_status}}</a>\n      </p>\n      <p class=\"phone\" *ngIf=\"feed.phone\">\n        <i class=\"fa fa-phone-square pr-2\"></i>{{feed.phone}}\n      </p>\n      <p class=\"website\" *ngIf=\"feed.website\">\n        <i class=\"fab fa-chrome pr-2\"></i>\n        <a href=\"{{feed.website}}\" target=\"_blank\">{{feed.website}}</a>\n      </p>\n      <p class=\"twitter\" *ngIf=\"feed.twitter\">\n        <i class=\"fab fa-twitter-square pr-2\"></i>\n        <a href=\"{{feed.twitter}}\" target=\"_blank\">{{feed.twitter}}</a>\n      </p>\n      <p class=\"facebook\" *ngIf=\"feed.facebook\">\n        <i class=\"fab fa-facebook-square pr-2\"></i>\n        <a href=\"{{feed.facebook}}\" target=\"_blank\">{{feed.facebook}}</a>\n      </p>\n      <p class=\"more\" *ngIf=\"user\">\n        <!-- <span>\n          <i class=\"fas fa-thumbs-up\" [ngStyle]=\"feed.isLiked ? {'color': '#1e9bff', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n            (click)=\"like()\"></i> {{feed.like_count}}\n        </span>\n        <span style=\"margin: auto 5px;\">\n          <i class=\"fas fa-thumbs-down\" [ngStyle]=\"feed.isDisliked ? {'color': 'red', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n            (click)=\"dislike()\"></i> {{feed.dislike_count}}\n        </span>\n        <span style=\"margin: auto 5px;\">\n          <i class=\"fas fa-users\" [ngStyle]=\"feed.isFollowed ? {'color': '#00941b', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n            (click)=\"follow()\"></i> {{feed.following_count}}\n        </span> -->\n        <span>\n          <i class=\"fas fa-heart\" [ngStyle]=\"feed.isLiked ? {'color': '#1e9bff', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n            (click)=\"favourite()\"></i> {{feed.likedBy ? feed.likedBy.length : 0}}\n        </span>\n      </p>\n    </div>\n  </div>\n  <div class=\"detail\">\n    <div class=\"menu\">\n      <ul>\n        <li (click)=\"goto('galery')\">\n          <a [ngStyle]=\"(ui | async)?.feedMenu == 'galery' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n            Media\n            <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n          </a>\n        </li>\n        <li (click)=\"goto('happening')\">\n          <a [ngStyle]=\"(ui | async)?.feedMenu == 'happening' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n            Happening Now\n            <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n          </a>\n        </li>\n        <li (click)=\"goto('review')\">\n          <a [ngStyle]=\"(ui | async)?.feedMenu == 'review' ? {'color': '#e52402', 'border-left': '#c00 1px solid'} : {}\">\n            Review\n            <span class=\"fa fa-angle-right\" style=\"float: right; font-size: 14px; margin: 5px;\"></span>\n          </a>\n        </li>\n      </ul>\n    </div>\n    <div class=\"content\">\n      <section class=\"galery\" *ngIf=\"(ui | async)?.feedMenu == 'galery'\">\n        <div *ngFor=\"let media of feed.media\">\n          <img style=\"border: 1px solid #d5d5d5;\" src=\"{{media.content}}\"/>\n        </div>\n      </section>\n      <section class=\"happening\" *ngIf=\"(ui | async)?.feedMenu == 'happening'\">\n        <div class=\"media\" *ngFor=\"let sharefeed of feed.sharefeeds\" style=\"margin-bottom: 0.8rem;\">\n          <img class=\"d-flex mr-3\" style=\"border-radius: 50%;\" src=\"{{(sharefeed.postedBy.user_avatar && sharefeed.postedBy.user_avatar !== 'undefined') ? sharefeed.postedBy.user_avatar : 'https://i.pinimg.com/originals/5a/59/1c/5a591c4e208e1747894b41ec7f830beb.png'}}\"\n            width=\"40\" height=\"40\" alt=\"\">\n          <div class=\"media-body\">\n            <div *ngIf=\"sharefeed.type == 'photo'\" style=\"margin-bottom: 0;\">\n              <b style=\"color: #006dc3;\">{{sharefeed.postedBy.user_name ? sharefeed.postedBy.user_name : 'Unknown'}}</b>\n              <div style=\"color: #565656; font-size: 0.9rem;\">{{sharefeed.caption && sharefeed.caption !== 'null' ? sharefeed.caption : ''}}</div>\n            </div>\n            <img src=\"{{sharefeed.content}}\" style=\"width: 50%; border: solid 1px #d5d5d5;\" *ngIf=\"sharefeed.type == 'photo'\" />\n            <div *ngIf=\"sharefeed.type == 'text'\" style=\"margin-bottom: 0;\">\n              <b style=\"color: #006dc3;\">{{sharefeed.postedBy.user_name ? sharefeed.postedBy.user_name : 'Unknown'}}</b>\n              <div style=\"color: #565656; font-size: 0.9rem;\">{{sharefeed.content || sharefeed.caption}}</div>\n            </div>\n            <div style=\"margin-bottom: 0; font-size: 0.7rem; color: #8e8e8e;\">\n              {{sharefeed.dateTime | date}}\n            </div>\n          </div>\n        </div>\n        <create-share-feed [xp]=\"feed\" *ngIf=\"user\"></create-share-feed>\n        <div *ngIf=\"!user\" style=\"text-align: center\">\n          <a routerLink=\"/login\">Login to share your comment</a>\n        </div>\n      </section>\n      <section class=\"happening\" *ngIf=\"(ui | async)?.feedMenu == 'review'\">\n        <div class=\"media\" *ngFor=\"let review of feed.reviews\" style=\"margin-bottom: 0.8rem;\">\n          <img class=\"d-flex mr-3\" style=\"border-radius: 50%;\" src=\"{{(review.postedBy.user_avatar && review.postedBy.user_avatar !== 'undefined') ? review.postedBy.user_avatar : 'https://i.pinimg.com/originals/5a/59/1c/5a591c4e208e1747894b41ec7f830beb.png'}}\"\n            width=\"40\" height=\"40\" alt=\"\">\n          <div class=\"media-body\">\n            <div style=\"margin-bottom: 0;\">\n              <b style=\"color: #006dc3;\">{{review.postedBy.user_name ? review.postedBy.user_name : 'Unknown'}}</b>\n              <div style=\"color: #565656; font-size: 0.9rem;\">{{review.content}}</div>\n            </div>\n            <div style=\"margin-bottom: 0; font-size: 0.7rem; color: #8e8e8e;\">\n              {{review.dateTime | date}}\n            </div>\n          </div>\n        </div>\n        <create-review [xp]=\"feed\" *ngIf=\"user\"></create-review>\n        <div *ngIf=\"!user\" style=\"text-align: center\">\n          <a routerLink=\"/login\">Login to share your comment</a>\n        </div>\n      </section>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/public/experience/detail-feed/item.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailXPPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__agm_core__ = __webpack_require__("./node_modules/@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DetailXPPage = /** @class */ (function () {
    function DetailXPPage(apiService, uiService, store, activatedRoute) {
        var _this = this;
        this.apiService = apiService;
        this.uiService = uiService;
        this.store = store;
        this.activatedRoute = activatedRoute;
        this.photos = [];
        this.ui = store.select('ui');
        this.feedObs = store.select('feed');
        this.activatedRoute.params.subscribe(function (params) {
            _this.apiService.getDetailFeed(params['id']);
        });
        this.feedObs.subscribe(function (f) {
            if (!f)
                return;
            if (!_this.feed && f || _this.feed._id != f._id || _this.feed.likedBy.length != f.likedBy.length || _this.feed.sharefeeds.length != f.sharefeeds.length || _this.feed.reviews.length != f.reviews.length) {
                _this.feed = f;
            }
        });
        store.select('user').subscribe(function (u) { return _this.user = u; });
    }
    DetailXPPage.prototype.ngOnInit = function () {
    };
    DetailXPPage.prototype.goto = function (mode) {
        this.uiService.changeFeedMenu(mode);
    };
    DetailXPPage.prototype.favourite = function () {
        this.apiService.favouriteXPFeed(this.feed._id);
    };
    DetailXPPage.prototype.onMapShow = function () {
        this.map.triggerResize();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_11" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__agm_core__["b" /* AgmMap */])
    ], DetailXPPage.prototype, "map", void 0);
    DetailXPPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/public/experience/detail-feed/item.html"),
            styles: [__webpack_require__("./src/app/public/experience/detail-feed/item.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_5_app_services_ui__["a" /* UiService */], __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]])
    ], DetailXPPage);
    return DetailXPPage;
}());



/***/ }),

/***/ "./src/app/public/experience/experience.css":
/***/ (function(module, exports) {

module.exports = ".app-xp {\n  position: relative;\n  width: 100%;\n  margin: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.app-xp > .category-list {\n  width: 80vw;\n  min-width: 1200px;\n  margin: 5px auto;\n  /* padding-bottom: 40px;  */\n  border-bottom: solid 1px #fefefe;\n}\n\n.category-list {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.category-list > div {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  margin-bottom: 10px;\n}"

/***/ }),

/***/ "./src/app/public/experience/experience.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-xp\">\n  <div class=\"category-list\">\n    <div *ngFor=\"let categoryRow of categoryList\">\n      <category-item *ngFor=\"let category of categoryRow\" [category]=\"category\" style=\"width: 25%;\"></category-item>\n   </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/public/experience/experience.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return XPPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var XPPage = /** @class */ (function () {
    function XPPage(apiService, router, store) {
        var _this = this;
        this.apiService = apiService;
        this.router = router;
        this.store = store;
        this.categoryList = [];
        this.ui = store.select('ui');
        this.categories = store.select('categories');
        console.log('Experience');
        //Fetching categories
        this.apiService.fetchCategories();
        //Subcribe to show categories
        this.categories.subscribe(function (allCategories) {
            for (var row = 0; row < allCategories.length / 4; row++) {
                _this.categoryList[row] = [];
                for (var col = 0; col < 4; col++) {
                    if (allCategories[row * 4 + col] && allCategories[row * 4 + col].id !== '145') {
                        _this.categoryList[row][col] = allCategories[row * 4 + col];
                    }
                }
            }
        });
    }
    XPPage.prototype.ngOnInit = function () {
    };
    XPPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/public/experience/experience.html"),
            styles: [__webpack_require__("./src/app/public/experience/experience.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_2__ngrx_store__["a" /* Store */]])
    ], XPPage);
    return XPPage;
}());



/***/ }),

/***/ "./src/app/public/experience/filter/filter.css":
/***/ (function(module, exports) {

module.exports = ".filter {\n  margin: 10px auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  background-color: white;\n  border-radius: 3px;\n  padding: 15px;\n}\n\n.filter > div {\n  width: 45%;\n  margin-bottom: 0;\n  margin: auto 5px;\n}\n\n.filter > button {\n  height: 40px;\n  width: 150px;\n  padding-top: 10px;\n  border-radius: 30px;\n}"

/***/ }),

/***/ "./src/app/public/experience/filter/filter.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"filter\">\n  <div class=\"md-form\">\n    <input mdbActive type=\"text\" id=\"location\" [(ngModel)]=\"location\" class=\"form-control\">\n    <label for=\"location\">Address</label>\n  </div>\n  <div class=\"md-form\" *ngIf=\"category != 145\">\n    <input mdbActive type=\"text\" id=\"keyword\" [(ngModel)]=\"keyword\" class=\"form-control\">\n    <label for=\"keyword\">Keyword</label>\n  </div>\n  <button class=\"btn btn-primary waves-light\" mdbRippleRadius (click)=\"search()\">\n    Search\n  </button>\n</div>"

/***/ }),

/***/ "./src/app/public/experience/filter/filter.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return XpFilter; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var XpFilter = /** @class */ (function () {
    function XpFilter(apiService, store, activatedRoute) {
        var _this = this;
        this.apiService = apiService;
        this.store = store;
        this.activatedRoute = activatedRoute;
        this.location = '';
        this.keyword = '';
        this.routes = {
            '101': 'art-entertainment',
            '102': 'music',
            '103': 'film',
            '104': 'sport-fitness',
            '110': 'community-politic',
            '111': 'creativy',
            '117': 'lifestyle',
            '121': 'science-technology',
            '122': 'college-study',
            '123': 'spiritualy-religion',
            '130': 'business',
            '131': 'fashion-beauty',
            '132': 'food-drink',
            '133': 'nightlife',
            '134': 'street-events',
            '135': 'fitness-health-wellbeing',
            '136': 'outdoors',
            '137': 'travel',
            '138': 'shopping',
            '139': 'kids',
            '140': 'auto-boat-air',
            '141': 'animals',
            '142': 'others',
            '143': 'museums',
            '144': 'theatres',
            '145': 'cinemas'
        };
        //Save category id from url
        this.activatedRoute.params.subscribe(function (params) {
            _this.category = (__WEBPACK_IMPORTED_MODULE_0_underscore__["invert"](_this.routes))[params['id']];
        });
        //Set default city to filter
        store.select('ui').subscribe(function (uiState) {
            _this.ui = uiState;
            if (uiState.xpLocation != '' && _this.location != uiState.xpLocation) {
                _this.location = uiState.xpLocation;
                _this.keyword = uiState.xpKeyword;
                _this.apiService.fetchXP(1, uiState.xpLocation, uiState.xpKeyword, _this.category);
            }
        });
    }
    XpFilter.prototype.search = function () {
        this.store.dispatch({
            type: 'FILTER_XP',
            payload: {
                location: this.location,
                keyword: this.keyword
            }
        });
        this.apiService.fetchXP(1, this.location, this.keyword, this.category);
    };
    XpFilter.prototype.sltType = function () {
    };
    XpFilter.prototype.ngOnInit = function () {
    };
    XpFilter.prototype.ngOnDestroy = function () {
    };
    XpFilter = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'xp-filter',
            template: __webpack_require__("./src/app/public/experience/filter/filter.html"),
            styles: [__webpack_require__("./src/app/public/experience/filter/filter.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]])
    ], XpFilter);
    return XpFilter;
}());



/***/ }),

/***/ "./src/app/public/experience/list-feed/item.css":
/***/ (function(module, exports) {

module.exports = ".experience {\n  width: 80vw;\n  min-width: 1200px;\n  margin: auto;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.experience > div {\n  /* margin: 20px auto auto auto; */\n  background-color: white;\n  border: 1px solid #d7d7d7;\n  padding: 10px;\n}\n\n.experience > div > div {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -ms-flex-pack: distribute;\n      justify-content: space-around;\n}\n\n.experience > div > nav {\n  margin: 10px auto;\n}\n\n.experience > div > nav > ul {\n  float: right;\n}"

/***/ }),

/***/ "./src/app/public/experience/list-feed/item.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"experience\">\n  <xp-filter></xp-filter>\n  <div *ngIf=\"category!='145' && xpList.length\">\n    <div *ngFor=\"let xpRow of xpList\" [ngStyle]=\"xpRow.length < 5 && {'justify-content' :'start'}\">\n      <xp-card [data]=\"xp\" *ngFor=\"let xp of xpRow\" style=\"width: 20%;\"></xp-card>\n    </div>\n    <nav>\n      <ul class=\"pagination\">\n        <li class=\"page-item\">\n          <a class=\"page-link\" mdbRippleRadius aria-label=\"Previous\" (click)=\"prevPage()\">\n            <span aria-hidden=\"true\">&laquo;</span>\n            <span class=\"sr-only\">Previous</span>\n          </a>\n        </li>\n        <li [ngClass]=\"xpPage == i ? 'page-item active' : 'page-item'\" *ngFor=\"let i of pages\" (click)=\"sltPage(i)\">\n          <a class=\"page-link\" mdbRippleRadius>{{i}}</a>\n        </li>\n        <li class=\"page-item\" (click)=\"nextPage()\">\n          <a class=\"page-link\" mdbRippleRadius aria-label=\"Next\">\n            <span aria-hidden=\"true\">&raquo;</span>\n            <span class=\"sr-only\">Next</span>\n          </a>\n        </li>\n      </ul>\n    </nav>\n  </div>\n  <!-- <div *ngIf=\"category=='145' && cinemaList.length\">\n    <div *ngFor=\"let cinemaRow of cinemaList\" [ngStyle]=\"cinemaRow.length < 5 && {'justify-content' :'start'}\">\n      <cinema-card [data]=\"cinema\" *ngFor=\"let cinema of cinemaRow\"></cinema-card>\n    </div>\n    <nav>\n      <ul class=\"pagination\">\n        <li class=\"page-item\">\n          <a class=\"page-link\" mdbRippleRadius aria-label=\"Previous\" (click)=\"prevXPPage()\">\n            <span aria-hidden=\"true\">&laquo;</span>\n            <span class=\"sr-only\">Previous</span>\n          </a>\n        </li>\n        <li [ngClass]=\"xpPage == i ? 'page-item active' : 'page-item'\" *ngFor=\"let i of pages\" (click)=\"sltPage(i)\">\n          <a class=\"page-link\" mdbRippleRadius>{{i}}</a>\n        </li>\n        <li class=\"page-item\" (click)=\"nextXPPage()\">\n          <a class=\"page-link\" mdbRippleRadius aria-label=\"Next\">\n            <span aria-hidden=\"true\">&raquo;</span>\n            <span class=\"sr-only\">Next</span>\n          </a>\n        </li>\n      </ul>\n    </nav>\n  </div> -->\n</div>"

/***/ }),

/***/ "./src/app/public/experience/list-feed/item.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListXPPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ListXPPage = /** @class */ (function () {
    function ListXPPage(apiService, uiService, router, store, activatedRoute) {
        var _this = this;
        this.apiService = apiService;
        this.uiService = uiService;
        this.router = router;
        this.store = store;
        this.activatedRoute = activatedRoute;
        this.xpList = [];
        this.xpPage = 1;
        this.xpTotalPage = 1;
        this.pages = [];
        this.routes = {
            '101': 'art-entertainment',
            '102': 'music',
            '103': 'film',
            '104': 'sport-fitness',
            '110': 'community-politic',
            '111': 'creativy',
            '117': 'lifestyle',
            '121': 'science-technology',
            '122': 'college-study',
            '123': 'spiritualy-religion',
            '130': 'business',
            '131': 'fashion-beauty',
            '132': 'food-drink',
            '133': 'nightlife',
            '134': 'street-events',
            '135': 'fitness-health-wellbeing',
            '136': 'outdoors',
            '137': 'travel',
            '138': 'shopping',
            '139': 'kids',
            '140': 'auto-boat-air',
            '141': 'animals',
            '142': 'others',
            '143': 'museums',
            '144': 'theatres',
            '145': 'cinemas'
        };
        this.xpItems = store.select('xpItems');
        //Save category id from url
        this.activatedRoute.params.subscribe(function (params) {
            _this.category = (__WEBPACK_IMPORTED_MODULE_0_underscore__["invert"](_this.routes))[params['id']];
        });
        //Subcribe to show xp items
        this.xpItems.subscribe(function (allXps) {
            _this.xpList = [];
            for (var row = 0; row < allXps.length / 5; row++) {
                _this.xpList[row] = [];
                for (var col = 0; col < 5; col++) {
                    if (allXps[row * 5 + col]) {
                        _this.xpList[row][col] = allXps[row * 5 + col];
                    }
                }
            }
        });
        //Subcribe discover page to load new xps
        store.select('ui').subscribe(function (uiState) {
            _this.ui = uiState;
            //Get city and keyword
            _this.location = uiState.xpLocation;
            _this.keyword = uiState.xpKeyword;
            //Reset page array
            _this.pages = [];
            //Set current page
            _this.xpPage = uiState.xpPage;
            _this.xpTotalPage = uiState.xpTotalPage;
            //Create array of page number
            for (var i = 1; i <= _this.xpTotalPage; i++) {
                _this.pages.push(i);
            }
        });
    }
    ListXPPage.prototype.ngOnInit = function () {
    };
    ListXPPage.prototype.sltPage = function (page) {
        this.apiService.fetchXP(page, this.location, this.keyword, this.category);
    };
    ListXPPage.prototype.prevPage = function () {
        if (this.xpPage == 1)
            return;
        this.apiService.fetchXP(this.xpPage - 1, this.location, this.keyword, this.category);
    };
    ListXPPage.prototype.nextPage = function () {
        if (this.xpPage == this.xpTotalPage)
            return;
        this.apiService.fetchXP(this.xpPage + 1, this.location, this.keyword, this.category);
    };
    ListXPPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/public/experience/list-feed/item.html"),
            styles: [__webpack_require__("./src/app/public/experience/list-feed/item.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_5_app_services_ui__["a" /* UiService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]])
    ], ListXPPage);
    return ListXPPage;
}());



/***/ }),

/***/ "./src/app/public/experience/list-feed/xpcard/item.css":
/***/ (function(module, exports) {

module.exports = ".grid {\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  margin: 6px;\n}\n\n.card {\n  max-height: 100%;\n}\n\n.card img {\n  -o-object-fit: cover;\n     object-fit: cover;\n  width: 100%;\n  height: 20vh;\n}\n\n.card .card-body {\n  width: 15vw;\n  min-height: 15vh;\n}\n\n.card .card-body h5 {\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  overflow: hidden;\n}\n\n.card .card-body p {\n  overflow: hidden;\n  position: relative; \n  line-height: 1.2em;\n  max-height: 2.4em; \n  text-align: justify;  \n  margin-right: -1em;\n  padding-right: 1em;\n}\n\n.card .card-body p:before {\n  content: '...';\n  position: absolute;\n  right: 0;\n  bottom: 0;\n}\n\n.card .card-body p:after {\n  content: '';\n  position: absolute;\n  right: 0;\n  width: 1em;\n  height: 1em;\n  margin-top: 0.2em;\n  background: white;\n}\n"

/***/ }),

/***/ "./src/app/public/experience/list-feed/xpcard/item.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card grid\">\n  <div class=\"view hm-white-slight waves-light\" mdbRippleRadius (click)=\"showDetail()\">\n    <img class=\"img-fluid\" src=\"{{data.image}}\" alt=\"Card image cap\" *ngIf=\"data.image\" style=\"background-color: black;\">\n    <img class=\"img-fluid\" src=\"assets/graphics/placeholders2.png\" alt=\"Card image cap\" *ngIf=\"!data.image\">\n    <a>\n      <div class=\"mask\"></div>\n    </a>\n  </div>\n  <div class=\"card-body\">\n    <h5 class=\"card-title\">{{data.name}}</h5>\n    <p class=\"card-text\">{{data.distance | number: '1.2-2'}}km, {{data.city}}, {{data.country}}</p>\n  </div>\n  <p class=\"card-text\" style=\"padding: 10px;\">\n    <a style=\"margin: auto 5px;\">\n      <i class=\"fas fa-heart\" [ngStyle]=\"data.isLiked ? {'color': '#1e9bff', 'cursor': 'pointer'} : {'color': '#d7d7d7', 'cursor': 'pointer'}\"\n        (click)=\"favourite(data._id)\"></i> {{data.likedBy ? data.likedBy.length : 0}}</a>\n  </p>\n</div>"

/***/ }),

/***/ "./src/app/public/experience/list-feed/xpcard/item.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return XPCardItem; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_ui__ = __webpack_require__("./src/app/services/ui.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var XPCardItem = /** @class */ (function () {
    function XPCardItem(store, router, apiService, uiService) {
        this.store = store;
        this.router = router;
        this.apiService = apiService;
        this.uiService = uiService;
    }
    XPCardItem.prototype.ngOnInit = function () {
    };
    XPCardItem.prototype.ngOnDestroy = function () {
    };
    XPCardItem.prototype.favourite = function (id) {
        this.apiService.favouriteXPFeed(id);
    };
    XPCardItem.prototype.showDetail = function () {
        this.router.navigate(['/experience/feed/' + this.data._id]).then(function (data) {
            console.log('route success');
        }).catch(function (e) {
            console.log('route failed', e);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], XPCardItem.prototype, "data", void 0);
    XPCardItem = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'xp-card',
            template: __webpack_require__("./src/app/public/experience/list-feed/xpcard/item.html"),
            styles: [__webpack_require__("./src/app/public/experience/list-feed/xpcard/item.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_4_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_3_app_services_ui__["a" /* UiService */]])
    ], XPCardItem);
    return XPCardItem;
}());



/***/ }),

/***/ "./src/app/public/home/home.css":
/***/ (function(module, exports) {

module.exports = ".slider-img{\n\theight: 600px;\n\t/* background-image: url('/assets/graphics/girls_party.jpg'); */\n  background-width: 80%;\n  background-repeat: no-repeat;\n  background-size: 100%;\n  z-index: -2;\n}\n\n.slider-img h2{\n\ttext-align: center;\n  color: white;\n\tpadding-top: 7%;\n\tfont-weight: 700;\n  font-size: 140px;\n  margin-bottom: 0px;\n  margin-top: 0px;\n}\n\n.textbox {\n  position: relative;\n  top: 360px;\n}\n\n.textbox h2{\n\ttext-align: center;\n  /*color: rgba(86,3,173, 1);*/\n  position: relative;\n\tpadding-top: 0%;\n\tfont-weight: 100;\n  font-size: 108px;\n  margin-bottom: 10px;\n  bottom: 2px;\n  width: 100%;\n  text-shadow: 1px 1px black;\n}\n\n.banner {\n  width: 100%;\n  margin: 0 auto;\n}\n\n.banner h2 {\n  text-align: center;\n  font-weight: normal;\n  font-size: 60px;\n  color: #5603AD;\n  margin-top: 60px;\n  margin-bottom: 60px;\n}\n\n.banner h4 {\n  text-align: center;\n  margin: 0 25% 0;\n  font-weight: lighter;\n  color: grey;\n  font-size: 25px;\n}\n\n.btn-position {\n  text-align: center;\n  margin-bottom: 60px;\n}\n\n.items {\n  width: 100%;\n  margin-top: 40px;\n}\n\n.items {\n  font-size: 40px;\n}\n\n.width-50{\n\twidth: 50% !important;\n\tdisplay: inline;\n  padding-left: 0;\n}\n\n.inline{\n\tdisplay: inline-block;\n}\n\n.content-section{\n  position: absolute;\n  width: 350px;\n  padding-left: 40px;\n}\n\n.inline.content-section {\n  padding-bottom: 14px;\n  margin-top: 40px;\n  margin-left: -180px;\n}\n\n.inline.content-section h2 {\n  font-size: 45px;\n}\n\n.inline.content-section p {\n  font-size: 20px;\n  color: grey;\n  font-weight: lighter;\n}\n\n.mg-left-content {\n  margin-left: 8.1%;\n}\n\n.lighter {\n  font-weight: lighter;\n}\n\n.app-icons .left-clm img {\n  width: 300px;\n  position: relative;\n  left: 230px;\n\n}\n\n.app-icons .right-clm img {\n  width: 280px;\n  position: relative;\n  right: 230px;\n}\n\n.app-icon {\n  display: inline-block;\n}\n\n.left-clm {\n  float: left;\n  width: 50%;\n  display: inline;\n  text-align: center;\n}\n\n.right-clm {\n  float: right;\n  width: 50%;\n  display: inline;\n  text-align: center;\n  position: relative;\n  bottom: 26px;\n}\n\n.clearfix {\n  clear: both;\n}"

/***/ }),

/***/ "./src/app/public/home/home.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"slider-img\">\n\t<div class=\"text-center\">\n\t\t<div class=\"textbox\">\n\t\t\t<h2>Spinshare for the best experience!</h2>\n\t\t</div>\n\t</div>\n</section>\n\n<section class=\"banner\">\n\t<h2>Welcome to Spinshare!</h2>\n\t<div class=\"btn-position\">\n\t\t<button type=\"button\" class=\"btn btn-primary btn-lg waves-light\" mdbRippleRadius style=\"border-radius: 30px; width: 200px;\">Login</button>\n\t\t<button type=\"button\" class=\"btn btn-secondary btn-lg waves-light\" mdbRippleRadius style=\"border-radius: 30px; width: 200px;\">Download</button>\n\t</div>\n\t<h4>Spinshare provides a platform for crowd-sourced reviews and multimedia content sharing by customers about local businesses,\n\t\tvenues and events in a social media context.</h4>\n</section>\n\n<section class=\"items\">\n\t<div class=\"width-50\">\n\t\t<div class=\"inline\">\n\t\t\t<img src=\"../../assets/graphics/experience.png\" width=\"650px\">\n\t\t</div>\n\t\t<div class=\"inline content-section\">\n\t\t\t<h2 class=\"lighter\">experience.</h2>\n\t\t\t<p>Choose from 1,000s of activities, places, events and clubs in your area. No more boring nights out! The Experience feature\n\t\t\t\tgives you a clear glimpse of what is going on in a particular location or event and what previous visitors have to say\n\t\t\t\tand show about it!</p>\n\t\t</div>\n\t</div>\n\t<div class=\"width-50\">\n\t\t<div class=\"inline mg-left-content\">\n\t\t\t<img src=\"../../assets/graphics/share.png\" width=\"650px\">\n\t\t</div>\n\t\t<div class=\"inline content-section\">\n\t\t\t<h2 class=\"lighter\">share.</h2>\n\t\t\t<p>Upload and share your unique experiences. This feature allows you to share your latest adventures with your friends background\n\t\t\t\tcircle. Here you can catch up and comment on what your peers have been up to and link it back with the place where they\n\t\t\t\tmade their upload. Keep your peers up to date, and let them tell you where it's at! </p>\n\t\t</div>\n\t</div>\n\t<div class=\"clearfix\"></div>\n\t<div class=\"width-50\">\n\t\t<div class=\"inline\">\n\t\t\t<img src=\"../../assets/graphics/connect.png\" width=\"650px\">\n\t\t</div>\n\t\t<div class=\"inline content-section\">\n\t\t\t<h2 class=\"lighter\">discover.</h2>\n\t\t\t<p>Discover the most popular uploads of various amazing locations. This unique feature allows you quick access to the most\n\t\t\t\tpopular uploads in any area of the world. This way you can see what's hot anywhere in the world at a click of your finger.</p>\n\t\t</div>\n\t</div>\n\t<div class=\"width-50\">\n\t\t<div class=\"inline mg-left-content\">\n\t\t\t<img src=\"../../assets/graphics/connect.png\" width=\"650px\">\n\t\t</div>\n\t\t<div class=\"inline content-section\">\n\t\t\t<h2 class=\"lighter\">connect.</h2>\n\t\t\t<p>Connect with like-minded people and people in your area to find out more about what they are up to. Our chat feature allows\n\t\t\t\tyou to stay in touch with people with similar interests and people in your nearby area.</p>\n\t\t</div>\n\t</div>\n</section>\n\n<section class=\"app-icons\">\n\t<div class=\"left-clm\">\n\t\t<img src=\"../../assets/graphics/ios.png\" />\n\t</div>\n\t<div class=\"right-clm\">\n\t\t<img src=\"../../assets/graphics/google-play.png\" />\n\t</div>\n\t<div class=\"clearfix\"></div>\n</section>"

/***/ }),

/***/ "./src/app/public/home/home.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomePage = /** @class */ (function () {
    function HomePage(store, router) {
        this.store = store;
        this.router = router;
    }
    HomePage.prototype.ngOnInit = function () {
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/public/home/home.html"),
            styles: [__webpack_require__("./src/app/public/home/home.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]])
    ], HomePage);
    return HomePage;
}());



/***/ }),

/***/ "./src/app/public/login/login.css":
/***/ (function(module, exports) {

module.exports = ".app-login {\n  height: 90vh;\n  /* background-image: url('/assets/graphics/login.jpg'); */\n  background-repeat: no-repeat;\n  background-size: cover;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.app-login > div {\n  margin: auto;\n}\n\n.app-login > div > .login-form {\n  width: 30vw;\n  background-color: white;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 20px 20px;\n}\n\n.app-login > div > .login-form > h1 {\n  text-align: center;\n}"

/***/ }),

/***/ "./src/app/public/login/login.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"app-login\">\n  <div>\n    <div class=\"login-form\" (keyup.enter)=\"login()\">\n      <h1>login.</h1>\n      <div class=\"md-form\">\n        <i class=\"fa fa-envelope prefix grey-text\"></i>\n        <input mdbActive type=\"text\" id=\"email\" [(ngModel)]=\"email\" class=\"form-control\">\n        <label for=\"email\">Email</label>\n        <div *ngIf=\"isInvalid\"><i style=\"color: red;\">Your email is invalid</i></div>\n      </div>\n      <div class=\"md-form\">\n        <i class=\"fa fa-lock prefix grey-text\"></i>\n        <input mdbActive type=\"password\" id=\"pwd\" [(ngModel)]=\"password\" class=\"form-control\">\n        <label for=\"pwd\">Password</label>\n        <div *ngIf=\"isEmpty\"><i style=\"color: red;\">Please enter your email and password</i></div>\n        <div *ngIf=\"isLoginFailed\"><i style=\"color: red;\">Username is not existed or your password is wrong</i></div>\n      </div>\n      <button type=\"button\" class=\"btn btn-secondary waves-light\" mdbRippleRadius (click)=\"login()\">Login</button>\n      <button type=\"button\" class=\"btn btn-primary waves-light\" mdbRippleRadius>Login with Facebook</button>\n      <div style=\"display: flex; flex-direction: column; justify-content: center; align-items: center; font-size: 12px;\">\n        <a class=\"forgot-pass\" routerLink=\"/password_reset\" style=\"margin-top: 5px; margin-bottom: 5px;\">Forgot password?</a>\n        <a class=\"signin\" routerLink=\"/registration\">Not a member yet?</a>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/public/login/login.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_utils_util__ = __webpack_require__("./src/app/utils/util.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginPage = /** @class */ (function () {
    function LoginPage(store, apiService, router, utilService) {
        this.store = store;
        this.apiService = apiService;
        this.router = router;
        this.utilService = utilService;
    }
    LoginPage.prototype.login = function () {
        var _this = this;
        this.isEmpty = false;
        this.isInvalid = false;
        this.isLoginFailed = false;
        if (!this.email || !this.password) {
            this.isEmpty = true;
            return;
        }
        if (!this.utilService.isEmailValid(this.email)) {
            this.isInvalid = true;
            return;
        }
        this.isEmpty = false;
        this.isInvalid = false;
        this.apiService.login(this.email, this.password).then(function () {
        }).catch(function (err) {
            _this.isLoginFailed = true;
            console.log(err);
        });
    };
    LoginPage.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], LoginPage.prototype, "email", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], LoginPage.prototype, "password", void 0);
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            template: __webpack_require__("./src/app/public/login/login.html"),
            styles: [__webpack_require__("./src/app/public/login/login.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_3_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_4_app_utils_util__["a" /* UtilService */]])
    ], LoginPage);
    return LoginPage;
}());



/***/ }),

/***/ "./src/app/public/password_reset/password_reset.css":
/***/ (function(module, exports) {

module.exports = ".password-reset {\n  height: 90vh;\n  background-image: url('/assets/graphics/login.jpg');\n  background-repeat: no-repeat;\n  background-size: cover;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.password-reset > div {\n  width: 40vw;\n  padding: 30px 40px;\n  margin: auto;\n  background-color: white;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.password-reset > div > h3 {\n  margin-bottom: 20px;\n}"

/***/ }),

/***/ "./src/app/public/password_reset/password_reset.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"password-reset\">\n  <div>\n    <h3>Enter your email or phone number!</h3>\n    <div class=\"md-form\">\n      <input mdbActive type=\"text\" id=\"email\" class=\"form-control\" [(ngModel)]=\"email\">\n      <label for=\"email\">Email/Phone Number</label>\n    </div>\n    <button class=\"btn btn-secondary btn-lg\" (click)=\"reset()\">Password Reset</button>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/public/password_reset/password_reset.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ResetPasswordPage = /** @class */ (function () {
    function ResetPasswordPage(store) {
        this.store = store;
    }
    ResetPasswordPage.prototype.reset = function () {
        console.log(this.email);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], ResetPasswordPage.prototype, "email", void 0);
    ResetPasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'password_reset',
            template: __webpack_require__("./src/app/public/password_reset/password_reset.html"),
            styles: [__webpack_require__("./src/app/public/password_reset/password_reset.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["a" /* Store */]])
    ], ResetPasswordPage);
    return ResetPasswordPage;
}());



/***/ }),

/***/ "./src/app/public/registration/registration.css":
/***/ (function(module, exports) {

module.exports = ".registration {\n  height: 90vh;\n  background-image: url('/assets/graphics/login.jpg');\n  background-repeat: no-repeat;\n  background-size: cover;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.registration > div {\n  width: 30vw;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 30px 40px;\n  background-color: white;\n  margin: auto;\n}\n\n.registration > div > h3 {\n  text-align: center;\n  margin-bottom: 1.2rem;\n}"

/***/ }),

/***/ "./src/app/public/registration/registration.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"registration\">\n  <div>\n    <h3>Register a new account.</h3>\n    <div class=\"md-form\">\n      <input mdbActive type=\"text\" id=\"email\" class=\"form-control\" [(ngModel)]=\"email\">\n      <label for=\"email\">Email/Phone Number</label>\n    </div>\n    <div class=\"md-form\">\n      <input mdbActive type=\"text\" id=\"pwd\" class=\"form-control\" [(ngModel)]=\"password\">\n      <label for=\"pwd\">Password</label>\n    </div>\n    <button class=\"btn btn-secondary btn-lg\" (click)=\"signup()\">Register</button>\n    <!-- <button class=\"btn btn-primary btn-lg\">Sign up with Facebook</button> -->\n    <a routerLink=\"/login\">Already have an account? Click here</a>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/public/registration/registration.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistrationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegistrationPage = /** @class */ (function () {
    function RegistrationPage(store, apiService, router) {
        this.store = store;
        this.apiService = apiService;
        this.router = router;
    }
    RegistrationPage.prototype.signup = function () {
        this.apiService.signup(this.email, this.password);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], RegistrationPage.prototype, "email", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], RegistrationPage.prototype, "password", void 0);
    RegistrationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'registration',
            template: __webpack_require__("./src/app/public/registration/registration.html"),
            styles: [__webpack_require__("./src/app/public/registration/registration.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2_app_services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], RegistrationPage);
    return RegistrationPage;
}());



/***/ }),

/***/ "./src/app/public/verification/verification.css":
/***/ (function(module, exports) {

module.exports = ".verification {\n  height: 90vh;\n  background-image: url('/assets/graphics/login.jpg');\n  background-repeat: no-repeat;\n  background-size: cover;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n}\n\n.verification > div {\n  width: 30vw;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  padding: 30px 40px;\n  background-color: white;\n  margin: auto;\n}\n\n.verification > div > h3 {\n  text-align: center;\n  margin-bottom: 1.2rem;\n}"

/***/ }),

/***/ "./src/app/public/verification/verification.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"verification\">\n  <div>\n    <h3>A verification code was sent to you. Please enter your code below.</h3>\n    <div class=\"md-form\">\n      <input mdbActive type=\"text\" id=\"code\" class=\"form-control\" [(ngModel)]=\"token\" placeholder=\"XXXXXX\" autofocus>\n      <label for=\"code\">Verification code</label>\n    </div>\n    <button class=\"btn btn-secondary btn-lg\" (click)=\"verify()\">Continue</button>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/public/verification/verification.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VerificationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api__ = __webpack_require__("./src/app/services/api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var VerificationPage = /** @class */ (function () {
    function VerificationPage(store, apiService, router) {
        this.store = store;
        this.apiService = apiService;
        this.router = router;
    }
    VerificationPage.prototype.verify = function () {
        this.apiService.verify(this.token);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], VerificationPage.prototype, "token", void 0);
    VerificationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'verification',
            template: __webpack_require__("./src/app/public/verification/verification.html"),
            styles: [__webpack_require__("./src/app/public/verification/verification.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2__services_api__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]])
    ], VerificationPage);
    return VerificationPage;
}());



/***/ }),

/***/ "./src/app/services/api.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__agm_core__ = __webpack_require__("./node_modules/@agm/core/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






//localhost
// const BASE_URL = 'http://localhost:8096';
//product
var BASE_URL = 'http://34.211.21.108:8096';
var ApiService = /** @class */ (function () {
    function ApiService(store, http, router, mapsAPILoader) {
        this.store = store;
        this.http = http;
        this.router = router;
        this.mapsAPILoader = mapsAPILoader;
    }
    //SUSPENDING
    ApiService.prototype.makePayment = function (adid, nonce, amount) {
        var _this = this;
        var body = 'nonce=' + nonce + '&amount=' + amount;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.post(BASE_URL + '/ad/' + adid + '/payment', body, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            console.log('make payment: ', ret);
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            console.log(err);
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.fetchPayment = function (adid) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/ad/' + adid + '/payment', HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'FETCH_AD',
                        payload: ret.data
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    //Apis for user
    ApiService.prototype.signup = function (email, password) {
        var _this = this;
        var body = 'email=' + email + '&password=' + password + '';
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            })
        };
        this.http.post(BASE_URL + '/signup', body, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.router.navigate(['/verification']);
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.verify = function (token) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            })
        };
        this.http.get(BASE_URL + '/verify?token=' + token, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'LOGIN',
                        payload: true
                    });
                    _this.setLocation();
                    _this.router.navigate(['/experience']);
                    localStorage.setItem('access_token', ret.accessToken);
                    _this.getUser();
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.login = function (email, password) {
        var _this = this;
        var body = 'email=' + email + '&password=' + password + '';
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
            })
        };
        return new Promise(function (resolve, reject) {
            _this.http.post(BASE_URL + '/login', body, HEADER)
                .subscribe(function (res) {
                _this.store.dispatch({
                    type: 'END_FETCHING'
                });
                var ret = res.json();
                switch (ret.status) {
                    case 200:
                        _this.store.dispatch({
                            type: 'LOGIN',
                            payload: true
                        });
                        _this.setLocation();
                        _this.router.navigate(['/experience']);
                        localStorage.setItem('access_token', ret.accessToken);
                        _this.getUser();
                        return resolve();
                    default:
                        return reject();
                }
            }, function (err) {
                _this.store.dispatch({
                    type: 'END_FETCHING'
                });
                return reject(err);
            });
        });
    };
    ApiService.prototype.getUser = function () {
        var _this = this;
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/get_user', HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'FETCH_USER',
                        payload: ret.data
                    });
                    break;
                default:
                    if (_this.router) {
                        _this.router.navigate(['/login']);
                        localStorage.removeItem('access_token');
                    }
                    break;
            }
        });
    };
    ApiService.prototype.setLocation = function () {
        var _this = this;
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.findLocationByIP().then(function (data) {
            var loc = data.loc;
            var body = 'lat=' + loc.lat + '&long=' + loc.long;
            //Update new location to backend
            _this.http.post(BASE_URL + '/set_location', body, HEADER)
                .subscribe(function (res) {
                return _this.store.dispatch({
                    type: 'UPDATE_LOCATION',
                    payload: loc.address
                });
                // this.findAddressByLatLong(loc.lat, loc.long).then((address: any) => {
                //   return this.store.dispatch({
                //     type: 'UPDATE_LOCATION',
                //     payload: address.result.formatted_address
                //   });
                // }).catch(e => {
                //   console.log(e)
                //   return this.store.dispatch({
                //     type: 'UPDATE_LOCATION',
                //     payload: null
                //   });
                // })
            }, function (err) {
                return _this.store.dispatch({
                    type: 'UPDATE_LOCATION',
                    payload: null
                });
            });
        });
    };
    ApiService.prototype.getLocation = function () {
        var _this = this;
        this.findLocationByIP().then(function (data) {
            var loc = data.loc;
            return _this.store.dispatch({
                type: 'UPDATE_LOCATION',
                payload: loc.address
            });
            // this.findAddressByLatLong(loc.lat, loc.long).then((address: any) => {
            //   return this.store.dispatch({
            //     type: 'UPDATE_LOCATION',
            //     payload: address.result.formatted_address
            //   });
            // }).catch(e => {
            //   return this.store.dispatch({
            //     type: 'UPDATE_LOCATION',
            //     payload: null
            //   });
            // })
        });
    };
    //Apis for Experience page
    ApiService.prototype.fetchCategories = function () {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/categories', HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'FETCH_CATEGORY',
                        payload: ret.categories
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.fetchXP = function (page, location, keyword, category) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/xpfeed?page=' + page + '&page_size=30&status=confirmed&address=' + location + '&keyword=' + keyword + '&category=' + category + '&radius=1000000', HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'FETCH_XP',
                        payload: ret.data,
                        page: parseInt(page),
                        total_pages: parseInt(ret.pages)
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.fetchXpDetail = function (id, scope) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/get_details/place?id=' + id + '&scope=' + scope, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'FETCH_XP_DETAIL',
                        payload: ret.data
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.fetchXpHappening = function (id, type, scope, page) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/list_happening?feedId=' + id + '&feedScope=' + scope + '&feedType=' + type, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'FETCHING_HAPPENING',
                        payload: __WEBPACK_IMPORTED_MODULE_0_underscore__["sortBy"](ret.data, 'date_time')
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.likeXpItem = function (id, type, scope) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        var body = 'feedID=' + id + '&feedType=' + type + '&feedScope=' + scope;
        this.http.post(BASE_URL + '/like_xp_feed', body, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'LIKE_XP_FEED',
                        payload: id
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.dislikeXpItem = function (id, type, scope) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        var body = 'feedID=' + id + '&feedType=' + type + '&feedScope=' + scope;
        this.http.post(BASE_URL + '/dislike_xp_feed', body, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'DISLIKE_XP_FEED',
                        payload: id
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.followXpItem = function (id, type, scope) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        var body = 'feedID=' + id + '&feedType=' + type + '&feedScope=' + scope;
        this.http.post(BASE_URL + '/follow_xp_feed', body, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'FOLLOW_XP_FEED',
                        payload: id
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.favouriteXPFeed = function (id) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/json',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.post(BASE_URL + '/favourit', {
            id: id
        }, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'FAVOURITE_XP_FEED',
                        payload: {
                            feedId: id,
                            user: ret.user
                        }
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.getDetailFeed = function (id) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/get_details/feed?id=' + id, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'FETCH_DETAIL_FEED',
                        payload: ret.data
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.createShareFeed = function (feed, user, media) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        var formData = new FormData();
        formData.append('content', feed.content);
        formData.append('content_type', feed.content_type);
        formData.append('thumbnail', null);
        formData.append('caption', feed.caption);
        formData.append('attr_id', feed.attr_id);
        formData.append('attr_type', feed.attr_type);
        formData.append('attr_scope', feed.attr_scope);
        this.http.post(BASE_URL + '/share_feed', formData, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'CREATE_SHARE_FEED',
                        payload: {
                            id: ret.id,
                            user_id: user.id,
                            user_name: user.username,
                            user_picture: user.profile_pic,
                            liked: false,
                            seen: false,
                            content: media ? media : feed.content,
                            thumbnail: feed.content_type == 'video' ? feed.thumbnail : '',
                            content_type: feed.content_type,
                            caption: feed.caption,
                            date_time: Math.ceil(Date.now() / 1000),
                            comments_count: 0,
                            likes_count: 0,
                            tagged: false
                        }
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.fetchCinema = function (page, address) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/cinemas?page=' + page + '&page_size=30&radius=100000&address=' + address, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'FETCH_CINEMAS',
                        payload: ret.data,
                        page: parseInt(ret.page_number),
                        total_pages: parseInt(ret.total_pages)
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.getDetailCinema = function (id) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/cinema?id=' + id, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'FETCH_CINEMA',
                        payload: ret.data
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    //Apis for Discover page
    ApiService.prototype.fetchTrendings = function (page, location) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
                // 'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/trending_feeds?page=' + page + '&page_size=8&city=' + location, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    if (ret.data && ret.data.length) {
                        _this.store.dispatch({
                            type: 'FETCH_TRENDING',
                            payload: ret.data,
                            page: parseInt(ret.page_number),
                            total_pages: parseInt(ret.total_pages)
                        });
                    }
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.fetchTrendingDetail = function (id, scope) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/get_details/place?id=' + id + '&scope=' + scope, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'FETCH_TRENDING_DETAIL',
                        payload: ret.data
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.fetchSpecials = function (page, location) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded'
                // 'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/special_feeds?page=' + page + '&page_size=8&address=' + location, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    if (ret.data && ret.data.length) {
                        _this.store.dispatch({
                            type: 'FETCH_SPECIAL',
                            payload: ret.data,
                            page: parseInt(ret.page_number),
                            total_pages: parseInt(ret.total_pages)
                        });
                    }
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    //Apis for Connect page
    ApiService.prototype.fetchConversation = function (page) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/list_conversation?page=' + page, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    if (ret.data && ret.data.length) {
                        _this.store.dispatch({
                            type: 'FETCH_CONVERSATION',
                            payload: ret.data,
                            page: parseInt(ret.page_number),
                            total_pages: parseInt(ret.total_pages)
                        });
                    }
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.saveConversation = function (message) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/json',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.post(BASE_URL + '/save_conversation', message, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'SAVE_CONVERSATION',
                        payload: message
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.createPersonalConversation = function (sender, participants) {
        this.store.dispatch({
            type: 'CREATE_CONVERSATION',
            payload: {
                conversationID: participants[0].id,
                sender: sender,
                participants: participants,
                isGroup: false
            }
        });
    };
    ApiService.prototype.createGroupConversation = function (participants, name, avatar, creator) {
        var _this = this;
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        var formData = new FormData();
        formData.append('avatar', avatar);
        __WEBPACK_IMPORTED_MODULE_0_underscore__["each"](participants, function (p) {
            formData.append('participantIDs', p.id);
        });
        formData.append('participantIDs', creator.id);
        formData.append('name', name);
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        this.http.post(BASE_URL + '/create_conversation_web', formData, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'CREATE_CONVERSATION',
                        payload: {
                            conversationID: ret.id,
                            sender: creator,
                            participants: participants,
                            avatar: ret.picture,
                            name: ret.name,
                            isGroup: true
                        }
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    //Apis for Profile page
    ApiService.prototype.fetchFriends = function () {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/list_friends', HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    if (ret.data && ret.data.length) {
                        _this.store.dispatch({
                            type: 'FETCH_FRIEND',
                            payload: ret.data
                        });
                    }
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.fetchUpload = function () {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/list_my_feeds', HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    if (ret.data && ret.data.length) {
                        _this.store.dispatch({
                            type: 'FETCH_UPLOAD',
                            payload: ret.data
                        });
                    }
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.fetchReceivedInvitations = function () {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/list_received_invites', HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    if (ret.data && ret.data.length) {
                        _this.store.dispatch({
                            type: 'FETCH_RECEIVED_INVITATIONS',
                            payload: ret.data
                        });
                    }
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.fetchSentInvitations = function () {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/list_sent_invites', HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    if (ret.data && ret.data.length) {
                        _this.store.dispatch({
                            type: 'FETCH_SENT_INVITATIONS',
                            payload: ret.data
                        });
                    }
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.updatePref = function (prefs) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/json',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.post(BASE_URL + '/preferences', {
            preferences: prefs
        }, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'UPDATE_PREFERENCES_SUCCESS'
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.updateAcc = function (accInfo) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/json',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.post(BASE_URL + '/profile', {
            username: accInfo.username,
            bday: (new Date(accInfo.birthday).getTime()) / 1000,
            phone: accInfo.phone
        }, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'UPDATE_PROFILE_SUCCESS',
                        payload: accInfo
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.updatePermission = function (resource, mode) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/json',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        var permissions = {
            friends: false,
            friendsOfFriends: false,
            onlyMe: false,
            public: false
        };
        switch (mode) {
            case 'friends':
                permissions.friends = true;
                break;
            case 'fof':
                permissions.friendsOfFriends = true;
                break;
            case 'public':
                permissions.public = true;
                break;
            case 'onlyMe':
                permissions.onlyMe = true;
                break;
        }
        this.http.post(BASE_URL + '/permissions', {
            resource: resource,
            permissions: permissions
        }, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'UPDATE_PERMISSION_SUCCESS',
                        payload: {
                            resource: resource,
                            mode: mode
                        }
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    //Api for Upload page
    ApiService.prototype.createXPFeed = function (feed) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        var formData = new FormData();
        formData.append('name', feed.name);
        formData.append('phone', feed.phone);
        formData.append('address', feed.address);
        formData.append('email', feed.email);
        formData.append('website', feed.website);
        formData.append('city', feed.city);
        formData.append('country', feed.country);
        formData.append('postcode', feed.postcode);
        formData.append('category', feed.category.id);
        formData.append('image', feed.image);
        formData.append('type', feed.type);
        formData.append('startTime', feed.startTime);
        formData.append('endTime', feed.endTime);
        this.http.post(BASE_URL + '/xpfeed', formData, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'CREATE_XP_FEED',
                        payload: {
                            feed: ret.feed,
                            distance: ret.distance
                        }
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.getMyFeed = function () {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/xpfeed/me', HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'FETCH_XP_FEED',
                        payload: ret.data
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.getAllFeeds = function () {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.get(BASE_URL + '/xpfeed', HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'FETCH_XP_FEED',
                        payload: ret.data
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.createXPSharefeed = function (sharefeed, user, xpfeed) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        var formData = new FormData();
        formData.append('xpFeedId', xpfeed);
        formData.append('content', sharefeed.content);
        formData.append('type', sharefeed.type);
        formData.append('thumbnail', null);
        formData.append('caption', sharefeed.caption);
        formData.append('user_id', user.id);
        formData.append('user_name', user.username);
        formData.append('user_avatar', user.profile_pic);
        this.http.post(BASE_URL + '/xp_share_feed', formData, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    var newSharefeed = ret.data;
                    _this.store.dispatch({
                        type: 'CREATE_XP_SHARE_FEED',
                        payload: newSharefeed
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.createXPReview = function (review, user, xpfeed) {
        var _this = this;
        this.store.dispatch({
            type: 'START_FETCHING'
        });
        var HEADER = {
            headers: new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'content-type': 'application/json',
                'x-access-token': localStorage.getItem('access_token')
            })
        };
        this.http.post(BASE_URL + '/xp_review', {
            review: review.content,
            rating: review.rating,
            xpFeedId: xpfeed,
            user_id: user.id,
            user_name: user.username,
            user_avatar: user.profile_pic
        }, HEADER)
            .subscribe(function (res) {
            var ret = res.json();
            switch (ret.status) {
                case 200:
                    _this.store.dispatch({
                        type: 'CREATE_XP_REVIEW',
                        payload: ret.data
                    });
                    break;
                default:
                    break;
            }
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        }, function (err) {
            _this.store.dispatch({
                type: 'END_FETCHING'
            });
        });
    };
    ApiService.prototype.findPostcodeByLatLong = function (lat, long) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get('https://api.postcodes.io/postcodes?lon=' + long + '&lat=' + lat).subscribe(function (res) {
                var ret = res.json();
                console.log(ret);
                switch (ret.status) {
                    case 200:
                        if (ret.result.length) {
                            return resolve({
                                data: ret.result[0]
                            });
                        }
                        else {
                            return reject({
                                message: 'Can not found postcode'
                            });
                        }
                    default:
                        return reject({
                            message: 'Can not found postcode'
                        });
                }
            });
        });
    };
    ApiService.prototype.findAddressByLatLong = function (lat, long) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (navigator.geolocation) {
                _this.mapsAPILoader.load().then(function () {
                    var geocoder = new google.maps.Geocoder();
                    var latlng = new google.maps.LatLng(lat, long);
                    var request = { latLng: latlng };
                    geocoder.geocode(request, function (results, status) {
                        if (!results.length) {
                            return reject({
                                message: 'Can not find address'
                            });
                        }
                        if (status === google.maps.GeocoderStatus.OK) {
                            return resolve({
                                result: results[0]
                            });
                        }
                        else {
                            return reject({
                                message: 'Can not find address'
                            });
                        }
                    });
                }).catch(function (e) {
                    return reject({
                        message: 'Can not find address'
                    });
                });
            }
            else {
                return reject({
                    message: 'Browser does not support this feature'
                });
            }
        });
    };
    ApiService.prototype.findLatLongByAddress = function (address) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (navigator.geolocation) {
                _this.mapsAPILoader.load().then(function () {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'address': address
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            return resolve({
                                lat: results[0].geometry.location.lat(),
                                long: results[0].geometry.location.lng()
                            });
                        }
                        else {
                            return reject({
                                message: 'Can not find address'
                            });
                        }
                    });
                }).catch(function (e) {
                    return reject({
                        message: 'Can not find address'
                    });
                });
            }
            else {
                return reject({
                    message: 'Browser does not support this feature'
                });
            }
        });
    };
    ApiService.prototype.findLatLongByPostcode = function (postcode) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get('https://api.postcodes.io/postcodes/' + postcode).subscribe(function (res) {
                var ret = res.json();
                console.log(ret);
                switch (ret.status) {
                    case 200:
                        if (ret.result) {
                            return resolve({
                                lat: ret.result.latitude,
                                long: ret.result.longitude
                            });
                        }
                        else {
                            return reject({
                                message: 'Can not found postcode'
                            });
                        }
                    default:
                        return reject({
                            message: 'Can not found postcode'
                        });
                }
            });
        });
    };
    ApiService.prototype.findLocationByIP = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(BASE_URL + '/findLocationByIp').subscribe(function (res) {
                var ret = res.json();
                switch (ret.status) {
                    case 200:
                        return resolve({
                            loc: ret.loc
                        });
                    default:
                        return reject({
                            message: 'Something went wrong'
                        });
                }
            });
        });
    };
    ApiService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ngrx_store__["a" /* Store */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_5__agm_core__["c" /* MapsAPILoader */]])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "./src/app/services/ui.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UiService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__("./node_modules/@ngrx/store/@ngrx/store.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UiService = /** @class */ (function () {
    function UiService(store) {
        this.store = store;
    }
    UiService.prototype.logout = function () {
        this.store.dispatch({
            type: 'LOGOUT',
            payload: false
        });
        localStorage.removeItem('access_token');
    };
    UiService.prototype.sltTrendingPage = function (page) {
        this.store.dispatch({
            type: 'SELECT_TRENDING_PAGE',
            payload: page
        });
    };
    UiService.prototype.sltSpecialPage = function (page) {
        this.store.dispatch({
            type: 'SELECT_SPECIAL_PAGE',
            payload: page
        });
    };
    UiService.prototype.sltXPPage = function (page) {
        this.store.dispatch({
            type: 'SELECT_XP_PAGE',
            payload: page
        });
    };
    UiService.prototype.sltCinemaPage = function (page) {
        this.store.dispatch({
            type: 'SELECT_CINEMA_PAGE',
            payload: page
        });
    };
    UiService.prototype.showXpItemMore = function (itemId) {
        this.store.dispatch({
            type: 'SHOW_XP_ITEM_MORE',
            payload: itemId
        });
    };
    UiService.prototype.showTrendingMore = function (itemId) {
        this.store.dispatch({
            type: 'SHOW_TRENDING_MORE',
            payload: itemId
        });
    };
    UiService.prototype.changeFeedMenu = function (mode) {
        this.store.dispatch({
            type: 'CHANGE_FEED_MENU',
            payload: mode
        });
    };
    UiService.prototype.changeCinemaMenu = function (mode) {
        this.store.dispatch({
            type: 'CHANGE_CINEMA_MENU',
            payload: mode
        });
    };
    UiService.prototype.selectConversation = function (convId) {
        this.store.dispatch({
            type: 'SELECT_CONVERSATION',
            payload: convId
        });
    };
    UiService.prototype.getMessages = function (convId, messages) {
        this.store.dispatch({
            type: 'GET_MESSAGES',
            payload: {
                convId: convId,
                messages: messages
            }
        });
    };
    UiService.prototype.setTyping = function (convId, status) {
        this.store.dispatch({
            type: 'IS_TYPING',
            payload: {
                convId: convId,
                status: status
            }
        });
    };
    UiService.prototype.updatePref = function (cat, sub) {
        this.store.dispatch({
            type: 'UPDATE_PREF',
            payload: {
                cat: cat,
                sub: sub
            }
        });
    };
    UiService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ngrx_store__["a" /* Store */]])
    ], UiService);
    return UiService;
}());



/***/ }),

/***/ "./src/app/store/ads.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ads; });
var ads = function (state, action) {
    if (state === void 0) { state = []; }
    var _state = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case 'FETCH_AD':
            _state = [action.payload];
            return _state;
        case 'LOGOUT':
            return [];
        default:
            return _state;
    }
};


/***/ }),

/***/ "./src/app/store/app.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ui__ = __webpack_require__("./src/app/store/ui.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__loading__ = __webpack_require__("./src/app/store/loading.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user__ = __webpack_require__("./src/app/store/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__trendings__ = __webpack_require__("./src/app/store/trendings.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__specials__ = __webpack_require__("./src/app/store/specials.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ads__ = __webpack_require__("./src/app/store/ads.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__categories__ = __webpack_require__("./src/app/store/categories.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__xpItems__ = __webpack_require__("./src/app/store/xpItems.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__feed__ = __webpack_require__("./src/app/store/feed.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__conversations__ = __webpack_require__("./src/app/store/conversations.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__cinemas__ = __webpack_require__("./src/app/store/cinemas.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__detailCinema__ = __webpack_require__("./src/app/store/detailCinema.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__xpFeeds__ = __webpack_require__("./src/app/store/xpFeeds.ts");













/* harmony default export */ __webpack_exports__["a"] = ({
    ui: __WEBPACK_IMPORTED_MODULE_0__ui__["a" /* ui */],
    loading: __WEBPACK_IMPORTED_MODULE_1__loading__["a" /* loading */],
    user: __WEBPACK_IMPORTED_MODULE_2__user__["a" /* user */],
    trendings: __WEBPACK_IMPORTED_MODULE_3__trendings__["a" /* trendings */],
    specials: __WEBPACK_IMPORTED_MODULE_4__specials__["a" /* specials */],
    ads: __WEBPACK_IMPORTED_MODULE_5__ads__["a" /* ads */],
    categories: __WEBPACK_IMPORTED_MODULE_6__categories__["a" /* categories */],
    xpItems: __WEBPACK_IMPORTED_MODULE_7__xpItems__["a" /* xpItems */],
    feed: __WEBPACK_IMPORTED_MODULE_8__feed__["a" /* feed */],
    conversations: __WEBPACK_IMPORTED_MODULE_9__conversations__["a" /* conversations */],
    cinemaItems: __WEBPACK_IMPORTED_MODULE_10__cinemas__["a" /* cinemaItems */],
    detailCinema: __WEBPACK_IMPORTED_MODULE_11__detailCinema__["a" /* detailCinema */],
    xpFeeds: __WEBPACK_IMPORTED_MODULE_12__xpFeeds__["a" /* xpFeeds */]
});


/***/ }),

/***/ "./src/app/store/categories.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return categories; });
var categories = function (state, action) {
    if (state === void 0) { state = []; }
    var _state = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case 'FETCH_CATEGORY':
            _state = action.payload;
            return _state;
        case 'LOGOUT':
            return [];
        default:
            return _state;
    }
};


/***/ }),

/***/ "./src/app/store/cinemas.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return cinemaItems; });
var cinemaItems = function (state, action) {
    if (state === void 0) { state = []; }
    var _state = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case 'FETCH_CINEMAS':
            _state = action.payload;
            return _state;
        case 'SHOW_XP_ITEM_MORE':
            return _state;
        case 'FETCH_XP_DETAIL':
            return _state;
        case 'LIKE_XP_FEED':
            return _state;
        case 'DISLIKE_XP_FEED':
            return _state;
        case 'FOLLOW_XP_FEED':
            return _state;
        case 'LOGOUT':
            return [];
        default:
            return _state;
    }
};


/***/ }),

/***/ "./src/app/store/conversations.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return conversations; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);

var conversations = function (state, action) {
    if (state === void 0) { state = []; }
    var _state = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case 'FETCH_CONVERSATION':
            _state = [];
            __WEBPACK_IMPORTED_MODULE_0_underscore__["each"](action.payload, function (item) {
                _state.push(item);
            });
            _state[0].isSelected = true;
            return _state;
        case 'SELECT_CONVERSATION':
            var conv = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](_state, function (c) {
                return c.id == action.payload;
            });
            if (!conv)
                return _state;
            __WEBPACK_IMPORTED_MODULE_0_underscore__["each"](_state, function (c) {
                c.isSelected = false;
            });
            conv.isSelected = true;
            return _state;
        case 'GET_MESSAGES':
            var conv = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](_state, function (c) {
                return c.id == action.payload.convId;
            });
            if (!conv)
                return _state;
            conv.messages = action.payload.messages;
            return _state;
        case 'CREATE_CONVERSATION':
            var id = action.payload.conversationID;
            var participants = action.payload.participants;
            var sender = action.payload.sender;
            var isGroup = action.payload.isGroup;
            __WEBPACK_IMPORTED_MODULE_0_underscore__["each"](_state, function (conv) {
                conv.isSelected = false;
            });
            if (!isGroup) {
                //Only has 1 participant -> conversation is not group chat
                _state.push({
                    id: id,
                    contentType: 'text',
                    msg: '',
                    dateTime: new Date().toISOString(),
                    participants: [],
                    isGroup: false,
                    partnerID: participants[0].id,
                    partnerPicture: participants[0].picture,
                    partnerName: participants[0].username,
                    senderID: sender.id,
                    senderPicture: sender.profile_pic,
                    isSelected: true,
                    messages: []
                });
            }
            else {
                //Has many participants -> conversation is group chat
                _state.push({
                    id: id,
                    contentType: 'text',
                    msg: '',
                    dateTime: new Date().toISOString(),
                    participants: participants,
                    isGroup: true,
                    partnerID: id,
                    partnerPicture: action.payload.avatar,
                    partnerName: action.payload.name,
                    senderID: sender.id,
                    senderPicture: sender.profile_pic,
                    isSelected: true,
                    messages: []
                });
            }
            return _state;
        case 'SAVE_CONVERSATION':
            var conversation = null;
            if (!action.payload.isGroup) {
                conversation = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](_state, function (item) {
                    return item.partnerID == action.payload.conversationID;
                });
            }
            else {
                conversation = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](_state, function (item) {
                    return item.id == action.payload.conversationID;
                });
            }
            if (conversation) {
                conversation.msg = action.payload.msg;
                conversation.contentType = action.payload.contentType;
            }
            return _state;
        case 'LOGOUT':
            return [];
        default:
            return _state;
    }
};


/***/ }),

/***/ "./src/app/store/detailCinema.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return detailCinema; });
var detailCinema = function (state, action) {
    if (state === void 0) { state = null; }
    var _state = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case 'FETCH_CINEMA':
            _state = action.payload;
            return _state;
        case 'LOGOUT':
            return [];
        default:
            return _state;
    }
};


/***/ }),

/***/ "./src/app/store/feed.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return feed; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);

var feed = function (state, action) {
    if (state === void 0) { state = null; }
    var _state = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case 'FETCH_DETAIL_FEED':
            _state = action.payload;
            return _state;
        case 'FAVOURITE_XP_FEED':
            if (__WEBPACK_IMPORTED_MODULE_0_underscore__["find"](_state.likedBy, function (item) { return item._id == action.payload.user._id; })) {
                _state.likedBy = __WEBPACK_IMPORTED_MODULE_0_underscore__["reject"](_state.likedBy, function (item) { return item._id == action.payload.user._id; });
                _state.isLiked = false;
            }
            else {
                _state.likedBy.push(action.payload.user);
                _state.isLiked = true;
            }
            return _state;
        case 'CREATE_XP_SHARE_FEED':
            _state.sharefeeds.push(action.payload);
            switch (action.payload.type) {
                case 'photo':
                    _state.media.push({
                        type: 'photo',
                        content: action.payload.content
                    });
                    break;
                case 'video':
                    _state.media.push({
                        type: 'video',
                        content: action.payload.content,
                        thumbnail: action.payload.thumbnail
                    });
                    break;
            }
            return _state;
        case 'CREATE_XP_REVIEW':
            _state.reviews.push(action.payload);
            return _state;
        case 'LOGOUT':
            return null;
        default:
            return _state;
    }
};


/***/ }),

/***/ "./src/app/store/loading.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loading; });
var loading = function (state, action) {
    if (state === void 0) { state = false; }
    var _state = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case 'START_FETCHING':
            _state = true;
            return _state;
        case 'END_FETCHING':
            _state = false;
            return _state;
        default:
            return _state;
    }
};


/***/ }),

/***/ "./src/app/store/specials.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return specials; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);

var specials = function (state, action) {
    if (state === void 0) { state = []; }
    var _state = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case 'FETCH_SPECIAL':
            _state = [];
            __WEBPACK_IMPORTED_MODULE_0_underscore__["each"](action.payload, function (item) {
                _state.push(item);
            });
            return _state;
        case 'LOGOUT':
            return [];
        default:
            return _state;
    }
};


/***/ }),

/***/ "./src/app/store/trendings.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return trendings; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);

var trendings = function (state, action) {
    if (state === void 0) { state = []; }
    var _state = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case 'FETCH_TRENDING':
            _state = [];
            __WEBPACK_IMPORTED_MODULE_0_underscore__["each"](action.payload, function (item) {
                item.showMore = false;
                item.latitude = 0;
                item.longitude = 0;
                item.website = '';
                item.phone = '';
                item.twitter = '';
                item.photos = [];
                item.tagged = false;
                _state.push(item);
            });
            return _state;
        case 'SHOW_TRENDING_MORE':
            var item = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](_state, function (trending) {
                return trending.placeID == action.payload;
            });
            if (!item)
                return _state;
            item.showMore = !item.showMore;
            return _state;
        case 'FETCH_TRENDING_DETAIL':
            var item = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](_state, function (trending) {
                return trending.placeID == action.payload.id;
            });
            if (!item)
                return _state;
            item.latitude = Number(action.payload.latitude);
            item.longitude = Number(action.payload.longitude);
            item.website = action.payload.website;
            item.phone = action.payload.phone;
            item.twitter = action.payload.twitter;
            item.photos = action.payload.photos;
            item.tagged = action.payload.tagged;
            return _state;
        case 'LOGOUT':
            return [];
        default:
            return _state;
    }
};


/***/ }),

/***/ "./src/app/store/ui.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ui; });
var _init = {
    xpKeyword: '',
    xpLocation: '',
    xpPage: 1,
    xpTotalPage: 1,
    feedMenu: 'happening',
    discoverLocation: '',
    trendingPage: 1,
    trendingTotalPage: 1,
    specialPage: 1,
    specialTotalPage: 1,
    conversationPage: 1,
    conversationTotalPage: 1,
    cinemaLocation: '',
    cinemaMenu: 'showtime',
    cinemaPage: 1,
    cinemaTotalPage: 1
};
var ui = function (state, action) {
    if (state === void 0) { state = _init; }
    var _state = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case 'UPDATE_LOCATION':
            _state.discoverLocation = action.payload;
            _state.xpLocation = action.payload;
            _state.cinemaLocation = action.payload;
            return _state;
        case 'FETCH_USER':
            _state.discoverLocation = action.payload.location;
            _state.xpLocation = action.payload.location;
            _state.cinemaLocation = action.payload.location;
            return _state;
        case 'FILTER_XP':
            _state.xpLocation = action.payload.location;
            _state.xpKeyword = action.payload.keyword;
            return _state;
        case 'FILTER_DISCOVER':
            _state.discoverLocation = action.payload.location;
            return _state;
        case 'FILTER_CINEMA':
            _state.cinemaLocation = action.payload.location;
            return _state;
        case 'FETCH_CINEMAS':
            _state.cinemaPage = action.page;
            _state.cinemaTotalPage = action.total_pages;
            return _state;
        case 'FETCH_XP':
            _state.xpPage = action.page;
            _state.xpTotalPage = action.total_pages;
            return _state;
        case 'SELECT_XP_PAGE':
            _state.xpPage = action.payload;
            return _state;
        case 'SELECT_CINEMA_PAGE':
            _state.cinemaPage = action.payload;
            return _state;
        case 'FETCH_TRENDING':
            _state.trendingPage = action.page;
            _state.trendingTotalPage = action.total_pages;
            return _state;
        case 'SELECT_TRENDING_PAGE':
            _state.trendingPage = action.payload;
            return _state;
        case 'RESET_SHARE_TRENDING_DATA':
            _state.trendingPage = 1;
            return _state;
        case 'FETCH_SPECIAL':
            _state.specialPage = action.page;
            _state.specialTotalPage = action.total_pages;
            return _state;
        case 'SELECT_SPECIAL_PAGE':
            _state.specialPage = action.payload;
            return _state;
        case 'CHANGE_FEED_MENU':
            _state.feedMenu = action.payload;
            return _state;
        case 'CHANGE_CINEMA_MENU':
            _state.cinemaMenu = action.payload;
            return _state;
        case 'FETCH_CONVERSATION':
            _state.conversationPage = action.page;
            _state.conversationTotalPage = action.total_pages;
            return _state;
        case 'LOGOUT':
            return _state;
        default:
            return _state;
    }
};


/***/ }),

/***/ "./src/app/store/user.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return user; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore__ = __webpack_require__("./node_modules/underscore/underscore.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_underscore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_underscore__);

var user = function (state, action) {
    if (state === void 0) { state = null; }
    var _state = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case 'FETCH_USER':
            _state = action.payload;
            _state.friends = [];
            _state.uploads = [];
            _state.receivedInviations = [];
            _state.sentInviations = [];
            return _state;
        case 'FETCH_FRIEND':
            _state.friends = [];
            __WEBPACK_IMPORTED_MODULE_0_underscore__["each"](action.payload, function (friend) {
                _state.friends.push(friend);
            });
            return _state;
        case 'FETCH_UPLOAD':
            _state.uploads = [];
            __WEBPACK_IMPORTED_MODULE_0_underscore__["each"](action.payload, function (upload) {
                _state.uploads.push(upload);
            });
            return _state;
        case 'FETCH_RECEIVED_INVITATIONS':
            _state.receivedInviations = [];
            __WEBPACK_IMPORTED_MODULE_0_underscore__["each"](action.payload, function (invite) {
                _state.receivedInviations.push(invite);
            });
            return _state;
        case 'FETCH_SENT_INVITATIONS':
            _state.sentInviations = [];
            __WEBPACK_IMPORTED_MODULE_0_underscore__["each"](action.payload, function (invite) {
                _state.sentInviations.push(invite);
            });
            return _state;
        case 'UPDATE_PREF':
            var cat = action.payload.cat;
            var sub = action.payload.sub;
            var pref = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](_state.preferences, function (item) { return item.id == cat.id; });
            if (!pref) {
                if (!sub) {
                    _state.preferences.push({
                        id: cat.id,
                        name: cat.name
                    });
                }
                else {
                    _state.preferences.push({
                        id: cat.id,
                        name: cat.name,
                        sub_categories: [sub]
                    });
                }
            }
            else {
                if (!sub) {
                    _state.preferences = __WEBPACK_IMPORTED_MODULE_0_underscore__["reject"](_state.preferences, function (item) { return item.id == cat.id; });
                }
                else {
                    var findSub = __WEBPACK_IMPORTED_MODULE_0_underscore__["find"](pref.sub_categories, function (item) { return item.id == sub.id; });
                    if (findSub) {
                        pref.sub_categories = __WEBPACK_IMPORTED_MODULE_0_underscore__["reject"](pref.sub_categories, function (item) { return item.id == sub.id; });
                        if (!pref.sub_categories.length) {
                            _state.preferences = __WEBPACK_IMPORTED_MODULE_0_underscore__["reject"](_state.preferences, function (item) { return item.id == cat.id; });
                        }
                    }
                    else {
                        pref.sub_categories.push(sub);
                    }
                }
            }
            return _state;
        case 'UPDATE_PROFILE_SUCCESS':
            _state.username = action.payload.username;
            _state.birthday = action.payload.birthday;
            _state.phone = action.payload.phone;
            return _state;
        case 'UPDATE_PERMISSION_SUCCESS':
            var resource = action.payload.resource;
            var mode = action.payload.mode;
            switch (resource) {
                case 'contact_me':
                    _state.permissions.contact_me.push(mode);
                    break;
                case 'friends':
                    _state.permissions.friends.push(mode);
                    break;
                case 'friend_request':
                    _state.permissions.friend_request.push(mode);
                    break;
                case 'message':
                    _state.permissions.message.push(mode);
                    break;
            }
            return _state;
        case 'LOGOUT':
            return null;
        default:
            return _state;
    }
};


/***/ }),

/***/ "./src/app/store/xpFeeds.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return xpFeeds; });
var xpFeeds = function (state, action) {
    if (state === void 0) { state = []; }
    var _state = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case 'FETCH_XP_FEED':
            _state = action.payload;
            return _state;
        case 'LOGOUT':
            return [];
        default:
            return _state;
    }
};


/***/ }),

/***/ "./src/app/store/xpItems.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return xpItems; });
var xpItems = function (state, action) {
    if (state === void 0) { state = []; }
    var _state = JSON.parse(JSON.stringify(state));
    switch (action.type) {
        case 'FETCH_XP':
            _state = action.payload;
            return _state;
        case 'LOGOUT':
            return [];
        default:
            return _state;
    }
};


/***/ }),

/***/ "./src/app/utils/util.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var typingTimeout = null;

var UtilService = /** @class */ (function () {
    function UtilService(http) {
        this.http = http;
    }
    UtilService.prototype.setTypingTimeout = function () {
        // return new Promise((resolve, reject) => {
        //   typingTimeout = setTimeout(() => {
        //     resolve();
        //   }, 3000);
        // });
    };
    UtilService.prototype.unsetTypingTimeout = function () {
        // if(typingTimeout) {
        //   clearTimeout(typingTimeout);
        // }
        // typingTimeout = null;
    };
    UtilService.prototype.isEmailValid = function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    UtilService.prototype.getAddressByPostcode = function (postcode) {
        var _this = this;
        var apiKey = 'PCWYH-6L4MX-9Y3PW-GNBLP';
        return new Promise(function (resolve, reject) {
            _this.http.get("http://ws.postcoder.com/pcw/" + apiKey + "/address/uk/" + postcode.replace(' ', '') + "?lines=3&format=json")
                .subscribe(function (res) {
                var ret = res.json();
                return resolve(ret);
            }, function (err) {
                return reject(err);
            });
        });
    };
    UtilService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], UtilService);
    return UtilService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
var environment = {
    production: false,
    firebase: {
        apiKey: "AIzaSyBQZXAmRvj9KCn0eTROrKq9p_lByTwGB74",
        authDomain: "meeting-up.firebaseapp.com",
        databaseURL: "https://meeting-up.firebaseio.com",
        projectId: "meeting-up",
        storageBucket: "meeting-up.appspot.com",
        messagingSenderId: "970065394670"
    }
};
1;


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("./src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("./node_modules/rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_zip__ = __webpack_require__("./node_modules/rxjs/_esm5/add/observable/zip.js");






if (__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_16" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */]);


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map